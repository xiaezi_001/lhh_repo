CREATE DATABASE  IF NOT EXISTS `szgc_2020` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `szgc_2020`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 192.168.1.104    Database: szgc_2020
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `szgc_branch`
--

DROP TABLE IF EXISTS `szgc_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `szgc_branch` (
  `branch_id` bigint NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `branch_type` int DEFAULT NULL,
  `link_man` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `link_phone` varchar(50) DEFAULT NULL,
  `memo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `delete_mark` int DEFAULT NULL,
  `register_id` bigint DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `operator_id` bigint DEFAULT NULL,
  `operator_date` datetime DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `szgc_branch`
--

LOCK TABLES `szgc_branch` WRITE;
/*!40000 ALTER TABLE `szgc_branch` DISABLE KEYS */;
INSERT INTO `szgc_branch` VALUES (1,'一分公司',0,'3','13811111111','一分公司',0,1,'2020-09-20 20:30:37',1,'2020-09-20 20:30:42');
/*!40000 ALTER TABLE `szgc_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `szgc_contract`
--

DROP TABLE IF EXISTS `szgc_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `szgc_contract` (
  `con_id` bigint NOT NULL AUTO_INCREMENT,
  `con_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `con_start_date` datetime DEFAULT NULL,
  `con_end_date` datetime DEFAULT NULL,
  `con_type` int DEFAULT NULL,
  `con_yz` bigint DEFAULT NULL,
  `con_link_man` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `con_link_phone` varchar(50) DEFAULT NULL,
  `con_money` decimal(18,2) DEFAULT NULL,
  `con_status` int DEFAULT NULL,
  `data_line` int DEFAULT NULL,
  `con_line` int DEFAULT NULL,
  `register_id` bigint DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `operator_id` bigint DEFAULT NULL,
  `operator_date` datetime DEFAULT NULL,
  `delete_mark` int DEFAULT NULL,
  `memo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `szgc_contract`
--

LOCK TABLES `szgc_contract` WRITE;
/*!40000 ALTER TABLE `szgc_contract` DISABLE KEYS */;
INSERT INTO `szgc_contract` VALUES (1,'测试合同','2019-06-01 00:00:00','2020-09-30 00:00:00',0,2,'张三','13512345678',1000000.00,0,0,0,1,'2020-09-19 17:23:59',1,'2020-09-20 14:43:47',0,'啊打发打发涛涛涛涛'),(2,'测试合同2','2019-08-01 00:00:00','2020-12-31 00:00:00',1,3,'1','13522222222',5000000.00,0,0,0,1,'2020-09-20 21:00:47',1,'2020-09-20 21:00:47',0,NULL),(3,'测试合同3','2019-06-05 00:00:00','2020-09-30 00:00:00',0,3,'ttttt','13511111111',3333345687.00,0,0,0,1,'2020-09-22 20:57:26',1,'2020-09-22 20:58:44',0,NULL);
/*!40000 ALTER TABLE `szgc_contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `szgc_yz_payment`
--

DROP TABLE IF EXISTS `szgc_yz_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `szgc_yz_payment` (
  `jl_id` bigint NOT NULL AUTO_INCREMENT,
  `con_id` bigint DEFAULT NULL,
  `jl_month` int DEFAULT NULL,
  `jl_date` datetime DEFAULT NULL,
  `jl_custom_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `jl_money` decimal(18,2) DEFAULT NULL,
  `con_cz` decimal(18,2) DEFAULT NULL,
  `input_user` bigint DEFAULT NULL,
  `memo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `delete_mark` int DEFAULT NULL,
  `register_id` bigint DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `operator_id` bigint DEFAULT NULL,
  `operator_date` datetime DEFAULT NULL,
  PRIMARY KEY (`jl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `szgc_yz_payment`
--

LOCK TABLES `szgc_yz_payment` WRITE;
/*!40000 ALTER TABLE `szgc_yz_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `szgc_yz_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `szgc_yzdw`
--

DROP TABLE IF EXISTS `szgc_yzdw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `szgc_yzdw` (
  `yz_id` bigint NOT NULL AUTO_INCREMENT,
  `yz_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `yz_type` int DEFAULT NULL,
  `yz_link_man` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `yz_link_phone` varchar(50) DEFAULT NULL,
  `memo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `yz_parent_id` int DEFAULT NULL,
  `yz_status` int DEFAULT NULL,
  `delete_mark` int DEFAULT NULL,
  `register_id` bigint DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `operator_id` bigint DEFAULT NULL,
  `operator_date` datetime DEFAULT NULL,
  PRIMARY KEY (`yz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `szgc_yzdw`
--

LOCK TABLES `szgc_yzdw` WRITE;
/*!40000 ALTER TABLE `szgc_yzdw` DISABLE KEYS */;
INSERT INTO `szgc_yzdw` VALUES (1,'天津市公路养护管理中心',0,'张三','13512345678','天津市公路处',0,0,0,1,'2020-09-20 11:35:08',1,'2020-09-20 12:40:24'),(2,'天津高速公路集团',0,'李四ttt','13546798','天津高速公路集团',0,0,0,1,'2020-09-20 11:35:52',1,'2020-09-20 11:36:00'),(3,'天津市公安局宁河分局',1,'1','13522222222','天津市公安局宁河分局',0,0,0,1,'2020-09-20 12:41:13',1,'2020-09-20 15:40:22'),(4,'天津市汉沽区建设管理委员会',1,'2','13811111111','天津市汉沽区建设管理委员会',NULL,NULL,0,1,'2020-09-20 20:21:47',1,'2020-09-20 20:21:53');
/*!40000 ALTER TABLE `szgc_yzdw` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-23 20:35:45
