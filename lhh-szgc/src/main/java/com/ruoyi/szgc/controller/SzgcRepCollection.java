package com.ruoyi.szgc.controller;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.szgc.domain.SzgcRepSearch;
import com.ruoyi.szgc.domain.SzgcSearchResult;
import com.ruoyi.szgc.domain.SzgcYzUpload;
import com.ruoyi.szgc.domain.SzgcYzdw;
import com.ruoyi.szgc.service.ISzgcExport;
import com.ruoyi.szgc.service.ISzgcYzPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.ruoyi.framework.datasource.DynamicDataSourceContextHolder.log;

@RestController
@RequestMapping("/szgc/report")
public class SzgcRepCollection extends BaseController {

    @Autowired
    private ISzgcExport expertService;

    @Autowired
    private ISzgcYzPaymentService yzPaymentService;

    @GetMapping("/ybb")
    @PreAuthorize("@ss.hasPermi('szgc:report:xls')")
    public AjaxResult exportYbb(SzgcRepSearch query)
    {
        int y = Integer.parseInt(query.getEndDate().substring(0, 4));
        int m = Integer.parseInt(query.getEndDate().substring(5, 7));

        query.setYear(y);
        query.setMonth(m);
        //query.setEndDate("2020-12-31");
        return expertService.exportYbb(query);
    }

    @GetMapping("/nbzq")
    @PreAuthorize("@ss.hasPermi('szgc:report:xls')")
    public AjaxResult exportNBZQ(SzgcRepSearch query)
    {
        //query.setStartDate("2020-12-01");
        //query.setEndDate("2020-12-31");
        return expertService.exportNBZQ(query);
    }

    @GetMapping("/gsbk")
    @PreAuthorize("@ss.hasPermi('szgc:report:xls')")
    public AjaxResult exportBK(SzgcRepSearch query)
    {
        //query.setStartDate("2020-12-01");
        //query.setEndDate("2020-12-31");
        query.setYear(Integer.parseInt(query.getEndDate().substring(0, 4)));
        return expertService.exportBK(query);
    }

    @GetMapping("/gszq")
    @PreAuthorize("@ss.hasPermi('szgc:report:xls')")
    public AjaxResult exportGSZQ(SzgcRepSearch query)
    {
        //query.setStartDate("2020-12-01");
        //query.setEndDate("2020-12-31");
        query.setYear(Integer.parseInt(query.getEndDate().substring(0, 4)));
        return expertService.exportGSZQ(query);
    }

    @GetMapping("/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            File tmpFile = new File(fileName);
            String fileShortName = tmpFile.getName();

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + fileShortName);
            FileUtils.writeBytes(fileName, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(fileName);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    @GetMapping("/downloadEx")
    public void fileDownloadEx(String fileId, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            SzgcYzUpload upload = yzPaymentService.getUploadFileById(Long.parseLong(fileId));

            //File tmpFile = new File(upload.getFileUrl());
            String fileShortName = URLEncoder.encode(upload.getFileName(), "utf-8");

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + fileShortName);
            FileUtils.writeBytes(upload.getFileUrl(), response.getOutputStream());
            if (delete)
            {
                //FileUtils.deleteFile(fileName);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    @PreAuthorize("@ss.hasPermi('szgc:report:list')")
    @GetMapping("/searchlist")
    public TableDataInfo list(SzgcRepSearch query)
    {
        startPage();
        List<SzgcSearchResult> list = expertService.selectSearch(query);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('szgc:report:list')")
    @GetMapping("/mainsum")
    public AjaxResult selectMainSum()
    {
        return AjaxResult.success(expertService.selectMainSum());
    }
}
