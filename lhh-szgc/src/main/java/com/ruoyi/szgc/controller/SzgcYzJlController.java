package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcYzJl;
import com.ruoyi.szgc.service.ISzgcYzJlService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主计量Controller
 * 
 * @author lhh
 * @date 2020-11-29
 */
@RestController
@RequestMapping("/szgc/yzjl")
public class SzgcYzJlController extends BaseController
{
    @Autowired
    private ISzgcYzJlService szgcYzJlService;

    @Autowired
    private TokenService tokenService;
    /**
     * 查询业主计量列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzJl szgcYzJl)
    {
        startPage();
        List<SzgcYzJl> list = szgcYzJlService.selectSzgcYzJlList(szgcYzJl);
        return getDataTable(list);
    }

    /**
     * 导出业主计量列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:export')")
    @Log(title = "业主计量", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcYzJl szgcYzJl)
    {
        List<SzgcYzJl> list = szgcYzJlService.selectSzgcYzJlList(szgcYzJl);
        ExcelUtil<SzgcYzJl> util = new ExcelUtil<SzgcYzJl>(SzgcYzJl.class);
        return util.exportExcel(list, "yzjl");
    }

    /**
     * 获取业主计量详细信息
     */
    //@PreAuthorize("@ss.hasPermi('szgc:yzjl:query')")
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/{jlId}")
    public AjaxResult getInfo(@PathVariable("jlId") Long jlId)
    {
        return AjaxResult.success(szgcYzJlService.selectSzgcYzJlById(jlId));
    }

    /**
     * 新增业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:add')")
    @Log(title = "业主计量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcYzJl szgcYzJl)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzJl.setCreateBy(userName);
        szgcYzJl.setUpdateBy(userName);

        return toAjax(szgcYzJlService.insertSzgcYzJl(szgcYzJl));
    }

    /**
     * 修改业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:edit')")
    @Log(title = "业主计量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcYzJl szgcYzJl)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzJl.setUpdateBy(userName);
        return toAjax(szgcYzJlService.updateSzgcYzJl(szgcYzJl));
    }

    /**
     * 删除业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:remove')")
    @Log(title = "业主计量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jlIds}")
    public AjaxResult remove(@PathVariable Long[] jlIds)
    {
        return toAjax(szgcYzJlService.deleteSzgcYzJlByIds(jlIds));
    }
}
