package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcYzCz;
import com.ruoyi.szgc.service.ISzgcYzCzService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主产值Controller
 * 
 * @author lhh
 * @date 2020-12-17
 */
@RestController
@RequestMapping("/szgc/yzcz")
public class SzgcYzCzController extends BaseController
{
    @Autowired
    private ISzgcYzCzService szgcYzCzService;

    @Autowired
    private TokenService tokenService;
    /**
     * 查询业主产值列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzCz szgcYzCz)
    {
        startPage();
        List<SzgcYzCz> list = szgcYzCzService.selectSzgcYzCzList(szgcYzCz);
        return getDataTable(list);
    }

    /**
     * 导出业主产值列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:export')")
    @Log(title = "业主产值", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcYzCz szgcYzCz)
    {
        List<SzgcYzCz> list = szgcYzCzService.selectSzgcYzCzList(szgcYzCz);
        ExcelUtil<SzgcYzCz> util = new ExcelUtil<SzgcYzCz>(SzgcYzCz.class);
        return util.exportExcel(list, "yzcz");
    }

    /**
     * 获取业主产值详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/{czId}")
    public AjaxResult getInfo(@PathVariable("czId") Long czId)
    {
        return AjaxResult.success(szgcYzCzService.selectSzgcYzCzById(czId));
    }

    /**
     * 新增业主产值
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:add')")
    @Log(title = "业主产值", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcYzCz szgcYzCz)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzCz.setCreateBy(userName);
        szgcYzCz.setUpdateBy(userName);

        return toAjax(szgcYzCzService.insertSzgcYzCz(szgcYzCz));
    }

    /**
     * 修改业主产值
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:edit')")
    @Log(title = "业主产值", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcYzCz szgcYzCz)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzCz.setUpdateBy(userName);

        return toAjax(szgcYzCzService.updateSzgcYzCz(szgcYzCz));
    }

    /**
     * 删除业主产值
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:remove')")
    @Log(title = "业主产值", businessType = BusinessType.DELETE)
	@DeleteMapping("/{czIds}")
    public AjaxResult remove(@PathVariable Long[] czIds)
    {
        return toAjax(szgcYzCzService.deleteSzgcYzCzByIds(czIds));
    }
}
