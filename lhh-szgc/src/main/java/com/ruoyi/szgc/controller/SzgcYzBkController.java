package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcYzBk;
import com.ruoyi.szgc.service.ISzgcYzBkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 总公司拨款Controller
 * 
 * @author lhh
 * @date 2020-11-29
 */
@RestController
@RequestMapping("/szgc/zgsbk")
public class SzgcYzBkController extends BaseController
{
    @Autowired
    private ISzgcYzBkService szgcYzBkService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询总公司拨款列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzBk szgcYzBk)
    {
        startPage();
        List<SzgcYzBk> list = szgcYzBkService.selectSzgcYzBkList(szgcYzBk);
        return getDataTable(list);
    }

    /**
     * 导出总公司拨款列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:export')")
    @Log(title = "总公司拨款", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcYzBk szgcYzBk)
    {
        List<SzgcYzBk> list = szgcYzBkService.selectSzgcYzBkList(szgcYzBk);
        ExcelUtil<SzgcYzBk> util = new ExcelUtil<SzgcYzBk>(SzgcYzBk.class);
        return util.exportExcel(list, "zgsbk");
    }

    /**
     * 获取总公司拨款详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/{bkId}")
    public AjaxResult getInfo(@PathVariable("bkId") Long bkId)
    {
        return AjaxResult.success(szgcYzBkService.selectSzgcYzBkById(bkId));
    }

    /**
     * 新增总公司拨款
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:add')")
    @Log(title = "总公司拨款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcYzBk szgcYzBk)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzBk.setCreateBy(userName);
        szgcYzBk.setUpdateBy(userName);
        return toAjax(szgcYzBkService.insertSzgcYzBk(szgcYzBk));
    }

    /**
     * 修改总公司拨款
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:edit')")
    @Log(title = "总公司拨款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcYzBk szgcYzBk)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzBk.setUpdateBy(userName);
        return toAjax(szgcYzBkService.updateSzgcYzBk(szgcYzBk));
    }

    /**
     * 删除总公司拨款
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:remove')")
    @Log(title = "总公司拨款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{bkIds}")
    public AjaxResult remove(@PathVariable Long[] bkIds)
    {
        return toAjax(szgcYzBkService.deleteSzgcYzBkByIds(bkIds));
    }
}
