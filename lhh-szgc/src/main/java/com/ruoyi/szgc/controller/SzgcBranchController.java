package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcBranch;
import com.ruoyi.szgc.service.ISzgcBranchService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 分公司Controller
 * 
 * @author lhh
 * @date 2020-09-20
 */
@RestController
@RequestMapping("/szgc/branch")
public class SzgcBranchController extends BaseController
{
    @Autowired
    private ISzgcBranchService szgcBranchService;

    @Autowired
    private TokenService tokenService;
    /**
     * 查询分公司列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:branch:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcBranch szgcBranch)
    {
        startPage();
        List<SzgcBranch> list = szgcBranchService.selectSzgcBranchList(szgcBranch);
        return getDataTable(list);
    }

    /**
     * 导出分公司列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:branch:export')")
    @Log(title = "分公司", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcBranch szgcBranch)
    {
        List<SzgcBranch> list = szgcBranchService.selectSzgcBranchList(szgcBranch);
        ExcelUtil<SzgcBranch> util = new ExcelUtil<SzgcBranch>(SzgcBranch.class);
        return util.exportExcel(list, "branch");
    }

    /**
     * 获取分公司详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:branch:query')")
    @GetMapping(value = "/{branchId}")
    public AjaxResult getInfo(@PathVariable("branchId") Long branchId)
    {
        return AjaxResult.success(szgcBranchService.selectSzgcBranchById(branchId));
    }

    /**
     * 新增分公司
     */
    @PreAuthorize("@ss.hasPermi('szgc:branch:add')")
    @Log(title = "分公司", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcBranch szgcBranch)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        szgcBranch.setBranchType(0);
        szgcBranch.setRegisterId(userId);
        szgcBranch.setOperatorId(userId);

        return toAjax(szgcBranchService.insertSzgcBranch(szgcBranch));
    }

    /**
     * 修改分公司
     */
    @PreAuthorize("@ss.hasPermi('szgc:branch:edit')")
    @Log(title = "分公司", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcBranch szgcBranch)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();
        szgcBranch.setOperatorId(userId);

        return toAjax(szgcBranchService.updateSzgcBranch(szgcBranch));
    }

    /**
     * 删除分公司
     */
    @PreAuthorize("@ss.hasPermi('szgc:branch:remove')")
    @Log(title = "分公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{branchIds}")
    public AjaxResult remove(@PathVariable Long[] branchIds)
    {
        return toAjax(szgcBranchService.deleteSzgcBranchByIds(branchIds));
    }
}
