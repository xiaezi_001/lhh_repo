package com.ruoyi.szgc.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.service.ISzgcCmpPaymentService;
import com.ruoyi.szgc.service.ISzgcContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * 业主计量Controller
 * 
 * @author lhh
 * @date 2020-09-24
 */
@RestController
@RequestMapping("/szgc/cmppayment")
public class SzgcCmpPaymentController extends BaseController
{
    @Autowired
    private ISzgcCmpPaymentService szgcCmpPaymentService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISzgcContractService contractService;

    /**
     * 查询业主计量列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcCmpPayment CmpPayment)
    {
        startPage();

        if(CmpPayment.getConId() == null){
            CmpPayment.setConId(0L);
        }

        List<SzgcCmpPayment> list = szgcCmpPaymentService.selectSzgcCmpPaymentList(CmpPayment);
        return getDataTable(list);
    }
    /**
     * 导出业主计量列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:export')")
    @Log(title = "业主计量", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcCmpPayment szgcCmpPayment)
    {
        List<SzgcCmpPayment> list = szgcCmpPaymentService.selectSzgcCmpPaymentList(szgcCmpPayment);
        ExcelUtil<SzgcCmpPayment> util = new ExcelUtil<SzgcCmpPayment>(SzgcCmpPayment.class);
        return util.exportExcel(list, "Cmppayment");
    }

    /**
     * 获取业主计量详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:query')")
    @GetMapping(value = "/{jlId}")
    public AjaxResult getInfo(@PathVariable("jlId") Long jlId)
    {
        return AjaxResult.success(szgcCmpPaymentService.selectSzgcCmpPaymentById(jlId));
    }

    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:query')")
    @GetMapping(value = "/listbycon/{conId}")
    public TableDataInfo getListByConId(@PathVariable("conId") String conId)
    {
        Long cid = 0L;
        if(!conId.equals("null")){
            cid = Long.parseLong(conId);
        }

        List<SzgcCmpPayment> list = szgcCmpPaymentService.selectSzgcCmpPaymentByConId(cid);
        return getDataTable(list);
    }

    /**
     * 新增业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:add')")
    @Log(title = "业主计量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcJLPayment jlPayment) throws ParseException {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        jlPayment.setUserId(userId);

        int res = szgcCmpPaymentService.insertSzgcCmpPayment(jlPayment);

        if(res < 0){
            return AjaxResult.error(jlPayment.getMemo());
        }
        else {
            return toAjax(res);
        }
    }

    /**
     * 修改业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:edit')")
    @Log(title = "业主计量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcJLPayment jlPayment) throws ParseException {
        return toAjax(szgcCmpPaymentService.updateSzgcCmpPayment(jlPayment));
    }

    /**
     * 删除业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:cmppayment:remove')")
    @Log(title = "业主计量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jlIds}")
    public AjaxResult remove(@PathVariable Long[] jlIds)
    {
        return toAjax(szgcCmpPaymentService.deleteSzgcCmpPaymentByIds(jlIds));
    }

    @GetMapping("/jlcon/{conId}")
    public AjaxResult getJLAndConSum(@PathVariable("conId") Long conId){
        SzgcContract con = contractService.selectSzgcContractById(conId);
        SzgcJLSumMoney jlsum = szgcCmpPaymentService.getJLSumByConId(conId);

        SzgcJLConData jlcon = new SzgcJLConData();
        jlcon.setContract(con);
        jlcon.setJlSumInfo(jlsum);

        return AjaxResult.success(jlcon);
    }

    @GetMapping("/jlcmplist/{jlId}")
    public AjaxResult getCmpPaymentCmpList(@PathVariable("jlId") Long jlId){
        return AjaxResult.success(szgcCmpPaymentService.getCmpPaymentList(jlId));
    }

    @PostMapping("/savecmpjlcmp")
    public AjaxResult saveCmpJLCmp(@RequestBody List<SzgcCmpPaymentCmp> jlcmp){
        return toAjax(szgcCmpPaymentService.saveCmpJLCmpPayment(jlcmp));
    }
}
