package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcYzdw;
import com.ruoyi.szgc.service.ISzgcYzdwService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主单位Controller
 * 
 * @author lhh
 * @date 2020-09-19
 */
@RestController
@RequestMapping("/szgc/yzdw")
public class SzgcYzdwController extends BaseController
{
    @Autowired
    private ISzgcYzdwService szgcYzdwService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询业主单位列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzdw:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzdw szgcYzdw)
    {
        startPage();
        List<SzgcYzdw> list = szgcYzdwService.selectSzgcYzdwList(szgcYzdw);
        return getDataTable(list);
    }

    /**
     * 导出业主单位列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzdw:export')")
    @Log(title = "业主单位", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcYzdw szgcYzdw)
    {
        List<SzgcYzdw> list = szgcYzdwService.selectSzgcYzdwList(szgcYzdw);
        ExcelUtil<SzgcYzdw> util = new ExcelUtil<SzgcYzdw>(SzgcYzdw.class);
        return util.exportExcel(list, "yzdw");
    }

    /**
     * 获取业主单位详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzdw:query')")
    @GetMapping(value = "/{yzId}")
    public AjaxResult getInfo(@PathVariable("yzId") Long yzId)
    {
        return AjaxResult.success(szgcYzdwService.selectSzgcYzdwById(yzId));
    }

    /**
     * 新增业主单位
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzdw:add')")
    @Log(title = "业主单位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcYzdw szgcYzdw)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        szgcYzdw.setRegisterId(userId);
        szgcYzdw.setOperatorId(userId);

        return toAjax(szgcYzdwService.insertSzgcYzdw(szgcYzdw));
    }

    /**
     * 修改业主单位
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzdw:edit')")
    @Log(title = "业主单位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcYzdw szgcYzdw)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        szgcYzdw.setOperatorId(userId);

        return toAjax(szgcYzdwService.updateSzgcYzdw(szgcYzdw));
    }

    /**
     * 删除业主单位
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzdw:remove')")
    @Log(title = "业主单位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{yzIds}")
    public AjaxResult remove(@PathVariable Long[] yzIds) throws Exception {
        return toAjax(szgcYzdwService.deleteSzgcYzdwByIds(yzIds));
    }

    @GetMapping("/yzdwSelectList")
    public AjaxResult getYzdwSelectList(){
        //return AjaxResult.success(szgcYzdwService.getYzdwDisplay());
        return AjaxResult.success(szgcYzdwService.getYzdwGroupSelect());
    }
}
