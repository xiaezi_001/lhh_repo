package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcYzCzPlan;
import com.ruoyi.szgc.service.ISzgcYzCzPlanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产值计划Controller
 * 
 * @author lhh
 * @date 2020-12-17
 */
@RestController
@RequestMapping("/szgc/yzczplan")
public class SzgcYzCzPlanController extends BaseController
{
    @Autowired
    private ISzgcYzCzPlanService szgcYzCzPlanService;

    @Autowired
    private TokenService tokenService;
    /**
     * 查询产值计划列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzCzPlan szgcYzCzPlan)
    {
        startPage();
        List<SzgcYzCzPlan> list = szgcYzCzPlanService.selectSzgcYzCzPlanList(szgcYzCzPlan);
        return getDataTable(list);
    }

    /**
     * 导出产值计划列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:export')")
    @Log(title = "产值计划", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcYzCzPlan szgcYzCzPlan)
    {
        List<SzgcYzCzPlan> list = szgcYzCzPlanService.selectSzgcYzCzPlanList(szgcYzCzPlan);
        ExcelUtil<SzgcYzCzPlan> util = new ExcelUtil<SzgcYzCzPlan>(SzgcYzCzPlan.class);
        return util.exportExcel(list, "yzczplan");
    }

    /**
     * 获取产值计划详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/{czPlanId}")
    public AjaxResult getInfo(@PathVariable("czPlanId") Long czPlanId)
    {
        return AjaxResult.success(szgcYzCzPlanService.selectSzgcYzCzPlanById(czPlanId));
    }

    /**
     * 新增产值计划
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:add')")
    @Log(title = "产值计划", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcYzCzPlan szgcYzCzPlan) throws Exception {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzCzPlan.setCreateBy(userName);
        szgcYzCzPlan.setUpdateBy(userName);

        return toAjax(szgcYzCzPlanService.insertSzgcYzCzPlan(szgcYzCzPlan));
    }

    /**
     * 修改产值计划
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:edit')")
    @Log(title = "产值计划", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcYzCzPlan szgcYzCzPlan)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzCzPlan.setUpdateBy(userName);

        return toAjax(szgcYzCzPlanService.updateSzgcYzCzPlan(szgcYzCzPlan));
    }

    /**
     * 删除产值计划
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:remove')")
    @Log(title = "产值计划", businessType = BusinessType.DELETE)
	@DeleteMapping("/{czPlanIds}")
    public AjaxResult remove(@PathVariable Long[] czPlanIds)
    {
        return toAjax(szgcYzCzPlanService.deleteSzgcYzCzPlanByIds(czPlanIds));
    }
}
