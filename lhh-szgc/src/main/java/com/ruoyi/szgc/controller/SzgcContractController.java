package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.szgc.domain.SzgcContract;
import com.ruoyi.szgc.service.ISzgcContractService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.framework.web.service.TokenService;

/**
 * 合同Controller
 * 
 * @author lhh
 * @date 2020-09-17
 */
@RestController
@RequestMapping("/szgc/contract")
public class SzgcContractController extends BaseController
{
    @Autowired
    private ISzgcContractService szgcContractService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询合同列表
     */
    @GetMapping("/test")
    public AjaxResult getTestStr(){
        return AjaxResult.success("Test OK");
    }


    @PreAuthorize("@ss.hasPermi('szgc:contract:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcContract szgcContract)
    {
        startPage();
        List<SzgcContract> list = szgcContractService.selectSzgcContractList(szgcContract);
        return getDataTable(list);
    }

    /**
     * 导出合同列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:contract:export')")
    @Log(title = "合同", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcContract szgcContract)
    {
        List<SzgcContract> list = szgcContractService.selectSzgcContractList(szgcContract);
        ExcelUtil<SzgcContract> util = new ExcelUtil<SzgcContract>(SzgcContract.class);
        return util.exportExcel(list, "contract");
    }

    /**
     * 获取合同详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:contract:query')")
    @GetMapping(value = "/{conId}")
    public AjaxResult getInfo(@PathVariable("conId") Long conId)
    {
        return AjaxResult.success(szgcContractService.selectSzgcContractById(conId));
    }

    /**
     * 新增合同
     */
    @PreAuthorize("@ss.hasPermi('szgc:contract:add')")
    @Log(title = "合同", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcContract szgcContract)
    {
        //for production
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        szgcContract.setConStatus(0);
        szgcContract.setDataLine(0);
        szgcContract.setConLine(0);
        szgcContract.setRegisterId(userId);
        szgcContract.setOperatorId(userId);

        return toAjax(szgcContractService.insertSzgcContract(szgcContract));
    }

    /**
     * 修改合同
     */
    @PreAuthorize("@ss.hasPermi('szgc:contract:edit')")
    @Log(title = "合同", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcContract szgcContract)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        szgcContract.setOperatorId(userId);

        return toAjax(szgcContractService.updateSzgcContract(szgcContract));
    }

    /**
     * 删除合同
     */
    @PreAuthorize("@ss.hasPermi('szgc:contract:remove')")
    @Log(title = "合同", businessType = BusinessType.DELETE)
	@DeleteMapping("/{conIds}")
    public AjaxResult remove(@PathVariable Long[] conIds) throws Exception {
        return toAjax(szgcContractService.deleteSzgcContractByIds(conIds));
    }

    @GetMapping("/conSelectList/{cname}/{ctype}")
    public AjaxResult getContractSelectList(@PathVariable("cname") String cname, @PathVariable("ctype") Integer ctype){
        return AjaxResult.success(szgcContractService.getSelectConByName(cname, ctype));
    }
}
