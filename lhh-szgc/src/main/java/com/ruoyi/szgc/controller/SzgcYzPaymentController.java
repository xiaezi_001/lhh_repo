package com.ruoyi.szgc.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysDictTypeService;
import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.service.ISzgcBranchService;
import com.ruoyi.szgc.service.ISzgcContractService;
import com.ruoyi.szgc.service.ISzgcExport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.service.ISzgcYzPaymentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 业主计量Controller
 * 
 * @author lhh
 * @date 2020-09-24
 */
@RestController
@RequestMapping("/szgc/yzpayment")
public class SzgcYzPaymentController extends BaseController
{
    @Autowired
    private ISzgcYzPaymentService szgcYzPaymentService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISzgcContractService contractService;

    @Autowired
    @Qualifier("exportExcel")
    private ISzgcExport exportService;
    /**
     * 查询业主计量列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzPayment yzPayment)
    {
        startPage();

        if(yzPayment.getConId() == null){
            yzPayment.setConId(0L);
        }

        List<SzgcYzPayment> list = szgcYzPaymentService.selectSzgcYzPaymentList(yzPayment);
        return getDataTable(list);
    }
    /**
     * 导出业主计量列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:export')")
    @Log(title = "业主计量", businessType = BusinessType.EXPORT)
    @GetMapping("/export/{jlIds}")
    public AjaxResult export(@PathVariable Long[] jlIds)
    {
        /*
        List<SzgcYzPayment> list = szgcYzPaymentService.selectSzgcYzPaymentList(null);
        ExcelUtil<SzgcYzPayment> util = new ExcelUtil<SzgcYzPayment>(SzgcYzPayment.class);
        return util.exportExcel(list, "yzpayment");
        //*/
        return exportService.exportPayDetail(jlIds);
    }

    /**
     * 获取业主计量详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/{jlId}")
    public AjaxResult getInfo(@PathVariable("jlId") Long jlId)
    {
        return AjaxResult.success(szgcYzPaymentService.selectSzgcYzPaymentById(jlId));
    }

    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/listbycon/{conId}")
    public TableDataInfo getListByConId(@PathVariable("conId") String conId)
    {
        Long cid = 0L;
        if(!conId.equals("null")){
            cid = Long.parseLong(conId);
        }

        List<SzgcYzPayment> list = szgcYzPaymentService.selectSzgcYzPaymentByConId(cid);
        return getDataTable(list);
    }

    /**
     * 新增业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:add')")
    @Log(title = "业主计量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcJLPayment jlPayment) throws ParseException {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();

        jlPayment.setUserId(userId);

        int res = szgcYzPaymentService.insertSzgcYzPayment(jlPayment);

        if(res < 0){
            return AjaxResult.error(jlPayment.getMemo());
        }
        else {
            return toAjax(res);
        }
    }

    /**
     * 修改业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:edit')")
    @Log(title = "业主计量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcJLPayment jlPayment) throws ParseException {
        return toAjax(szgcYzPaymentService.updateSzgcYzPayment(jlPayment));
    }

    /**
     * 删除业主计量
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:remove')")
    @Log(title = "业主计量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jlIds}")
    public AjaxResult remove(@PathVariable Long[] jlIds)
    {
        return toAjax(szgcYzPaymentService.deleteSzgcYzPaymentByIds(jlIds));
    }

    @GetMapping("/jlcon/{conId}")
    public AjaxResult getJLAndConSum(@PathVariable("conId") Long conId){
        SzgcContract con = contractService.selectSzgcContractById(conId);
        SzgcJLSumMoney jlsum = szgcYzPaymentService.getJLSumByConId(conId);
        SzgcJLPayment jlpay = szgcYzPaymentService.getLastYZJLItem(conId);
        SzgcJLConData jlcon = new SzgcJLConData();
        List<SzgcYzPaymentCmp> cmps = szgcYzPaymentService.getCmpPaymentList(0L);

        jlcon.setContract(con);
        jlcon.setJlSumInfo(jlsum);
        //jlcon.setLastSL(szgcYzPaymentService.getLastYZJlSLRate(conId));
        jlcon.setLastSL(jlpay.getSlRate());
        jlcon.setLastGLF(jlpay.getGlfRate());
        jlcon.setLastLR(jlpay.getLrRate());
        jlcon.setCmpList(cmps);

        return AjaxResult.success(jlcon);
    }

    @GetMapping("/jlcmplist/{jlId}")
    public AjaxResult getYZPaymentCmpList(@PathVariable("jlId") Long jlId){
        return AjaxResult.success(szgcYzPaymentService.getCmpPaymentList(jlId));
    }

    @PostMapping("/saveyzjlcmp")
    public AjaxResult saveYZJLCmp(@RequestBody List<SzgcYzPaymentCmp> jlcmp){
        return toAjax(szgcYzPaymentService.saveYZJLCmpPayment(jlcmp));
    }

    @GetMapping("/getYzJLMonth")
    public AjaxResult getYzJLByMonth(SzgcYzPayment yzPayment){

        List<SzgcYzPayment> yzList = szgcYzPaymentService.selectSzgcYzPaymentList(yzPayment);

        if(yzList.size() > 0){
            return AjaxResult.success(szgcYzPaymentService.selectSzgcYzPaymentById(yzList.get(0).getJlId()));
        }
        else{
            SzgcJLPayment jlPayment = new SzgcJLPayment();
            jlPayment.setJlMoney(new BigDecimal(0));
            jlPayment.setJlPay(new BigDecimal(0));
            jlPayment.setJlBGSPay(new BigDecimal(0));
            jlPayment.setYfMoney(new BigDecimal(0));
            jlPayment.setGcMoney(new BigDecimal(0));
            jlPayment.setNmgMoney(new BigDecimal(0));

            return AjaxResult.success(yzPayment);
        }
    }

    @PostMapping("/saveUpload")
    public AjaxResult saveUpload(@RequestBody SzgcYzUpload upFile)
    {
        upFile.setDeleteMark(0);
        int res = szgcYzPaymentService.saveUploadFile(upFile);

        if(res < 0){
            return AjaxResult.error("upload fail");
        }
        else {
            return AjaxResult.success(upFile);  //toAjax(upFile);
        }
    }

    @GetMapping(value = "/getUploadFile/{jlId}/{ftype}")
    public TableDataInfo getUpFileByJlId(@PathVariable("jlId") Long jlId, @PathVariable("ftype") Integer ftype)
    {
        List<SzgcYzUpload> list = szgcYzPaymentService.getUploadFiles(jlId, ftype);
        return getDataTable(list);
    }

    @DeleteMapping("/deleteUpLoad/{upId}")
    public AjaxResult deleteUpload(@PathVariable("upId") Long uploadId)
    {
        int res = szgcYzPaymentService.deleteUpload(uploadId);

        if(res < 0){
            return AjaxResult.error("upload fail");
        }
        else {
            return toAjax(res);
        }
    }
}
