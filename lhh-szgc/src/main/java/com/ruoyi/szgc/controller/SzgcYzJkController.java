package com.ruoyi.szgc.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.szgc.domain.SzgcYzJk;
import com.ruoyi.szgc.service.ISzgcYzJkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主进款Controller
 * 
 * @author lhh
 * @date 2020-11-29
 */
@RestController
@RequestMapping("/szgc/yzjk")
public class SzgcYzJkController extends BaseController
{
    @Autowired
    private ISzgcYzJkService szgcYzJkService;

    @Autowired
    private TokenService tokenService;
    /**
     * 查询业主进款列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SzgcYzJk szgcYzJk)
    {
        startPage();
        List<SzgcYzJk> list = szgcYzJkService.selectSzgcYzJkList(szgcYzJk);
        return getDataTable(list);
    }

    /**
     * 导出业主进款列表
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:export')")
    @Log(title = "业主进款", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SzgcYzJk szgcYzJk)
    {
        List<SzgcYzJk> list = szgcYzJkService.selectSzgcYzJkList(szgcYzJk);
        ExcelUtil<SzgcYzJk> util = new ExcelUtil<SzgcYzJk>(SzgcYzJk.class);
        return util.exportExcel(list, "yzjk");
    }

    /**
     * 获取业主进款详细信息
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:query')")
    @GetMapping(value = "/{jkId}")
    public AjaxResult getInfo(@PathVariable("jkId") Long jkId)
    {
        return AjaxResult.success(szgcYzJkService.selectSzgcYzJkById(jkId));
    }

    /**
     * 新增业主进款
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:add')")
    @Log(title = "业主进款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SzgcYzJk szgcYzJk)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzJk.setCreateBy(userName);
        szgcYzJk.setUpdateBy(userName);

        return toAjax(szgcYzJkService.insertSzgcYzJk(szgcYzJk));
    }

    /**
     * 修改业主进款
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:edit')")
    @Log(title = "业主进款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SzgcYzJk szgcYzJk)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getUserName();

        szgcYzJk.setUpdateBy(userName);
        return toAjax(szgcYzJkService.updateSzgcYzJk(szgcYzJk));
    }

    /**
     * 删除业主进款
     */
    @PreAuthorize("@ss.hasPermi('szgc:yzpayment:remove')")
    @Log(title = "业主进款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jkIds}")
    public AjaxResult remove(@PathVariable Long[] jkIds)
    {
        return toAjax(szgcYzJkService.deleteSzgcYzJkByIds(jkIds));
    }
}
