package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzCz;

/**
 * 业主产值Mapper接口
 * 
 * @author lhh
 * @date 2020-12-17
 */
public interface SzgcYzCzMapper 
{
    /**
     * 查询业主产值
     * 
     * @param czId 业主产值ID
     * @return 业主产值
     */
    public SzgcYzCz selectSzgcYzCzById(Long czId);

    /**
     * 查询业主产值列表
     * 
     * @param szgcYzCz 业主产值
     * @return 业主产值集合
     */
    public List<SzgcYzCz> selectSzgcYzCzList(SzgcYzCz szgcYzCz);

    /**
     * 新增业主产值
     * 
     * @param szgcYzCz 业主产值
     * @return 结果
     */
    public int insertSzgcYzCz(SzgcYzCz szgcYzCz);

    /**
     * 修改业主产值
     * 
     * @param szgcYzCz 业主产值
     * @return 结果
     */
    public int updateSzgcYzCz(SzgcYzCz szgcYzCz);

    /**
     * 删除业主产值
     * 
     * @param czId 业主产值ID
     * @return 结果
     */
    public int deleteSzgcYzCzById(Long czId);

    /**
     * 批量删除业主产值
     * 
     * @param czIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzCzByIds(Long[] czIds);
}
