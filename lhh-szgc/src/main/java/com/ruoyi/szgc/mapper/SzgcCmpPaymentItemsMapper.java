package com.ruoyi.szgc.mapper;

import com.ruoyi.szgc.domain.SzgcCmpPaymentItems;

import java.util.List;

/**
 * 业主计量明细Mapper接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface SzgcCmpPaymentItemsMapper
{
    /**
     * 查询业主计量明细
     * 
     * @param jlId 业主计量明细ID
     * @return 业主计量明细
     */
    public SzgcCmpPaymentItems selectSzgcCmpPaymentItemsById(Long jlId);

    /**
     * 查询业主计量明细列表
     * 
     * @param szgcCmpPaymentItems 业主计量明细
     * @return 业主计量明细集合
     */
    public List<SzgcCmpPaymentItems> selectSzgcCmpPaymentItemsList(SzgcCmpPaymentItems szgcCmpPaymentItems);

    /**
     * 新增业主计量明细
     * 
     * @param szgcCmpPaymentItems 业主计量明细
     * @return 结果
     */
    public int insertSzgcCmpPaymentItems(SzgcCmpPaymentItems szgcCmpPaymentItems);

    /**
     * 修改业主计量明细
     * 
     * @param szgcCmpPaymentItems 业主计量明细
     * @return 结果
     */
    public int updateSzgcCmpPaymentItems(SzgcCmpPaymentItems szgcCmpPaymentItems);

    /**
     * 删除业主计量明细
     * 
     * @param jlId 业主计量明细ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentItemsById(Long jlId);

    /**
     * 批量删除业主计量明细
     * 
     * @param jlIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentItemsByIds(Long[] jlIds);
}
