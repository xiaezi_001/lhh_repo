package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcContract;

/**
 * 合同Mapper接口
 *
 * @author lhh
 * @date 2020-09-17
 */
public interface SzgcContractMapper
{
    /**
     * 查询合同
     *
     * @param conId 合同ID
     * @return 合同
     */
    public SzgcContract selectSzgcContractById(Long conId);

    /**
     * 查询合同列表
     *
     * @param szgcContract 合同
     * @return 合同集合
     */
    public List<SzgcContract> selectSzgcContractList(SzgcContract szgcContract);

    /**
     * 新增合同
     *
     * @param szgcContract 合同
     * @return 结果
     */
    public int insertSzgcContract(SzgcContract szgcContract);

    /**
     * 修改合同
     *
     * @param szgcContract 合同
     * @return 结果
     */
    public int updateSzgcContract(SzgcContract szgcContract);

    /**
     * 删除合同
     *
     * @param conId 合同ID
     * @return 结果
     */
    public int deleteSzgcContractById(Long conId);

    /**
     * 批量删除合同
     *
     * @param conIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcContractByIds(Long[] conIds);
}
