package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzPaymentCmp;
import org.apache.ibatis.annotations.Param;

/**
 * 业主计量分公司Mapper接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface SzgcYzPaymentCmpMapper 
{
    /**
     * 查询业主计量分公司
     * 
     * @param jlCmpId 业主计量分公司ID
     * @return 业主计量分公司
     */
    public SzgcYzPaymentCmp selectSzgcYzPaymentCmpById(Long jlCmpId);

    /**
     * 查询业主计量分公司列表
     * 
     * @param szgcYzPaymentCmp 业主计量分公司
     * @return 业主计量分公司集合
     */
    public List<SzgcYzPaymentCmp> selectSzgcYzPaymentCmpList(SzgcYzPaymentCmp szgcYzPaymentCmp);

    /**
     * 新增业主计量分公司
     * 
     * @param szgcYzPaymentCmp 业主计量分公司
     * @return 结果
     */
    public int insertSzgcYzPaymentCmp(SzgcYzPaymentCmp szgcYzPaymentCmp);

    /**
     * 修改业主计量分公司
     * 
     * @param szgcYzPaymentCmp 业主计量分公司
     * @return 结果
     */
    public int updateSzgcYzPaymentCmp(SzgcYzPaymentCmp szgcYzPaymentCmp);

    /**
     * 删除业主计量分公司
     * 
     * @param jlCmpId 业主计量分公司ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentCmpById(Long jlCmpId);

    /**
     * 批量删除业主计量分公司
     * 
     * @param jlCmpIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentCmpByIds(Long[] jlCmpIds);

    List<SzgcYzPaymentCmp> getYzPaymentCmpListByJlId(@Param("jlId") Long jlId, @Param("ptype") Integer ptype);

    Integer checkHasPaymentCmp(@Param("jlId") Long jlId, @Param("cmpId") Integer cmpId, @Param("ptype") Integer ptype);
}
