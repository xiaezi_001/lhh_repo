package com.ruoyi.szgc.mapper;

import java.util.List;

import com.ruoyi.szgc.domain.SzgcJLSumMoney;
import com.ruoyi.szgc.domain.SzgcYzPayment;

/**
 * 业主计量Mapper接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface SzgcYzPaymentMapper 
{
    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    public SzgcYzPayment selectSzgcYzPaymentById(Long jlId);

    SzgcYzPayment selectSzgcYzPaymentJS(Long conId);

    public List<SzgcYzPayment> selectSzgcYzPaymentByConId(Long conId);

    /**
     * 查询业主计量列表
     * 
     * @param szgcYzPayment 业主计量
     * @return 业主计量集合
     */
    public List<SzgcYzPayment> selectSzgcYzPaymentList(SzgcYzPayment szgcYzPayment);

    /**
     * 新增业主计量
     * 
     * @param szgcYzPayment 业主计量
     * @return 结果
     */
    public int insertSzgcYzPayment(SzgcYzPayment szgcYzPayment);

    /**
     * 修改业主计量
     * 
     * @param szgcYzPayment 业主计量
     * @return 结果
     */
    public int updateSzgcYzPayment(SzgcYzPayment szgcYzPayment);

    /**
     * 删除业主计量
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentById(Long jlId);

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentByIds(Long[] jlIds);

    /* jl sum */
    SzgcJLSumMoney selectCZSum(Long conId);
    SzgcJLSumMoney selectJLSum(Long conId);
    SzgcJLSumMoney selectJKSum(Long conId);
    SzgcJLSumMoney selectBKSum(Long conId);
}
