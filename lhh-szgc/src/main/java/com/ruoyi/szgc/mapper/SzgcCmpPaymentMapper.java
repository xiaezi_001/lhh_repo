package com.ruoyi.szgc.mapper;

import com.ruoyi.szgc.domain.SzgcCmpPayment;
import com.ruoyi.szgc.domain.SzgcJLSumMoney;

import java.util.List;

/**
 * 业主计量Mapper接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface SzgcCmpPaymentMapper
{
    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    public SzgcCmpPayment selectSzgcCmpPaymentById(Long jlId);

    SzgcCmpPayment selectSzgcCmpPaymentJS(Long conId);

    public List<SzgcCmpPayment> selectSzgcCmpPaymentByConId(Long conId);

    /**
     * 查询业主计量列表
     * 
     * @param szgcCmpPayment 业主计量
     * @return 业主计量集合
     */
    public List<SzgcCmpPayment> selectSzgcCmpPaymentList(SzgcCmpPayment szgcCmpPayment);

    /**
     * 新增业主计量
     * 
     * @param szgcCmpPayment 业主计量
     * @return 结果
     */
    public int insertSzgcCmpPayment(SzgcCmpPayment szgcCmpPayment);

    /**
     * 修改业主计量
     * 
     * @param szgcCmpPayment 业主计量
     * @return 结果
     */
    public int updateSzgcCmpPayment(SzgcCmpPayment szgcCmpPayment);

    /**
     * 删除业主计量
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentById(Long jlId);

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentByIds(Long[] jlIds);

    /* jl sum */
    SzgcJLSumMoney selectJlSumByConId(Long conId);
}
