package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzUpload;

/**
 * 计量上传文件Mapper接口
 * 
 * @author lhh
 * @date 2020-10-29
 */
public interface SzgcYzUploadMapper 
{
    /**
     * 查询计量上传文件
     * 
     * @param uploadId 计量上传文件ID
     * @return 计量上传文件
     */
    public SzgcYzUpload selectSzgcYzUploadById(Long uploadId);

    /**
     * 查询计量上传文件列表
     * 
     * @param szgcYzUpload 计量上传文件
     * @return 计量上传文件集合
     */
    public List<SzgcYzUpload> selectSzgcYzUploadList(SzgcYzUpload szgcYzUpload);

    /**
     * 新增计量上传文件
     * 
     * @param szgcYzUpload 计量上传文件
     * @return 结果
     */
    public int insertSzgcYzUpload(SzgcYzUpload szgcYzUpload);

    /**
     * 修改计量上传文件
     * 
     * @param szgcYzUpload 计量上传文件
     * @return 结果
     */
    public int updateSzgcYzUpload(SzgcYzUpload szgcYzUpload);

    /**
     * 删除计量上传文件
     * 
     * @param uploadId 计量上传文件ID
     * @return 结果
     */
    public int deleteSzgcYzUploadById(Long uploadId);

    /**
     * 批量删除计量上传文件
     * 
     * @param uploadIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzUploadByIds(Long[] uploadIds);
}
