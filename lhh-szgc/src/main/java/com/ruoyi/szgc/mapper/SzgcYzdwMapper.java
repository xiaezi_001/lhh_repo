package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzdw;

/**
 * 业主单位Mapper接口
 * 
 * @author lhh
 * @date 2020-09-19
 */
public interface SzgcYzdwMapper 
{
    /**
     * 查询业主单位
     * 
     * @param yzId 业主单位ID
     * @return 业主单位
     */
    public SzgcYzdw selectSzgcYzdwById(Long yzId);

    /**
     * 查询业主单位列表
     * 
     * @param szgcYzdw 业主单位
     * @return 业主单位集合
     */
    public List<SzgcYzdw> selectSzgcYzdwList(SzgcYzdw szgcYzdw);

    /**
     * 新增业主单位
     * 
     * @param szgcYzdw 业主单位
     * @return 结果
     */
    public int insertSzgcYzdw(SzgcYzdw szgcYzdw);

    /**
     * 修改业主单位
     * 
     * @param szgcYzdw 业主单位
     * @return 结果
     */
    public int updateSzgcYzdw(SzgcYzdw szgcYzdw);

    /**
     * 删除业主单位
     * 
     * @param yzId 业主单位ID
     * @return 结果
     */
    public int deleteSzgcYzdwById(Long yzId);
    public int deleteSzgcYzdwByParenId(Long yzId);
    /**
     * 批量删除业主单位
     * 
     * @param yzIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzdwByIds(Long[] yzIds);
}
