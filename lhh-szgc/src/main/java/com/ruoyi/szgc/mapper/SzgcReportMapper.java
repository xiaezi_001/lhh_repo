package com.ruoyi.szgc.mapper;

import com.ruoyi.szgc.domain.*;

import java.util.List;

public interface SzgcReportMapper {

    List<SzgcRepYbb> selectYbb(SzgcRepSearch search);

    List<SzgcRepNBZQ> selectNBZQ(SzgcRepSearch search);

    List<SzgcRepBK1> selectBK_1(SzgcRepSearch search);

    List<SzgcRepBK2> selectBK_2(SzgcRepSearch search);

    List<SzgcRepBK3> selectBK_3(SzgcRepSearch search);

    List<SzgcRepBK2> selectBK_4(SzgcRepSearch search);

    List<SzgcRepZQ1> selectZQ_1(SzgcRepSearch search);

    List<SzgcRepZQ2> selectZQ_2(SzgcRepSearch search);

    List<SzgcRepZQ2> selectZQ_3(SzgcRepSearch search);

    List<SzgcSearchResult> selectSearch(SzgcRepSearch search);

    List<SzgcSearchDetail> selectResultDetail(SzgcSearchDetail detail);

    List<SzgcSearchDetail> selectResultCZDetail(SzgcSearchDetail detail);
    List<SzgcSearchDetail> selectResultJSDetail(SzgcSearchDetail detail);
    List<SzgcSearchDetail> selectResultJLDetail(SzgcSearchDetail detail);
    List<SzgcSearchDetail> selectResultJKDetail(SzgcSearchDetail detail);
    List<SzgcSearchDetail> selectResultBKDetail(SzgcSearchDetail detail);

    List<SzgcMainSumMoney> selectMainSum();
}
