package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzJl;

/**
 * 业主计量Mapper接口
 * 
 * @author lhh
 * @date 2020-11-29
 */
public interface SzgcYzJlMapper 
{
    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    public SzgcYzJl selectSzgcYzJlById(Long jlId);
    SzgcYzJl selectSzgcYzJSByConId(Long conId);
    /**
     * 查询业主计量列表
     * 
     * @param szgcYzJl 业主计量
     * @return 业主计量集合
     */
    public List<SzgcYzJl> selectSzgcYzJlList(SzgcYzJl szgcYzJl);

    /**
     * 新增业主计量
     * 
     * @param szgcYzJl 业主计量
     * @return 结果
     */
    public int insertSzgcYzJl(SzgcYzJl szgcYzJl);

    /**
     * 修改业主计量
     * 
     * @param szgcYzJl 业主计量
     * @return 结果
     */
    public int updateSzgcYzJl(SzgcYzJl szgcYzJl);

    /**
     * 删除业主计量
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    public int deleteSzgcYzJlById(Long jlId);

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzJlByIds(Long[] jlIds);
}
