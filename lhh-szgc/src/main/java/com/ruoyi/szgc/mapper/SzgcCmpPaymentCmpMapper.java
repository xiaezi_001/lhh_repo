package com.ruoyi.szgc.mapper;

import com.ruoyi.szgc.domain.SzgcCmpPaymentCmp;

import java.util.List;

/**
 * 业主计量分公司Mapper接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface SzgcCmpPaymentCmpMapper
{
    /**
     * 查询业主计量分公司
     * 
     * @param jlCmpId 业主计量分公司ID
     * @return 业主计量分公司
     */
    public SzgcCmpPaymentCmp selectSzgcCmpPaymentCmpById(Long jlCmpId);

    /**
     * 查询业主计量分公司列表
     * 
     * @param szgcCmpPaymentCmp 业主计量分公司
     * @return 业主计量分公司集合
     */
    public List<SzgcCmpPaymentCmp> selectSzgcCmpPaymentCmpList(SzgcCmpPaymentCmp szgcCmpPaymentCmp);

    /**
     * 新增业主计量分公司
     * 
     * @param szgcCmpPaymentCmp 业主计量分公司
     * @return 结果
     */
    public int insertSzgcCmpPaymentCmp(SzgcCmpPaymentCmp szgcCmpPaymentCmp);

    /**
     * 修改业主计量分公司
     * 
     * @param szgcCmpPaymentCmp 业主计量分公司
     * @return 结果
     */
    public int updateSzgcCmpPaymentCmp(SzgcCmpPaymentCmp szgcCmpPaymentCmp);

    /**
     * 删除业主计量分公司
     * 
     * @param jlCmpId 业主计量分公司ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentCmpById(Long jlCmpId);

    /**
     * 批量删除业主计量分公司
     * 
     * @param jlCmpIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentCmpByIds(Long[] jlCmpIds);

    List<SzgcCmpPaymentCmp> getCmpPaymentCmpListByJlId(Long jlId);
}
