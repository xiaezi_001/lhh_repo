package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzJk;

/**
 * 业主进款Mapper接口
 * 
 * @author lhh
 * @date 2020-11-29
 */
public interface SzgcYzJkMapper 
{
    /**
     * 查询业主进款
     * 
     * @param jkId 业主进款ID
     * @return 业主进款
     */
    public SzgcYzJk selectSzgcYzJkById(Long jkId);
    SzgcYzJk selectLastSzgcYzJk();
    /**
     * 查询业主进款列表
     * 
     * @param szgcYzJk 业主进款
     * @return 业主进款集合
     */
    public List<SzgcYzJk> selectSzgcYzJkList(SzgcYzJk szgcYzJk);

    /**
     * 新增业主进款
     * 
     * @param szgcYzJk 业主进款
     * @return 结果
     */
    public int insertSzgcYzJk(SzgcYzJk szgcYzJk);

    /**
     * 修改业主进款
     * 
     * @param szgcYzJk 业主进款
     * @return 结果
     */
    public int updateSzgcYzJk(SzgcYzJk szgcYzJk);

    /**
     * 删除业主进款
     * 
     * @param jkId 业主进款ID
     * @return 结果
     */
    public int deleteSzgcYzJkById(Long jkId);

    /**
     * 批量删除业主进款
     * 
     * @param jkIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzJkByIds(Long[] jkIds);
}
