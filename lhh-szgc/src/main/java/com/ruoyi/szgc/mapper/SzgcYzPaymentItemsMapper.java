package com.ruoyi.szgc.mapper;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzPaymentItems;

/**
 * 业主计量明细Mapper接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface SzgcYzPaymentItemsMapper 
{
    /**
     * 查询业主计量明细
     * 
     * @param jlId 业主计量明细ID
     * @return 业主计量明细
     */
    public SzgcYzPaymentItems selectSzgcYzPaymentItemsById(Long jlId);

    /**
     * 查询业主计量明细列表
     * 
     * @param szgcYzPaymentItems 业主计量明细
     * @return 业主计量明细集合
     */
    public List<SzgcYzPaymentItems> selectSzgcYzPaymentItemsList(SzgcYzPaymentItems szgcYzPaymentItems);

    /**
     * 新增业主计量明细
     * 
     * @param szgcYzPaymentItems 业主计量明细
     * @return 结果
     */
    public int insertSzgcYzPaymentItems(SzgcYzPaymentItems szgcYzPaymentItems);

    /**
     * 修改业主计量明细
     * 
     * @param szgcYzPaymentItems 业主计量明细
     * @return 结果
     */
    public int updateSzgcYzPaymentItems(SzgcYzPaymentItems szgcYzPaymentItems);

    /**
     * 删除业主计量明细
     * 
     * @param jlId 业主计量明细ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentItemsById(Long jlId);

    /**
     * 批量删除业主计量明细
     * 
     * @param jlIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentItemsByIds(Long[] jlIds);
}
