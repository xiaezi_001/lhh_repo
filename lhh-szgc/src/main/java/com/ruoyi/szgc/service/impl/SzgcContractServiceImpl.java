package com.ruoyi.szgc.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.service.ISzgcContractService;

/**
 * 合同Service业务层处理
 * 
 * @author lhh
 * @date 2020-09-17
 */
@Service
public class SzgcContractServiceImpl implements ISzgcContractService 
{
    @Autowired
    private SzgcYzdwMapper yzdwMapper;

    @Autowired
    private SzgcContractMapper szgcContractMapper;

    @Autowired
    private SzgcYzCzPlanMapper czPlanMapper;

    @Autowired
    private SzgcYzCzMapper czMapper;

    @Autowired
    private SzgcYzJlMapper jlMapper;

    @Autowired
    private SzgcYzJkMapper jkMapper;

    @Autowired
    private SzgcYzBkMapper bkMapper;

    /**
     * 查询合同
     * 
     * @param conId 合同ID
     * @return 合同
     */
    @Override
    public SzgcContract selectSzgcContractById(Long conId)
    {
        return szgcContractMapper.selectSzgcContractById(conId);
    }

    /**
     * 查询合同列表
     * 
     * @param szgcContract 合同
     * @return 合同
     */
    @Override
    public List<SzgcContract> selectSzgcContractList(SzgcContract szgcContract)
    {
        SzgcYzdw yzdw = null;

        if(szgcContract.getConYz() != null){
            yzdw = yzdwMapper.selectSzgcYzdwById(szgcContract.getConYz());
        }

        if(yzdw != null && yzdw.getParentId() == 0)
        {
            szgcContract.setConYz(null);
            szgcContract.setYzParent(yzdw.getYzId());
        }

        return szgcContractMapper.selectSzgcContractList(szgcContract);
    }

    /**
     * 新增合同
     * 
     * @param szgcContract 合同
     * @return 结果
     */
    @Override
    public int insertSzgcContract(SzgcContract szgcContract)
    {
        return szgcContractMapper.insertSzgcContract(szgcContract);
    }

    /**
     * 修改合同
     * 
     * @param szgcContract 合同
     * @return 结果
     */
    @Override
    public int updateSzgcContract(SzgcContract szgcContract)
    {
        return szgcContractMapper.updateSzgcContract(szgcContract);
    }

    /**
     * 批量删除合同
     * 
     * @param conIds 需要删除的合同ID
     * @return 结果
     */
    @Override
    public int deleteSzgcContractByIds(Long[] conIds) throws Exception {
        for (Long cid: conIds) {
            if(cannotDelete(cid)){
                throw new Exception("合同存在计量数据，不能删除");
            }
        }

        return szgcContractMapper.deleteSzgcContractByIds(conIds);
    }

    /**
     * 删除合同信息
     * 
     * @param conId 合同ID
     * @return 结果
     */
    @Override
    public int deleteSzgcContractById(Long conId) throws Exception {
        if(cannotDelete(conId)){
            throw new Exception("合同存在计量数据，不能删除");
        }

        return szgcContractMapper.deleteSzgcContractById(conId);
    }

    @Override
    public List<SzgcSelectList> getSelectConByName(String conName, Integer ctype) {

        List<SzgcSelectList> conList = new ArrayList<>();
        SzgcContract queryCon = new SzgcContract();

        queryCon.setConName(conName);
        queryCon.setConType(ctype);
        List<SzgcContract> contracts = szgcContractMapper.selectSzgcContractList(queryCon);

        contracts.forEach(con -> {
            SzgcSelectList item = new SzgcSelectList();
            item.setValue(con.getConId().toString());
            item.setLabel(con.getConName());

            conList.add(item);
        });

        return conList;
    }

    private Boolean cannotDelete(Long conId){
        SzgcYzCzPlan czPlan = new SzgcYzCzPlan();
        SzgcYzCz cz = new SzgcYzCz();
        SzgcYzJl jl = new SzgcYzJl();
        SzgcYzJk jk = new SzgcYzJk();
        SzgcYzBk bk = new SzgcYzBk();

        czPlan.setConId(conId);
        cz.setConId(conId);
        jl.setConId(conId);
        jk.setConId(conId);
        bk.setConId(conId);

        List<SzgcYzCzPlan> czPlans = czPlanMapper.selectSzgcYzCzPlanList(czPlan);
        List<SzgcYzCz> czs = czMapper.selectSzgcYzCzList(cz);
        List<SzgcYzJl> jls = jlMapper.selectSzgcYzJlList(jl);
        List<SzgcYzJk> jks = jkMapper.selectSzgcYzJkList(jk);
        List<SzgcYzBk> bks = bkMapper.selectSzgcYzBkList(bk);

        return (czPlans.size() + czs.size() + jls.size() + jks.size() + bks.size()) > 0;
    }
}
