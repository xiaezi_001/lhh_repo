package com.ruoyi.szgc.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import com.ruoyi.szgc.domain.*;

/**
 * 业主计量Service接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface ISzgcYzPaymentService 
{
    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    public SzgcJLPayment selectSzgcYzPaymentById(Long jlId);

    public List<SzgcYzPayment> selectSzgcYzPaymentByConId(Long conId);

    /**
     * 查询业主计量列表
     * 
     * @param yzPayment 业主计量
     * @return 业主计量集合
     */
    public List<SzgcYzPayment> selectSzgcYzPaymentList(SzgcYzPayment yzPayment);

    /**
     * 新增业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    public int insertSzgcYzPayment(SzgcJLPayment jlPayment) throws ParseException;

    /**
     * 修改业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    public int updateSzgcYzPayment(SzgcJLPayment jlPayment) throws ParseException;

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的业主计量ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentByIds(Long[] jlIds);

    /**
     * 删除业主计量信息
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    public int deleteSzgcYzPaymentById(Long jlId);

    SzgcJLSumMoney getJLSumByConId(Long conId);

    List<SzgcYzPaymentCmp> getCmpPaymentList(Long jlId);

    int saveYZJLCmpPayment(List<SzgcYzPaymentCmp> yzcmps);

    BigDecimal getLastYZJlSLRate(Long conId);
    SzgcJLPayment getLastYZJLItem(Long conId);
    int saveUploadFile(SzgcYzUpload upload);

    List<SzgcYzUpload> getUploadFiles(Long jlId, Integer fileType);
    SzgcYzUpload getUploadFileById(Long fileId);
    int deleteUpload(Long uploadId);
}
