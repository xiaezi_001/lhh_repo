package com.ruoyi.szgc.service.impl;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.mapper.SzgcBranchMapper;
import com.ruoyi.szgc.mapper.SzgcReportMapper;
import com.ruoyi.szgc.mapper.SzgcYzdwMapper;
import com.ruoyi.szgc.service.ISzgcContractService;
import com.ruoyi.szgc.service.ISzgcExport;
import com.ruoyi.szgc.service.ISzgcYzPaymentService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.jni.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service("exportExcel")
public class SzgcExportExcel implements ISzgcExport
{

    @Autowired
    private ISzgcYzPaymentService paymentService;

    @Autowired
    private ISzgcContractService contractService;

    @Autowired
    private SzgcReportMapper reportMapper;

    @Autowired
    private SzgcBranchMapper cmpMapper;

    @Autowired
    private SzgcYzdwMapper yzdwMapper;

    private List<SzgcJLPayment> paymentList;

    private CellStyle cellText;
    private CellStyle cellMoney;

    @Override
    public AjaxResult exportPayDetail(Long[] jlIds) {
        ExcelUtil excel = new ExcelUtil();

        //load jl data
        loadPayDetailData(jlIds);

        if(paymentList.size() == 0){ return AjaxResult.error("No export data"); }

        SzgcContract con = contractService.selectSzgcContractById(paymentList.get(0).getConId());

        //create excel workbook & work sheet
        excel.createWorkbook();
        excel.createPDCellStyel();
        int shIndex = 0;
        for (SzgcJLPayment p : paymentList) {
            excel.createSheet(p.getJlCustomNum());
            excel.setPDColumn();
            excel.setPDTitle1("天津市公路工程总公司第二分中心项目拨款传递单", 0);
            excel.setPDTitle1("第二分中心项目管理室存根", 1);

            shIndex++;
        }
        return excel.exportExcelDirect("test");

    }

    @Override
    public AjaxResult exportYbb(SzgcRepSearch query) {

        query.setConType(0);
        List<SzgcRepYbb> ybbZZ = reportMapper.selectYbb(query);
        query.setConType(1);
        List<SzgcRepYbb> ybbHZ = reportMapper.selectYbb(query);

        SzgcRepYbb ybbSumZZ = new SzgcRepYbb();
        ybbSumZZ.reset();
        SzgcRepYbb ybbSumHZ = new SzgcRepYbb();
        ybbSumHZ.reset();

        //region create new excel file
        String tmpStr = RuoYiConfig.getTemplatePath() + "/ybb.xlsx";
        String target = RuoYiConfig.getExportPath() + "/ybb" + StringUtils.getTimeStampString() +  ".xlsx";
        //String target = RuoYiConfig.getDownloadPath() + "ybb" + StringUtils.getTimeStampString() +  ".xlsx";

        Path srcPath = Paths.get(tmpStr);
        Path desPath = Paths.get(target);
        //FileUtils.copyFile(tmp, target);
        try {
            Files.copy(srcPath, desPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            return AjaxResult.error("月报表模板错误，请联系管理员!");
        }
        //endregion

        //handle excel
        Workbook workbook = getWorkbook(target);
        if(workbook == null){
            return AjaxResult.error("月报表模板错误，请联系管理员!");
        }
        String tmpY = query.getYear().toString().substring(2);

        //region set Header column
        Sheet sh = workbook.getSheetAt(0);
        sh.getRow(0).getCell(0).setCellValue("第二分中心" + query.getMonth() + "月份报表");
        Row r3 = sh.getRow(3);
        r3.getCell(7).setCellValue(tmpY + "年计划产值");
        r3.getCell(8).setCellValue(query.getMonth() + "月完成产值");
        r3.getCell(9).setCellValue(tmpY + "年累计产值");
        r3.getCell(10).setCellValue(tmpY + "年剩余产值");
        r3.getCell(12).setCellValue(query.getMonth() + "月新增计量");
        r3.getCell(13).setCellValue(tmpY + "年累计计量额");
        r3.getCell(15).setCellValue(query.getMonth() + "月进款");
        r3.getCell(16).setCellValue(tmpY + "年累计进款");
        r3.getCell(18).setCellValue(tmpY + "年累计拨付");
        //endregion

        Integer rowStart = 4, rowEnd = 0;
        Integer rowIndex = 4;
        Row tmpRow = null;
        //自主项目
        for (SzgcRepYbb d: ybbZZ) {
            //region zzxm loop body
            d.setCzRemain(d.getCzPlan().subtract(d.getCzSum()));
            d.setCzYearRemain(d.getCzYearPlan().subtract(d.getCzYear()));
            if(d.getJsMoney().compareTo(new BigDecimal(0)) > -1){
                //>= 0
                d.setJlSum(d.getJsMoney());
            }

            //tmpRow = sh.getRow(rowIndex);
            tmpRow = sh.createRow(rowIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzdw());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getConStatus());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            //cz
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getCzSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getCzRemain()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getCzYearPlan()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getCzMonth()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getCzYear()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getCzYearRemain()));
            //jl
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getJlMonth()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellMoney);
            tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(d.getJlYear()));
            //jk
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellMoney);
            tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellMoney);
            tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(d.getJkMonth()));
            tmpRow.createCell(16);
            tmpRow.getCell(16).setCellStyle(cellMoney);
            tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(d.getJkYear()));
            //bk
            tmpRow.createCell(17);
            tmpRow.getCell(17).setCellStyle(cellMoney);
            tmpRow.getCell(17).setCellValue(StringUtils.getMoneyString(d.getBkSum()));
            tmpRow.createCell(18);
            tmpRow.getCell(18).setCellStyle(cellMoney);
            tmpRow.getCell(18).setCellValue(StringUtils.getMoneyString(d.getBkYear()));

            //memo
            tmpRow.createCell(19);
            tmpRow.getCell(19).setCellStyle(cellMoney);
            tmpRow.getCell(19).setCellValue("");

            //sum
            ybbSumZZ.setConMoney(ybbSumZZ.getConMoney().add(d.getConMoney()));
            ybbSumZZ.setCzSum (ybbSumZZ.getCzSum().add(d.getCzSum()));
            ybbSumZZ.setCzRemain(ybbSumZZ.getCzRemain().add(d.getCzRemain()));
            ybbSumZZ.setCzYearPlan(ybbSumZZ.getCzYearPlan().add(d.getCzYearPlan()));
            ybbSumZZ.setCzMonth(ybbSumZZ.getCzMonth().add(d.getCzMonth()));
            ybbSumZZ.setCzYear(ybbSumZZ.getCzYear().add(d.getCzYear()));
            ybbSumZZ.setCzYearRemain(ybbSumZZ.getCzYearRemain().add(d.getCzYearRemain()));
            ybbSumZZ.setJlSum(ybbSumZZ.getJlSum().add(d.getJlSum()));
            ybbSumZZ.setJlMonth(ybbSumZZ.getJlMonth().add(d.getJlMonth()));
            ybbSumZZ.setJlYear(ybbSumZZ.getJlYear().add(d.getJlYear()));
            ybbSumZZ.setJkSum(ybbSumZZ.getJkSum().add(d.getJkSum()));
            ybbSumZZ.setJkMonth(ybbSumZZ.getJkMonth().add(d.getJkMonth()));
            ybbSumZZ.setJkYear(ybbSumZZ.getJkYear().add(d.getJkYear()));
            ybbSumZZ.setBkSum(ybbSumZZ.getBkSum().add(d.getBkSum()));
            ybbSumZZ.setBkYear(ybbSumZZ.getBkYear().add(d.getBkYear()));

            rowIndex++;

            //endregion
        }
        rowEnd = rowIndex;

        //region 自主项目合计
        tmpRow = sh.createRow(rowEnd);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);

        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getConMoney()));
        //cz
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getCzSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getCzRemain()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getCzYearPlan()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getCzMonth()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getCzYear()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getCzYearRemain()));
        //jl
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getJlSum()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getJlMonth()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getJlYear()));
        //jk
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getJkSum()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellMoney);
        tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getJkMonth()));
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellMoney);
        tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getJkYear()));
        //bk
        tmpRow.createCell(17);
        tmpRow.getCell(17).setCellStyle(cellMoney);
        tmpRow.getCell(17).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getBkSum()));
        tmpRow.createCell(18);
        tmpRow.getCell(18).setCellStyle(cellMoney);
        tmpRow.getCell(18).setCellValue(StringUtils.getMoneyString(ybbSumZZ.getBkYear()));
        tmpRow.createCell(19);
        tmpRow.getCell(19).setCellStyle(cellMoney);
        tmpRow.getCell(19).setCellValue("");

        if(rowStart < rowEnd) {
            CellRangeAddress regType = new CellRangeAddress(rowStart, rowEnd, 0, 0);
            sh.addMergedRegion(regType);
        }
        sh.getRow(rowStart).getCell(0).setCellValue("自主项目");

        CellRangeAddress regSum = new CellRangeAddress(rowEnd, rowEnd, 1,3);
        sh.addMergedRegion(regSum);
        sh.getRow(rowEnd).getCell(1).setCellValue("小计");
        //endregion

        rowStart = rowEnd + 1;
        rowIndex = rowStart;
        //合作项目
        for (SzgcRepYbb d: ybbHZ) {
            //region zzxm loop body
            d.setCzRemain(d.getCzPlan().subtract(d.getCzSum()));
            d.setCzYearRemain(d.getCzYearPlan().subtract(d.getCzYear()));
            if(d.getJsMoney().compareTo(new BigDecimal(0)) > -1){
                //>= 0
                d.setJlSum(d.getJsMoney());
            }

            //tmpRow = sh.getRow(rowIndex);
            tmpRow = sh.createRow(rowIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzdw());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getConStatus());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            //cz
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getCzSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getCzRemain()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getCzYearPlan()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getCzMonth()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getCzYear()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getCzYearRemain()));
            //jl
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getJlMonth()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellMoney);
            tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(d.getJlYear()));
            //jk
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellMoney);
            tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellMoney);
            tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(d.getJkMonth()));
            tmpRow.createCell(16);
            tmpRow.getCell(16).setCellStyle(cellMoney);
            tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(d.getJkYear()));
            //bk
            tmpRow.createCell(17);
            tmpRow.getCell(17).setCellStyle(cellMoney);
            tmpRow.getCell(17).setCellValue(StringUtils.getMoneyString(d.getBkSum()));
            tmpRow.createCell(18);
            tmpRow.getCell(18).setCellStyle(cellMoney);
            tmpRow.getCell(18).setCellValue(StringUtils.getMoneyString(d.getBkYear()));

            //memo
            tmpRow.createCell(19);
            tmpRow.getCell(19).setCellStyle(cellMoney);
            tmpRow.getCell(19).setCellValue("");

            //sum
            ybbSumHZ.setConMoney(ybbSumHZ.getConMoney().add(d.getConMoney()));
            ybbSumHZ.setCzSum (ybbSumHZ.getCzSum().add(d.getCzSum()));
            ybbSumHZ.setCzRemain(ybbSumHZ.getCzRemain().add(d.getCzRemain()));
            ybbSumHZ.setCzYearPlan(ybbSumHZ.getCzYearPlan().add(d.getCzYearPlan()));
            ybbSumHZ.setCzMonth(ybbSumHZ.getCzMonth().add(d.getCzMonth()));
            ybbSumHZ.setCzYear(ybbSumHZ.getCzYear().add(d.getCzYear()));
            ybbSumHZ.setCzYearRemain(ybbSumHZ.getCzYearRemain().add(d.getCzYearRemain()));
            ybbSumHZ.setJlSum(ybbSumHZ.getJlSum().add(d.getJlSum()));
            ybbSumHZ.setJlMonth(ybbSumHZ.getJlMonth().add(d.getJlMonth()));
            ybbSumHZ.setJlYear(ybbSumHZ.getJlYear().add(d.getJlYear()));
            ybbSumHZ.setJkSum(ybbSumHZ.getJkSum().add(d.getJkSum()));
            ybbSumHZ.setJkMonth(ybbSumHZ.getJkMonth().add(d.getJkMonth()));
            ybbSumHZ.setJkYear(ybbSumHZ.getJkYear().add(d.getJkYear()));
            ybbSumHZ.setBkSum(ybbSumHZ.getBkSum().add(d.getBkSum()));
            ybbSumHZ.setBkYear(ybbSumHZ.getBkYear().add(d.getBkYear()));

            rowIndex++;

            //endregion
        }
        rowEnd = rowIndex;

        //region 合作项目合计
        tmpRow = sh.createRow(rowEnd);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);

        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getConMoney()));
        //cz
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getCzSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getCzRemain()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getCzYearPlan()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getCzMonth()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getCzYear()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getCzYearRemain()));
        //jl
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getJlSum()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getJlMonth()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getJlYear()));
        //jk
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getJkSum()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellMoney);
        tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getJkMonth()));
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellMoney);
        tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getJkYear()));
        //bk
        tmpRow.createCell(17);
        tmpRow.getCell(17).setCellStyle(cellMoney);
        tmpRow.getCell(17).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getBkSum()));
        tmpRow.createCell(18);
        tmpRow.getCell(18).setCellStyle(cellMoney);
        tmpRow.getCell(18).setCellValue(StringUtils.getMoneyString(ybbSumHZ.getBkYear()));
        tmpRow.createCell(19);
        tmpRow.getCell(19).setCellStyle(cellMoney);
        tmpRow.getCell(19).setCellValue("");

        if(rowStart < rowEnd) {
            CellRangeAddress regType2 = new CellRangeAddress(rowStart, rowEnd, 0, 0);
            sh.addMergedRegion(regType2);
        }
        sh.getRow(rowStart).getCell(0).setCellValue("合作项目");

        CellRangeAddress regSum2 = new CellRangeAddress(rowEnd, rowEnd, 1,3);
        sh.addMergedRegion(regSum2);
        sh.getRow(rowEnd).getCell(1).setCellValue("小计");
        //endregion

        //region 总计
        SzgcRepYbb ybbSum = new SzgcRepYbb();
        ybbSum.reset();
        rowEnd = rowEnd + 1;

        ybbSum.setConMoney(ybbSumZZ.getConMoney().add(ybbSumHZ.getConMoney()));
        ybbSum.setCzSum(ybbSumZZ.getCzSum().add(ybbSumHZ.getCzSum()));
        ybbSum.setCzRemain(ybbSumZZ.getCzRemain().add(ybbSumHZ.getCzRemain()));
        ybbSum.setCzYearPlan(ybbSumZZ.getCzYearPlan().add(ybbSumHZ.getCzYearPlan()));
        ybbSum.setCzMonth(ybbSumZZ.getCzMonth().add(ybbSumHZ.getCzMonth()));
        ybbSum.setCzYear(ybbSumZZ.getCzYear().add(ybbSumHZ.getCzYear()));
        ybbSum.setCzYearRemain(ybbSumZZ.getCzYearRemain().add(ybbSumHZ.getCzYearRemain()));
        ybbSum.setJlSum(ybbSumZZ.getJlSum().add(ybbSumHZ.getJlSum()));
        ybbSum.setJlMonth(ybbSumZZ.getJlMonth().add(ybbSumHZ.getJlMonth()));
        ybbSum.setJlYear(ybbSumZZ.getJlYear().add(ybbSumHZ.getJlYear()));
        ybbSum.setJkSum(ybbSumZZ.getJkSum().add(ybbSumHZ.getJkSum()));
        ybbSum.setJkMonth(ybbSumZZ.getJkMonth().add(ybbSumHZ.getJkMonth()));
        ybbSum.setJkYear(ybbSumZZ.getJkYear().add(ybbSumHZ.getJkYear()));
        ybbSum.setBkSum(ybbSumZZ.getBkSum().add(ybbSumHZ.getBkSum()));
        ybbSum.setBkYear(ybbSumZZ.getBkYear().add(ybbSumHZ.getBkYear()));

        tmpRow = sh.createRow(rowEnd);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);

        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(ybbSum.getConMoney()));
        //cz
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(ybbSum.getCzSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(ybbSum.getCzRemain()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(ybbSum.getCzYearPlan()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(ybbSum.getCzMonth()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(ybbSum.getCzYear()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(ybbSum.getCzYearRemain()));
        //jl
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(ybbSum.getJlSum()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(ybbSum.getJlMonth()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(ybbSum.getJlYear()));
        //jk
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(ybbSum.getJkSum()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellMoney);
        tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(ybbSum.getJkMonth()));
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellMoney);
        tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(ybbSum.getJkYear()));
        //bk
        tmpRow.createCell(17);
        tmpRow.getCell(17).setCellStyle(cellMoney);
        tmpRow.getCell(17).setCellValue(StringUtils.getMoneyString(ybbSum.getBkSum()));
        tmpRow.createCell(18);
        tmpRow.getCell(18).setCellStyle(cellMoney);
        tmpRow.getCell(18).setCellValue(StringUtils.getMoneyString(ybbSum.getBkYear()));
        tmpRow.createCell(19);
        tmpRow.getCell(19).setCellStyle(cellMoney);
        tmpRow.getCell(19).setCellValue("");

        CellRangeAddress regSum3 = new CellRangeAddress(rowEnd, rowEnd, 0,3);
        sh.addMergedRegion(regSum3);
        sh.getRow(rowEnd).getCell(0).setCellValue("总计");
        //endregion

        try {
            OutputStream out = new FileOutputStream(target);
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AjaxResult.success(target);
    }

    @Override
    public AjaxResult exportNBZQ(SzgcRepSearch query) {
        query.setStartDate(query.getStartDate() + " 00:00:00");
        query.setEndDate(query.getEndDate() + " 23:59:59");

        SzgcBranch branchQuery = new SzgcBranch();
        branchQuery.setDeleteMark(0);
        List<SzgcBranch> cmpList = cmpMapper.selectSzgcBranchList(branchQuery);

        //region create new excel file
        String tmpStr = RuoYiConfig.getTemplatePath() + "/nbzq.xlsx";
        String target = RuoYiConfig.getExportPath() + "/nbzq" + StringUtils.getTimeStampString() +  ".xlsx";

        Path srcPath = Paths.get(tmpStr);
        Path desPath = Paths.get(target);
        //FileUtils.copyFile(tmp, target);
        try {
            Files.copy(srcPath, desPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            return AjaxResult.error("内部债权报表模板错误，请联系管理员!");
        }
        //endregion

        Workbook workbook = getWorkbook(target);
        if(workbook == null){
            return AjaxResult.error("内部债权报表模板错误，请联系管理员!");
        }

        Boolean hasData = false;
        Integer liIndex = 0, liIndex2 = 0, shIndex = 0;
        Integer startRow = 2, endRow = 0;
        Sheet sh = null;
        Row tmpRow = null;
        for (SzgcBranch cmp: cmpList) {
            query.setCmpId(cmp.getBranchId());
            List<SzgcRepNBZQ> nbzqs = reportMapper.selectNBZQ(query);
            if(nbzqs.size() == 0){ continue; }

            hasData = true;
            SzgcRepNBZQ sum = new SzgcRepNBZQ();
            sum.reset();

            sh = workbook.cloneSheet(0);
            shIndex = workbook.getSheetIndex(sh);
            workbook.setSheetName(shIndex, cmp.getBranchName());

            liIndex2 = 2; startRow = 2; endRow = 0;
            for(SzgcRepNBZQ d: nbzqs) {
                //region detail
                tmpRow = sh.createRow(liIndex2);
                tmpRow.createCell(0);
                tmpRow.getCell(0).setCellStyle(cellText);
                tmpRow.getCell(0).setCellValue(cmp.getBranchName());

                tmpRow.createCell(1);
                tmpRow.getCell(1).setCellStyle(cellText);
                tmpRow.getCell(1).setCellValue(liIndex2 - 1);

                tmpRow.createCell(2);
                tmpRow.getCell(2).setCellStyle(cellText);
                tmpRow.getCell(2).setCellValue(d.getConName());

                tmpRow.createCell(3);
                tmpRow.getCell(3).setCellStyle(cellMoney);
                tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(d.getCmpMoney()));

                tmpRow.createCell(4);
                tmpRow.getCell(4).setCellStyle(cellText);
                tmpRow.getCell(4).setCellValue("");

                sum.setCmpMoney(sum.getCmpMoney().add(d.getCmpMoney()));
                //endregion

                liIndex2++;
            }
            endRow = liIndex2;

            //region sum
            tmpRow = sh.createRow(endRow);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);

            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);

            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);

            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellMoney);
            tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(sum.getCmpMoney()));

            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellText);

            CellRangeAddress regSum = new CellRangeAddress(endRow, endRow, 0, 2);
            sh.addMergedRegion(regSum);
            sh.getRow(endRow).getCell(0).setCellValue("合计");

            //endregion

            liIndex++;
        }
        //delete first sheet
        if(hasData) {
            workbook.removeSheetAt(0);
        }

        try {
            OutputStream out = new FileOutputStream(target);
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AjaxResult.success(target);
    }

    @Override
    public AjaxResult exportBK(SzgcRepSearch query) {
        //region create new excel file
        String tmpStr = RuoYiConfig.getTemplatePath() + "/bk.xlsx";
        String target = RuoYiConfig.getExportPath() + "/bk" + StringUtils.getTimeStampString() +  ".xlsx";

        Path srcPath = Paths.get(tmpStr);
        Path desPath = Paths.get(target);
        //FileUtils.copyFile(tmp, target);
        try {
            Files.copy(srcPath, desPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            return AjaxResult.error("公司拨款报表模板错误，请联系管理员!");
        }
        //endregion

        Workbook workbook = getWorkbook(target);
        if(workbook == null){
            return AjaxResult.error("公司拨款报表模板错误，请联系管理员!");
        }

        Sheet shbk1 = workbook.getSheetAt(0);
        Sheet shbk2 = workbook.getSheetAt(1);
        Sheet shbk3 = workbook.getSheetAt(2);
        Sheet shbk4 = workbook.getSheetAt(3);

        query.setStartDate(query.getStartDate() + " 00:00:00");
        query.setEndDate(query.getEndDate() + " 00:00:00");

        createBK01(shbk1, query);
        createBK02(shbk2, query);
        createBK03(shbk3, query);
        createBK04(shbk4, query);

        try {
            OutputStream out = new FileOutputStream(target);
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AjaxResult.success(target);
    }

    private void createBK01(Sheet sh, SzgcRepSearch query)
    {
        query.setConAccount(0);
        List<SzgcRepBK1> bk0List = reportMapper.selectBK_1(query);
        query.setConAccount(1);
        List<SzgcRepBK1> bk1List = reportMapper.selectBK_1(query);

        SzgcRepBK1 bkSum0 = new SzgcRepBK1();
        SzgcRepBK1 bkSum1 = new SzgcRepBK1();
        SzgcRepBK1 bkSum  = new SzgcRepBK1();

        bkSum0.reset();
        bkSum1.reset();
        bkSum.reset();

        Row tmpRow = null;
        Integer liIndex = 2, startRow = 2, endRow = 0;
        for(SzgcRepBK1 d: bk0List){
            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - 1);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getConName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellMoney);
            tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(d.getBkJan()));
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellMoney);
            tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(d.getBkFeb()));
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getBkMar()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getBkApr()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getBkMay()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getBkJun()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getBkJul()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getBkAug()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getBkSep()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getBkOct()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getBkNav()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellMoney);
            tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(d.getBkDec()));
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellMoney);
            tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(d.getBkYear()));
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellMoney);
            tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(d.getBkSum()));
            tmpRow.createCell(16);
            tmpRow.getCell(16).setCellStyle(cellText);
            tmpRow.getCell(16).setCellValue("");

            bkSum0.setBkJan(bkSum0.getBkJan().add(d.getBkJan()));
            bkSum0.setBkFeb(bkSum0.getBkFeb().add(d.getBkFeb()));
            bkSum0.setBkMar(bkSum0.getBkMar().add(d.getBkMar()));
            bkSum0.setBkApr(bkSum0.getBkApr().add(d.getBkApr()));
            bkSum0.setBkMay(bkSum0.getBkMay().add(d.getBkMay()));
            bkSum0.setBkJun(bkSum0.getBkJun().add(d.getBkJun()));
            bkSum0.setBkJul(bkSum0.getBkJul().add(d.getBkJul()));
            bkSum0.setBkAug(bkSum0.getBkAug().add(d.getBkAug()));
            bkSum0.setBkSep(bkSum0.getBkSep().add(d.getBkSep()));
            bkSum0.setBkOct(bkSum0.getBkOct().add(d.getBkOct()));
            bkSum0.setBkNav(bkSum0.getBkNav().add(d.getBkNav()));
            bkSum0.setBkDec(bkSum0.getBkDec().add(d.getBkDec()));
            bkSum0.setBkYear(bkSum0.getBkYear().add(d.getBkYear()));
            bkSum0.setBkSum(bkSum0.getBkDec().add(d.getBkSum()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellMoney);
        tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(bkSum0.getBkJan()));
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellMoney);
        tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(bkSum0.getBkFeb()));
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(bkSum0.getBkMar()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(bkSum0.getBkApr()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(bkSum0.getBkMay()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(bkSum0.getBkJun()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(bkSum0.getBkJul()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(bkSum0.getBkAug()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(bkSum0.getBkSep()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(bkSum0.getBkOct()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(bkSum0.getBkNav()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(bkSum0.getBkDec()));
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(bkSum0.getBkYear()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellMoney);
        tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(bkSum0.getBkSum()));
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellText);
        tmpRow.getCell(16).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum01 = new CellRangeAddress(endRow, endRow, 0, 1);
        sh.addMergedRegion(regSum01);
        sh.getRow(endRow).getCell(0).setCellValue("小计");

        if(startRow < endRow) {
            CellRangeAddress regSum02 = new CellRangeAddress(startRow, endRow, 16, 16);
            sh.addMergedRegion(regSum02);
        }
        sh.getRow(startRow).getCell(16).setCellValue("专户");
        //endregion

        //region 公司账户
        liIndex = endRow + 1;
        startRow = liIndex;
        endRow = 0;
        for(SzgcRepBK1 d: bk1List){
            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - startRow + 1);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getConName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellMoney);
            tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(d.getBkJan()));
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellMoney);
            tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(d.getBkFeb()));
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getBkMar()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getBkApr()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getBkMay()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getBkJun()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getBkJul()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getBkAug()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getBkSep()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getBkOct()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getBkNav()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellMoney);
            tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(d.getBkDec()));
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellMoney);
            tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(d.getBkYear()));
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellMoney);
            tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(d.getBkSum()));
            tmpRow.createCell(16);
            tmpRow.getCell(16).setCellStyle(cellText);
            tmpRow.getCell(16).setCellValue("");

            bkSum1.setBkJan(bkSum1.getBkJan().add(d.getBkJan()));
            bkSum1.setBkFeb(bkSum1.getBkFeb().add(d.getBkFeb()));
            bkSum1.setBkMar(bkSum1.getBkMar().add(d.getBkMar()));
            bkSum1.setBkApr(bkSum1.getBkApr().add(d.getBkApr()));
            bkSum1.setBkMay(bkSum1.getBkMay().add(d.getBkMay()));
            bkSum1.setBkJun(bkSum1.getBkJun().add(d.getBkJun()));
            bkSum1.setBkJul(bkSum1.getBkJul().add(d.getBkJul()));
            bkSum1.setBkAug(bkSum1.getBkAug().add(d.getBkAug()));
            bkSum1.setBkSep(bkSum1.getBkSep().add(d.getBkSep()));
            bkSum1.setBkOct(bkSum1.getBkOct().add(d.getBkOct()));
            bkSum1.setBkNav(bkSum1.getBkNav().add(d.getBkNav()));
            bkSum1.setBkDec(bkSum1.getBkDec().add(d.getBkDec()));
            bkSum1.setBkYear(bkSum1.getBkYear().add(d.getBkYear()));
            bkSum1.setBkSum(bkSum1.getBkDec().add(d.getBkSum()));

            liIndex++;
        }

        endRow = liIndex;

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellMoney);
        tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(bkSum1.getBkJan()));
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellMoney);
        tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(bkSum1.getBkFeb()));
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(bkSum1.getBkMar()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(bkSum1.getBkApr()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(bkSum1.getBkMay()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(bkSum1.getBkJun()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(bkSum1.getBkJul()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(bkSum1.getBkAug()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(bkSum1.getBkSep()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(bkSum1.getBkOct()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(bkSum1.getBkNav()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(bkSum1.getBkDec()));
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(bkSum1.getBkYear()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellMoney);
        tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(bkSum1.getBkSum()));
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellText);
        tmpRow.getCell(16).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum11 = new CellRangeAddress(endRow, endRow, 0, 1);
        sh.addMergedRegion(regSum11);
        sh.getRow(endRow).getCell(0).setCellValue("小计");

        if(startRow < endRow) {
            CellRangeAddress regSum12 = new CellRangeAddress(startRow, endRow, 16, 16);
            sh.addMergedRegion(regSum12);
        }
        sh.getRow(startRow).getCell(16).setCellValue("公司账户");
        //endregion

        //region 合计
        endRow += 1;

        bkSum.setBkJan(bkSum0.getBkJan().add(bkSum1.getBkJan()));
        bkSum.setBkFeb(bkSum0.getBkFeb().add(bkSum1.getBkFeb()));
        bkSum.setBkMar(bkSum0.getBkMar().add(bkSum1.getBkMar()));
        bkSum.setBkApr(bkSum0.getBkApr().add(bkSum1.getBkApr()));
        bkSum.setBkMay(bkSum0.getBkMay().add(bkSum1.getBkMay()));
        bkSum.setBkJun(bkSum0.getBkJun().add(bkSum1.getBkJun()));
        bkSum.setBkJul(bkSum0.getBkJul().add(bkSum1.getBkJul()));
        bkSum.setBkAug(bkSum0.getBkAug().add(bkSum1.getBkAug()));
        bkSum.setBkSep(bkSum0.getBkSep().add(bkSum1.getBkSep()));
        bkSum.setBkOct(bkSum0.getBkOct().add(bkSum1.getBkOct()));
        bkSum.setBkNav(bkSum0.getBkNav().add(bkSum1.getBkNav()));
        bkSum.setBkDec(bkSum0.getBkDec().add(bkSum1.getBkDec()));
        bkSum.setBkYear(bkSum0.getBkYear().add(bkSum1.getBkYear()));
        bkSum.setBkSum(bkSum0.getBkSum().add(bkSum1.getBkSum()));

        tmpRow = sh.createRow(endRow);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellMoney);
        tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(bkSum.getBkJan()));
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellMoney);
        tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(bkSum.getBkFeb()));
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(bkSum.getBkMar()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(bkSum.getBkApr()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(bkSum.getBkMay()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(bkSum.getBkJun()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(bkSum.getBkJul()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(bkSum.getBkAug()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(bkSum.getBkSep()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(bkSum.getBkOct()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(bkSum.getBkNav()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(bkSum.getBkDec()));
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(bkSum.getBkYear()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellMoney);
        tmpRow.getCell(15).setCellValue(StringUtils.getMoneyString(bkSum.getBkSum()));
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellText);
        tmpRow.getCell(16).setCellValue("");

        CellRangeAddress regSum = new CellRangeAddress(endRow, endRow, 0, 1);
        sh.addMergedRegion(regSum);
        sh.getRow(endRow).getCell(0).setCellValue("合计");

        //endregion

    }

    private void createBK02(Sheet sh, SzgcRepSearch query)
    {
        List<SzgcRepBK2> bkList = reportMapper.selectBK_2(query);

        SzgcRepBK2 bkSum = new SzgcRepBK2();
        bkSum.reset();

        Row tmpRow = null;
        Integer liIndex = 4, startRow = 4, endRow = 0;

        for(SzgcRepBK2 d: bkList){

            d.setJkKJSum(d.getJkSFSum().add(d.getJkGLFSum().add(d.getJkLRSum())));
            d.setCmpQFSum(d.getJkYBSum().subtract(d.getJkBKSum()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - 3);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getStartYear() + "-" + d.getEndYear());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getJkFGSSum()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getJkBGSSum()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getJkSFSum()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getJkGLFSum()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getJkLRSum()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getJkKJSum()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellMoney);
            tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(d.getJkYBSum()));
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellMoney);
            tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(d.getJkBKSum()));
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellText);
            tmpRow.getCell(15).setCellValue(d.getLastBKDate());
            tmpRow.createCell(16);
            tmpRow.getCell(16).setCellStyle(cellMoney);
            tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(d.getCmpQFSum()));
            tmpRow.createCell(17);
            tmpRow.getCell(17).setCellStyle(cellText);
            tmpRow.getCell(17).setCellValue("");

            bkSum.setConMoney(bkSum.getConMoney().add(d.getConMoney()));
            bkSum.setJlSum(bkSum.getJlSum().add(d.getJlSum()));
            bkSum.setJkSum(bkSum.getJkSum().add(d.getJkSum()));
            bkSum.setJkFGSSum(bkSum.getJkFGSSum().add(d.getJkFGSSum()));
            bkSum.setJkBGSSum(bkSum.getJkBGSSum().add(d.getJkBGSSum()));
            bkSum.setJkSFSum(bkSum.getJkSFSum().add(d.getJkSFSum()));
            bkSum.setJkGLFSum(bkSum.getJkGLFSum().add(d.getJkGLFSum()));
            bkSum.setJkLRSum(bkSum.getJkLRSum().add(d.getJkLRSum()));
            bkSum.setJkKJSum(bkSum.getJkKJSum().add(d.getJkKJSum()));
            bkSum.setJkYBSum(bkSum.getJkYBSum().add(d.getJkYBSum()));
            bkSum.setJkBKSum(bkSum.getJkBKSum().add(d.getJkBKSum()));
            bkSum.setCmpQFSum(bkSum.getCmpQFSum().add(d.getCmpQFSum()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(bkSum.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(bkSum.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(bkSum.getJkSum()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(bkSum.getJkFGSSum()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(bkSum.getJkBGSSum()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(bkSum.getJkSFSum()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(bkSum.getJkGLFSum()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(bkSum.getJkLRSum()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(bkSum.getJkKJSum()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(bkSum.getJkYBSum()));
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(bkSum.getJkBKSum()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellText);
        tmpRow.getCell(15).setCellValue("");
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellMoney);
        tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(bkSum.getCmpQFSum()));
        tmpRow.createCell(17);
        tmpRow.getCell(17).setCellStyle(cellText);
        tmpRow.getCell(17).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum = new CellRangeAddress(endRow, endRow, 0, 4);
        sh.addMergedRegion(regSum);
        sh.getRow(endRow).getCell(0).setCellValue("合计");
        //endregion
    }

    private void createBK03(Sheet sh, SzgcRepSearch query)
    {
        List<SzgcRepBK3> bkList = reportMapper.selectBK_3(query);

        SzgcRepBK3 bkSum = new SzgcRepBK3();
        bkSum.reset();

        Row tmpRow = null;
        Integer liIndex = 2, startRow = 2, endRow = 0;

        for(SzgcRepBK3 d: bkList){
            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(d.getConName());
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getDateDis());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getTypeDis());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getJl()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJk()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getJkFGS()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getJkBGS()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getSf()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getGlf()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getLr()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getYb()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getCmpsf()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellText);
            tmpRow.getCell(13).setCellValue("");

            /*
            bkSum.setJl(bkSum.getJl().add(d.getJl()));
            bkSum.setJk(bkSum.getJk().add(d.getJk()));
            bkSum.setJkBGS(bkSum.getJkBGS().add(d.getJkBGS()));
            bkSum.setJkFGS(bkSum.getJkFGS().add(d.getJkFGS()));
            bkSum.setSf(bkSum.getSf().add(d.getSf()));
            bkSum.setGlf(bkSum.getGlf().add(d.getGlf()));
            bkSum.setLr(bkSum.getLr().add(d.getLr()));
            bkSum.setYb(bkSum.getYb().add(d.getYb()));
            bkSum.setCmpsf(bkSum.getCmpsf().add(d.getCmpsf()));
             */

            liIndex++;
        }
    }

    private void createBK04(Sheet sh, SzgcRepSearch query)
    {
        List<SzgcRepBK2> bkList = reportMapper.selectBK_4(query);

        SzgcRepBK2 bkSum = new SzgcRepBK2();
        bkSum.reset();

        Row tmpRow = null;
        Integer liIndex = 4, startRow = 4, endRow = 0;

        for(SzgcRepBK2 d: bkList){

            d.setJkKJSum(d.getJkSFSum().add(d.getJkGLFSum().add(d.getJkLRSum())));
            d.setCmpQFSum(d.getJkYBSum().subtract(d.getJkBKSum()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - 3);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getStartYear() + "-" + d.getEndYear());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getJkFGSSum()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getJkBGSSum()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellMoney);
            tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(d.getJkSFSum()));
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getJkGLFSum()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getJkLRSum()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getJkKJSum()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellMoney);
            tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(d.getJkYBSum()));
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellMoney);
            tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(d.getJkBKSum()));
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellText);
            tmpRow.getCell(15).setCellValue(d.getLastBKDate());
            tmpRow.createCell(16);
            tmpRow.getCell(16).setCellStyle(cellMoney);
            tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(d.getCmpQFSum()));
            tmpRow.createCell(17);
            tmpRow.getCell(17).setCellStyle(cellText);
            tmpRow.getCell(17).setCellValue("");

            bkSum.setConMoney(bkSum.getConMoney().add(d.getConMoney()));
            bkSum.setJlSum(bkSum.getJlSum().add(d.getJlSum()));
            bkSum.setJkSum(bkSum.getJkSum().add(d.getJkSum()));
            bkSum.setJkFGSSum(bkSum.getJkFGSSum().add(d.getJkFGSSum()));
            bkSum.setJkBGSSum(bkSum.getJkBGSSum().add(d.getJkBGSSum()));
            bkSum.setJkSFSum(bkSum.getJkSFSum().add(d.getJkSFSum()));
            bkSum.setJkGLFSum(bkSum.getJkGLFSum().add(d.getJkGLFSum()));
            bkSum.setJkLRSum(bkSum.getJkLRSum().add(d.getJkLRSum()));
            bkSum.setJkKJSum(bkSum.getJkKJSum().add(d.getJkKJSum()));
            bkSum.setJkYBSum(bkSum.getJkYBSum().add(d.getJkYBSum()));
            bkSum.setJkBKSum(bkSum.getJkBKSum().add(d.getJkBKSum()));
            bkSum.setCmpQFSum(bkSum.getCmpQFSum().add(d.getCmpQFSum()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(bkSum.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(bkSum.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(bkSum.getJkSum()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(bkSum.getJkFGSSum()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(bkSum.getJkBGSSum()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellMoney);
        tmpRow.getCell(9).setCellValue(StringUtils.getMoneyString(bkSum.getJkSFSum()));
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(bkSum.getJkGLFSum()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(bkSum.getJkLRSum()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(bkSum.getJkKJSum()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellMoney);
        tmpRow.getCell(13).setCellValue(StringUtils.getMoneyString(bkSum.getJkYBSum()));
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellMoney);
        tmpRow.getCell(14).setCellValue(StringUtils.getMoneyString(bkSum.getJkBKSum()));
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellText);
        tmpRow.getCell(15).setCellValue("");
        tmpRow.createCell(16);
        tmpRow.getCell(16).setCellStyle(cellMoney);
        tmpRow.getCell(16).setCellValue(StringUtils.getMoneyString(bkSum.getCmpQFSum()));
        tmpRow.createCell(17);
        tmpRow.getCell(17).setCellStyle(cellText);
        tmpRow.getCell(17).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum = new CellRangeAddress(endRow, endRow, 0, 4);
        sh.addMergedRegion(regSum);
        sh.getRow(endRow).getCell(0).setCellValue("合计");
        //endregion
    }

    @Override
    public AjaxResult exportGSZQ(SzgcRepSearch query) {
        //region create new excel file
        String tmpStr = RuoYiConfig.getTemplatePath() + "/wbzq.xlsx";
        String target = RuoYiConfig.getExportPath() + "/wbzq" + StringUtils.getTimeStampString() +  ".xlsx";

        Path srcPath = Paths.get(tmpStr);
        Path desPath = Paths.get(target);
        //FileUtils.copyFile(tmp, target);
        try {
            Files.copy(srcPath, desPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            return AjaxResult.error("外部债权报表模板错误，请联系管理员!");
        }
        //endregion

        Workbook workbook = getWorkbook(target);
        if(workbook == null){
            return AjaxResult.error("外部债权报表模板错误，请联系管理员!");
        }

        SzgcYzdw yzquery = new SzgcYzdw();
        yzquery.setParentId(0L);
        List<SzgcYzdw> yzdwList = yzdwMapper.selectSzgcYzdwList(yzquery);

        Sheet shzq1 = workbook.getSheetAt(0);
        createZQ01(shzq1, query);

        //yzsheet
        Sheet tmpsh = null;
        Integer shIndex = 0;
        List<SzgcRepZQ2> zqList2, zqList1, zqList0;
        for(SzgcYzdw yz: yzdwList)
        {
            query.setYzId(yz.getYzId());
            query.setConType(2);
            zqList2 = reportMapper.selectZQ_2(query);
            query.setConType(1);
            zqList1 = reportMapper.selectZQ_2(query);
            query.setConType(0);
            zqList0 = reportMapper.selectZQ_2(query);

            if(zqList0.size() + zqList1.size() + zqList2.size() == 0)
            {
                continue;
            }

            tmpsh = workbook.cloneSheet(1);
            shIndex = workbook.getSheetIndex(tmpsh);
            workbook.setSheetName(shIndex, yz.getYzName());

            createZQ02(tmpsh, yz.getYzName(), zqList2, zqList1, zqList0);
        }

        tmpsh = workbook.cloneSheet(2);
        shIndex = workbook.getSheetIndex(tmpsh);
        workbook.setSheetName(shIndex, "结算");

        createZQ03(tmpsh, query);

        //remove template
        workbook.removeSheetAt(2);
        workbook.removeSheetAt(1);

        try {
            OutputStream out = new FileOutputStream(target);
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AjaxResult.success(target);
    }

    private void createZQ01(Sheet sh, SzgcRepSearch query)
    {
        List<SzgcRepZQ1> zqList = reportMapper.selectZQ_1(query);

        SzgcRepZQ1 bkSum  = new SzgcRepZQ1();
        bkSum.reset();

        Row tmpRow = null;
        Integer liIndex = 3, startRow = 3, endRow = 0;
        for(SzgcRepZQ1 d: zqList){

            d.setZqSum(d.getCzSum().subtract(d.getJkSum()));
            d.setYqrSum(d.getJlSum().subtract(d.getJkSum()));
            d.setDqrSum(d.getZqSum().subtract(d.getYqrSum()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(d.getYzName());
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellMoney);
            tmpRow.getCell(1).setCellValue(StringUtils.getMoneyString(d.getZqSum()));
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellMoney);
            tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(d.getYqrSum()));
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellMoney);
            tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(d.getDqrSum()));
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellText);
            tmpRow.getCell(4).setCellValue("");

            bkSum.setZqSum(bkSum.getZqSum().add(d.getZqSum()));
            bkSum.setYqrSum(bkSum.getYqrSum().add(d.getYqrSum()));
            bkSum.setDqrSum(bkSum.getDqrSum().add(d.getDqrSum()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("债权合计");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellMoney);
        tmpRow.getCell(1).setCellValue(StringUtils.getMoneyString(bkSum.getZqSum()));
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellMoney);
        tmpRow.getCell(2).setCellValue(StringUtils.getMoneyString(bkSum.getYqrSum()));
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellMoney);
        tmpRow.getCell(3).setCellValue(StringUtils.getMoneyString(bkSum.getDqrSum()));
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue("");
        //endregion
    }

    private void createZQ02(Sheet sh, String yzName1,
                            List<SzgcRepZQ2> zqList2, List<SzgcRepZQ2> zqList1, List<SzgcRepZQ2> zqList0)
    {

        SzgcRepZQ2 zqSum2  = new SzgcRepZQ2();
        zqSum2.reset();
        SzgcRepZQ2 zqSum1  = new SzgcRepZQ2();
        zqSum1.reset();
        SzgcRepZQ2 zqSum0  = new SzgcRepZQ2();
        zqSum0.reset();
        SzgcRepZQ2 zqSum  = new SzgcRepZQ2();
        zqSum.reset();

        Row tmpRow = null;
        Integer liIndex = 4, endRow = 0;
        for(SzgcRepZQ2 d: zqList2){

            d.setWqrcz(d.getCzSum().subtract(d.getJlSum()));
            d.setZqk(d.getCzSum().subtract(d.getJkSum()));
            d.setKqrqk(d.getJlSum().subtract(d.getJkSum()));
            d.setWqrqk(d.getZqk().subtract(d.getKqrqk()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - 3);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getStartYear() + "-" + d.getEndYear());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getWqrcz()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getCzSum()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellText);
            tmpRow.getCell(9).setCellValue(d.getLastJKDate());
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getZqk()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getKqrqk()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getWqrqk()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellText);
            tmpRow.getCell(13).setCellValue("");
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellText);
            tmpRow.getCell(14).setCellValue(d.getZlDis());
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellText);
            tmpRow.getCell(15).setCellValue(d.getConPerson());

            zqSum2.setConMoney(zqSum2.getConMoney().add(d.getConMoney()));
            zqSum2.setJlSum(zqSum2.getJlSum().add(d.getJlSum()));
            zqSum2.setWqrcz(zqSum2.getWqrcz().add(d.getWqrcz()));
            zqSum2.setCzSum(zqSum2.getCzSum().add(d.getCzSum()));
            zqSum2.setJkSum(zqSum2.getJkSum().add(d.getJkSum()));
            zqSum2.setZqk(zqSum2.getZqk().add(d.getZqk()));
            zqSum2.setKqrqk(zqSum2.getKqrqk().add(d.getKqrqk()));
            zqSum2.setWqrqk(zqSum2.getWqrqk().add(d.getWqrqk()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(zqSum2.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(zqSum2.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(zqSum2.getWqrcz()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(zqSum2.getCzSum()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(zqSum2.getJkSum()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellText);
        tmpRow.getCell(9).setCellValue("");
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(zqSum2.getZqk()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(zqSum2.getKqrqk()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(zqSum2.getWqrqk()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellText);
        tmpRow.getCell(13).setCellValue("");
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellText);
        tmpRow.getCell(14).setCellValue("");
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellText);
        tmpRow.getCell(15).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum2 = new CellRangeAddress(endRow, endRow, 0, 3);
        sh.addMergedRegion(regSum2);
        sh.getRow(endRow).getCell(0).setCellValue("已完工已结算项目");
        //endregion

        liIndex = endRow + 1;
        for(SzgcRepZQ2 d: zqList1){

            d.setWqrcz(d.getCzSum().subtract(d.getJlSum()));
            d.setZqk(d.getCzSum().subtract(d.getJkSum()));
            d.setKqrqk(d.getJlSum().subtract(d.getJkSum()));
            d.setWqrqk(d.getZqk().subtract(d.getKqrqk()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - endRow);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getStartYear() + "-" + d.getEndYear());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getWqrcz()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getCzSum()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellText);
            tmpRow.getCell(9).setCellValue(d.getLastJKDate());
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getZqk()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getKqrqk()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getWqrqk()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellText);
            tmpRow.getCell(13).setCellValue("");
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellText);
            tmpRow.getCell(14).setCellValue(d.getZlDis());
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellText);
            tmpRow.getCell(15).setCellValue(d.getConPerson());

            zqSum1.setConMoney(zqSum1.getConMoney().add(d.getConMoney()));
            zqSum1.setJlSum(zqSum1.getJlSum().add(d.getJlSum()));
            zqSum1.setWqrcz(zqSum1.getWqrcz().add(d.getWqrcz()));
            zqSum1.setCzSum(zqSum1.getCzSum().add(d.getCzSum()));
            zqSum1.setJkSum(zqSum1.getJkSum().add(d.getJkSum()));
            zqSum1.setZqk(zqSum1.getZqk().add(d.getZqk()));
            zqSum1.setKqrqk(zqSum1.getKqrqk().add(d.getKqrqk()));
            zqSum1.setWqrqk(zqSum1.getWqrqk().add(d.getWqrqk()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(zqSum1.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(zqSum1.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(zqSum1.getWqrcz()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(zqSum1.getCzSum()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(zqSum1.getJkSum()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellText);
        tmpRow.getCell(9).setCellValue("");
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(zqSum1.getZqk()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(zqSum1.getKqrqk()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(zqSum1.getWqrqk()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellText);
        tmpRow.getCell(13).setCellValue("");
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellText);
        tmpRow.getCell(14).setCellValue("");
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellText);
        tmpRow.getCell(15).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum1 = new CellRangeAddress(endRow, endRow, 0, 3);
        sh.addMergedRegion(regSum1);
        sh.getRow(endRow).getCell(0).setCellValue("已完工未结算项目");
        //endregion

        liIndex = endRow + 1;
        for(SzgcRepZQ2 d: zqList0){

            d.setWqrcz(d.getCzSum().subtract(d.getJlSum()));
            d.setZqk(d.getCzSum().subtract(d.getJkSum()));
            d.setKqrqk(d.getJlSum().subtract(d.getJkSum()));
            d.setWqrqk(d.getZqk().subtract(d.getKqrqk()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - endRow);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getStartYear() + "-" + d.getEndYear());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getWqrcz()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellMoney);
            tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(d.getCzSum()));
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellMoney);
            tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellText);
            tmpRow.getCell(9).setCellValue(d.getLastJKDate());
            tmpRow.createCell(10);
            tmpRow.getCell(10).setCellStyle(cellMoney);
            tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(d.getZqk()));
            tmpRow.createCell(11);
            tmpRow.getCell(11).setCellStyle(cellMoney);
            tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(d.getKqrqk()));
            tmpRow.createCell(12);
            tmpRow.getCell(12).setCellStyle(cellMoney);
            tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(d.getWqrqk()));
            tmpRow.createCell(13);
            tmpRow.getCell(13).setCellStyle(cellText);
            tmpRow.getCell(13).setCellValue("");
            tmpRow.createCell(14);
            tmpRow.getCell(14).setCellStyle(cellText);
            tmpRow.getCell(14).setCellValue(d.getZlDis());
            tmpRow.createCell(15);
            tmpRow.getCell(15).setCellStyle(cellText);
            tmpRow.getCell(15).setCellValue(d.getConPerson());

            zqSum0.setConMoney(zqSum0.getConMoney().add(d.getConMoney()));
            zqSum0.setJlSum(zqSum0.getJlSum().add(d.getJlSum()));
            zqSum0.setWqrcz(zqSum0.getWqrcz().add(d.getWqrcz()));
            zqSum0.setCzSum(zqSum0.getCzSum().add(d.getCzSum()));
            zqSum0.setJkSum(zqSum0.getJkSum().add(d.getJkSum()));
            zqSum0.setZqk(zqSum0.getZqk().add(d.getZqk()));
            zqSum0.setKqrqk(zqSum0.getKqrqk().add(d.getKqrqk()));
            zqSum0.setWqrqk(zqSum0.getWqrqk().add(d.getWqrqk()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(zqSum0.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(zqSum0.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(zqSum0.getWqrcz()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(zqSum0.getCzSum()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(zqSum0.getJkSum()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellText);
        tmpRow.getCell(9).setCellValue("");
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(zqSum0.getZqk()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(zqSum0.getKqrqk()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(zqSum0.getWqrqk()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellText);
        tmpRow.getCell(13).setCellValue("");
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellText);
        tmpRow.getCell(14).setCellValue("");
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellText);
        tmpRow.getCell(15).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum0 = new CellRangeAddress(endRow, endRow, 0, 3);
        sh.addMergedRegion(regSum0);
        sh.getRow(endRow).getCell(0).setCellValue("在建项目");
        //endregion

        //region 总合计
        zqSum.setConMoney(zqSum0.getConMoney().add(zqSum1.getConMoney()).add(zqSum2.getConMoney()));
        zqSum.setJlSum(zqSum0.getJlSum().add(zqSum1.getJlSum()).add(zqSum2.getJlSum()));
        zqSum.setWqrcz(zqSum0.getJlSum().add(zqSum1.getWqrcz()).add(zqSum2.getWqrcz()));
        zqSum.setCzSum(zqSum0.getJlSum().add(zqSum1.getCzSum()).add(zqSum2.getCzSum()));
        zqSum.setJkSum(zqSum0.getJlSum().add(zqSum1.getJkSum()).add(zqSum2.getJkSum()));
        zqSum.setZqk(zqSum0.getJlSum().add(zqSum1.getZqk()).add(zqSum2.getZqk()));
        zqSum.setKqrqk(zqSum0.getJlSum().add(zqSum1.getKqrqk()).add(zqSum2.getKqrqk()));
        zqSum.setWqrqk(zqSum0.getJlSum().add(zqSum1.getWqrqk()).add(zqSum2.getWqrqk()));

        endRow += 1;
        tmpRow = sh.createRow(endRow);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(zqSum.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(zqSum.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(zqSum.getWqrcz()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellMoney);
        tmpRow.getCell(7).setCellValue(StringUtils.getMoneyString(zqSum.getCzSum()));
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellMoney);
        tmpRow.getCell(8).setCellValue(StringUtils.getMoneyString(zqSum.getJkSum()));
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellText);
        tmpRow.getCell(9).setCellValue("");
        tmpRow.createCell(10);
        tmpRow.getCell(10).setCellStyle(cellMoney);
        tmpRow.getCell(10).setCellValue(StringUtils.getMoneyString(zqSum.getZqk()));
        tmpRow.createCell(11);
        tmpRow.getCell(11).setCellStyle(cellMoney);
        tmpRow.getCell(11).setCellValue(StringUtils.getMoneyString(zqSum.getKqrqk()));
        tmpRow.createCell(12);
        tmpRow.getCell(12).setCellStyle(cellMoney);
        tmpRow.getCell(12).setCellValue(StringUtils.getMoneyString(zqSum.getWqrqk()));
        tmpRow.createCell(13);
        tmpRow.getCell(13).setCellStyle(cellText);
        tmpRow.getCell(13).setCellValue("");
        tmpRow.createCell(14);
        tmpRow.getCell(14).setCellStyle(cellText);
        tmpRow.getCell(14).setCellValue("");
        tmpRow.createCell(15);
        tmpRow.getCell(15).setCellStyle(cellText);
        tmpRow.getCell(15).setCellValue("");

        CellRangeAddress regSum = new CellRangeAddress(endRow, endRow, 0, 3);
        sh.addMergedRegion(regSum);
        sh.getRow(endRow).getCell(0).setCellValue(yzName1 + "合计");
        //endregion
    }

    private void createZQ03(Sheet sh, SzgcRepSearch query)
    {
        List<SzgcRepZQ2> zq3List = reportMapper.selectZQ_3(query);

        SzgcRepZQ2 zqSum = new SzgcRepZQ2();
        zqSum.reset();

        Row tmpRow = null;
        Integer liIndex = 4, endRow = 0;
        for(SzgcRepZQ2 d: zq3List){

            d.setWqrcz(d.getCzSum().subtract(d.getJlSum()));
            d.setZqk(d.getCzSum().subtract(d.getJkSum()));
            d.setKqrqk(d.getJlSum().subtract(d.getJkSum()));
            d.setWqrqk(d.getZqk().subtract(d.getKqrqk()));

            tmpRow = sh.createRow(liIndex);
            tmpRow.createCell(0);
            tmpRow.getCell(0).setCellStyle(cellText);
            tmpRow.getCell(0).setCellValue(liIndex - 3);
            tmpRow.createCell(1);
            tmpRow.getCell(1).setCellStyle(cellText);
            tmpRow.getCell(1).setCellValue(d.getYzName());
            tmpRow.createCell(2);
            tmpRow.getCell(2).setCellStyle(cellText);
            tmpRow.getCell(2).setCellValue(d.getConName());
            tmpRow.createCell(3);
            tmpRow.getCell(3).setCellStyle(cellText);
            tmpRow.getCell(3).setCellValue(d.getStartYear() + "-" + d.getEndYear());
            tmpRow.createCell(4);
            tmpRow.getCell(4).setCellStyle(cellMoney);
            tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(d.getConMoney()));
            tmpRow.createCell(5);
            tmpRow.getCell(5).setCellStyle(cellMoney);
            tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(d.getJlSum()));
            tmpRow.createCell(6);
            tmpRow.getCell(6).setCellStyle(cellMoney);
            tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(d.getJkSum()));
            tmpRow.createCell(7);
            tmpRow.getCell(7).setCellStyle(cellText);
            tmpRow.getCell(7).setCellValue(d.getLastJKDate());
            tmpRow.createCell(8);
            tmpRow.getCell(8).setCellStyle(cellText);
            tmpRow.getCell(8).setCellValue("");
            tmpRow.createCell(9);
            tmpRow.getCell(9).setCellStyle(cellText);
            tmpRow.getCell(9).setCellValue(d.getConPerson());

            zqSum.setConMoney(zqSum.getConMoney().add(d.getConMoney()));
            zqSum.setJlSum(zqSum.getJlSum().add(d.getJlSum()));
            zqSum.setJkSum(zqSum.getJkSum().add(d.getJkSum()));

            liIndex++;
        }

        //region 合计
        tmpRow = sh.createRow(liIndex);
        tmpRow.createCell(0);
        tmpRow.getCell(0).setCellStyle(cellText);
        tmpRow.getCell(0).setCellValue("");
        tmpRow.createCell(1);
        tmpRow.getCell(1).setCellStyle(cellText);
        tmpRow.getCell(1).setCellValue("");
        tmpRow.createCell(2);
        tmpRow.getCell(2).setCellStyle(cellText);
        tmpRow.getCell(2).setCellValue("");
        tmpRow.createCell(3);
        tmpRow.getCell(3).setCellStyle(cellText);
        tmpRow.getCell(3).setCellValue("");
        tmpRow.createCell(4);
        tmpRow.getCell(4).setCellStyle(cellMoney);
        tmpRow.getCell(4).setCellValue(StringUtils.getMoneyString(zqSum.getConMoney()));
        tmpRow.createCell(5);
        tmpRow.getCell(5).setCellStyle(cellMoney);
        tmpRow.getCell(5).setCellValue(StringUtils.getMoneyString(zqSum.getJlSum()));
        tmpRow.createCell(6);
        tmpRow.getCell(6).setCellStyle(cellMoney);
        tmpRow.getCell(6).setCellValue(StringUtils.getMoneyString(zqSum.getJkSum()));
        tmpRow.createCell(7);
        tmpRow.getCell(7).setCellStyle(cellText);
        tmpRow.getCell(7).setCellValue("");
        tmpRow.createCell(8);
        tmpRow.getCell(8).setCellStyle(cellText);
        tmpRow.getCell(8).setCellValue("");
        tmpRow.createCell(9);
        tmpRow.getCell(9).setCellStyle(cellText);
        tmpRow.getCell(9).setCellValue("");

        endRow = liIndex;

        CellRangeAddress regSum = new CellRangeAddress(endRow, endRow, 0, 3);
        sh.addMergedRegion(regSum);
        sh.getRow(endRow).getCell(0).setCellValue("合计");
        //endregion

    }

    private void loadPayDetailData(Long[] jlIds)
    {
        paymentList = new ArrayList<>();

        for (Long jlId : jlIds) {
            SzgcJLPayment jltmp = paymentService.selectSzgcYzPaymentById(jlId);
            paymentList.add(jltmp);
        }
    }

    private Workbook getWorkbook(String filePath){
        Workbook wk = null;
        try {
            InputStream in = new FileInputStream(filePath);
            if(filePath.endsWith(".xls")){
                wk = new HSSFWorkbook(in);
            }
            else if(filePath.endsWith(".xlsx")){
                wk = new XSSFWorkbook(in);
            }

            cellMoney = wk.createCellStyle();
            cellMoney.setAlignment(HorizontalAlignment.RIGHT);
            cellMoney.setVerticalAlignment(VerticalAlignment.CENTER);
            cellMoney.setBorderLeft(BorderStyle.THIN);
            cellMoney.setBorderTop(BorderStyle.THIN);
            cellMoney.setBorderRight(BorderStyle.THIN);
            cellMoney.setBorderBottom(BorderStyle.THIN);
            cellMoney.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellMoney.setTopBorderColor(IndexedColors.BLACK.getIndex());
            cellMoney.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellMoney.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            cellText = wk.createCellStyle();
            cellText.setAlignment(HorizontalAlignment.CENTER);
            cellText.setVerticalAlignment(VerticalAlignment.CENTER);
            cellText.setBorderLeft(BorderStyle.THIN);
            cellText.setBorderTop(BorderStyle.THIN);
            cellText.setBorderRight(BorderStyle.THIN);
            cellText.setBorderBottom(BorderStyle.THIN);
            cellText.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellText.setTopBorderColor(IndexedColors.BLACK.getIndex());
            cellText.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellText.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return wk;
    }


    private void copySheet(Workbook workbook, Sheet fromSheet, Sheet toSheet, Integer rowNumber, Integer colNumber) {
        //set column width
        for(int i = 0; i < colNumber; i++){
            toSheet.setColumnWidth(i, fromSheet.getColumnWidth(i));
        }

        // 复制所有的合并单元格
        copyMergedCells(fromSheet, toSheet);

        // 设置sheet页缩放
        toSheet.setZoom(100);

        for(int i = 0; i< rowNumber; i++)
        {
            Row fromRow = fromSheet.getRow(i);
            Row toRow = toSheet.createRow(i);
            //toSheet.setColumnWidth(i, fromSheet.getColumnWidth(i));
            copyRow(fromRow, toRow, colNumber);
        }

        // 复制所有行
        /*
        for (Iterator it = fromSheet.rowIterator(); it.hasNext();) {
            Row fromRow = (Row) it.next();
            Row toRow = toSheet.createRow(fromRow.getRowNum());
            copyRow(fromRow, toRow);
        }
         */
    }

    private void copyMergedCells(Sheet fromSheet, Sheet toSheet) {
        for (int i = 0; i < fromSheet.getNumMergedRegions(); i++) {
            toSheet.addMergedRegion(fromSheet.getMergedRegion(i));
        }
    }

    private void copyRow(Row fromRow, Row toRow, Integer colNumber) {
        // 复制行高和行样式
        toRow.setHeight(fromRow.getHeight());
        toRow.setRowStyle(fromRow.getRowStyle());

        for(int i = 0; i < colNumber; i++){
            Cell fromCell = fromRow.getCell(i);
            Cell toCell = toRow.createCell(i);

            copyCell(fromCell, toCell);
        }

        // 复制该行所有单元格
        /*
        for (Iterator it = fromRow.cellIterator(); it.hasNext();) {
            Cell fromCell = (Cell) it.next();
            Cell toCell = toRow.createCell(fromCell.getColumnIndex());
            copyCell(fromCell, toCell);
        }
         */
    }

    private void copyCell(Cell fromCell, Cell toCell) {
        // 复制单元格样式
        //toCell.setCellStyle((CellStyle) fromCell.getCellStyle().c);
        toCell.setCellStyle(fromCell.getCellStyle());
        // 复制列宽的方法在XSSFSheet里，所以本方法不做列宽的复制


        // 按照单元格类型复制单元格内容
        toCell.setCellType(fromCell.getCellTypeEnum());
        switch (fromCell.getCellTypeEnum()) {
            case NUMERIC :
                if (DateUtil.isCellDateFormatted(fromCell)) {
                    toCell.setCellValue(fromCell.getDateCellValue());
                } else {
                    toCell.setCellValue(fromCell.getNumericCellValue());
                }
                break;
            case STRING :
                toCell.setCellValue(fromCell.getStringCellValue());
                break;
            case BOOLEAN :
                toCell.setCellValue(fromCell.getBooleanCellValue());
                break;
            case ERROR :
                toCell.setCellValue(fromCell.getErrorCellValue());
                break;
            case FORMULA :
                toCell.setCellFormula(fromCell.getCellFormula());
            default:
                break;
        }
    }

    @Override
    public List<SzgcSearchResult> selectSearch(SzgcRepSearch query)
    {
        query.setStartDate(query.getStartDate() + " 00:00:00");
        query.setEndDate(query.getEndDate() + " 23:59:59");

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dtEnd = df.parse(query.getEndDate());
            Calendar cal = Calendar.getInstance();
            cal.setTime(dtEnd);

            query.setYear(cal.get(Calendar.YEAR));
            query.setMonth(cal.get(Calendar.MONTH));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(query.getYzId() == null)
        {
            query.setYZParent(null);
        }
        else
        {
            SzgcYzdw yzdw = yzdwMapper.selectSzgcYzdwById(query.getYzId());
            if(yzdw == null){
                query.setYZParent(null);
            }
            else{
                if(yzdw.getParentId() == 0){
                    query.setYZParent(1);
                }
                else{
                    query.setYZParent(0);
                }
            }
        }

        List<SzgcSearchResult> res = reportMapper.selectSearch(query);

        for(SzgcSearchResult d : res) {
            d.setConPeriod(d.getStartYear() + " - " + d.getEndYear());
            d.setJkKJ(d.getJkSF().add(d.getJkGLF()).add(d.getJkLR()));
            d.setGsqf(d.getJkYB().subtract(d.getBkSF()));
            d.setZqk(d.getCzSum().subtract(d.getJkSum()));
            d.setKqrqk(d.getJlPF().subtract(d.getJkSum()));
            d.setWqrqk(d.getZqk().subtract(d.getKqrqk()));

            //cz
            SzgcSearchDetail detail = new SzgcSearchDetail();
            detail.setConId(d.getConId());
            detail.setStartDate(query.getStartDate());
            detail.setEndDate(query.getEndDate());
            if(d.getCzSum().compareTo(new BigDecimal(0)) == 1 )
            {
                d.setCzDeatil(reportMapper.selectResultCZDetail(detail));
            }
            //jl
            if(d.getJlPF().compareTo(new BigDecimal(0)) == 1 )
            {
                List<SzgcSearchDetail> js = reportMapper.selectResultJSDetail(detail);
                if(js.size() > 0)
                {
                    d.setJlDeatil(js);
                }
                else
                {
                    d.setJlDeatil(reportMapper.selectResultJLDetail(detail));
                }
            }
            //jk
            if(d.getJkSum().compareTo(new BigDecimal(0)) == 1 )
            {
                d.setJkDeatil(reportMapper.selectResultJKDetail(detail));
            }
            //bk
            if(d.getBkSF().compareTo(new BigDecimal(0)) == 1 )
            {
                d.setBkDeatil(reportMapper.selectResultBKDetail(detail));
            }
        }

        return res;
    }

    @Override
    public SzgcMainSumMoney selectMainSum() {

        List<SzgcMainSumMoney> mainSum = reportMapper.selectMainSum();
        SzgcMainSumMoney res = null;
        if(mainSum.size() == 0){
            res = new SzgcMainSumMoney();
            res.reset();
        }
        else{
            res = mainSum.get(0);
            res.setZqSum(res.getCzSum().subtract(res.getJkSum()));
            res.setZqyqrSum(res.getJlSum().subtract(res.getJkSum()));
            res.setZqwqrSum(res.getZqSum().subtract(res.getZqyqrSum()));
        }

        return res;
    }
}
