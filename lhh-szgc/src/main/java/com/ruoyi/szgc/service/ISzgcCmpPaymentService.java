package com.ruoyi.szgc.service;

import com.ruoyi.szgc.domain.*;

import java.text.ParseException;
import java.util.List;

/**
 * 业主计量Service接口
 * 
 * @author lhh
 * @date 2020-09-24
 */
public interface ISzgcCmpPaymentService
{
    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    public SzgcJLPayment selectSzgcCmpPaymentById(Long jlId);

    public List<SzgcCmpPayment> selectSzgcCmpPaymentByConId(Long conId);

    /**
     * 查询业主计量列表
     * 
     * @param CmpPayment 业主计量
     * @return 业主计量集合
     */
    public List<SzgcCmpPayment> selectSzgcCmpPaymentList(SzgcCmpPayment CmpPayment);

    /**
     * 新增业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    public int insertSzgcCmpPayment(SzgcJLPayment jlPayment) throws ParseException;

    /**
     * 修改业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    public int updateSzgcCmpPayment(SzgcJLPayment jlPayment) throws ParseException;

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的业主计量ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentByIds(Long[] jlIds);

    /**
     * 删除业主计量信息
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    public int deleteSzgcCmpPaymentById(Long jlId);

    SzgcJLSumMoney getJLSumByConId(Long conId);

    List<SzgcCmpPaymentCmp> getCmpPaymentList(Long jlId);

    int saveCmpJLCmpPayment(List<SzgcCmpPaymentCmp> Cmpcmps);
}
