package com.ruoyi.szgc.service;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzBk;

/**
 * 总公司拨款Service接口
 * 
 * @author lhh
 * @date 2020-11-29
 */
public interface ISzgcYzBkService 
{
    /**
     * 查询总公司拨款
     * 
     * @param bkId 总公司拨款ID
     * @return 总公司拨款
     */
    public SzgcYzBk selectSzgcYzBkById(Long bkId);

    /**
     * 查询总公司拨款列表
     * 
     * @param szgcYzBk 总公司拨款
     * @return 总公司拨款集合
     */
    public List<SzgcYzBk> selectSzgcYzBkList(SzgcYzBk szgcYzBk);

    /**
     * 新增总公司拨款
     * 
     * @param szgcYzBk 总公司拨款
     * @return 结果
     */
    public int insertSzgcYzBk(SzgcYzBk szgcYzBk);

    /**
     * 修改总公司拨款
     * 
     * @param szgcYzBk 总公司拨款
     * @return 结果
     */
    public int updateSzgcYzBk(SzgcYzBk szgcYzBk);

    /**
     * 批量删除总公司拨款
     * 
     * @param bkIds 需要删除的总公司拨款ID
     * @return 结果
     */
    public int deleteSzgcYzBkByIds(Long[] bkIds);

    /**
     * 删除总公司拨款信息
     * 
     * @param bkId 总公司拨款ID
     * @return 结果
     */
    public int deleteSzgcYzBkById(Long bkId);
}
