package com.ruoyi.szgc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcYzCzMapper;
import com.ruoyi.szgc.domain.SzgcYzCz;
import com.ruoyi.szgc.service.ISzgcYzCzService;

/**
 * 业主产值Service业务层处理
 * 
 * @author lhh
 * @date 2020-12-17
 */
@Service
public class SzgcYzCzServiceImpl implements ISzgcYzCzService 
{
    @Autowired
    private SzgcYzCzMapper szgcYzCzMapper;

    /**
     * 查询业主产值
     * 
     * @param czId 业主产值ID
     * @return 业主产值
     */
    @Override
    public SzgcYzCz selectSzgcYzCzById(Long czId)
    {
        return szgcYzCzMapper.selectSzgcYzCzById(czId);
    }

    /**
     * 查询业主产值列表
     * 
     * @param szgcYzCz 业主产值
     * @return 业主产值
     */
    @Override
    public List<SzgcYzCz> selectSzgcYzCzList(SzgcYzCz szgcYzCz)
    {
        return szgcYzCzMapper.selectSzgcYzCzList(szgcYzCz);
    }

    /**
     * 新增业主产值
     * 
     * @param szgcYzCz 业主产值
     * @return 结果
     */
    @Override
    public int insertSzgcYzCz(SzgcYzCz szgcYzCz)
    {
        szgcYzCz.setCreateTime(DateUtils.getNowDate());
        szgcYzCz.setDeleteMark(0);
        return szgcYzCzMapper.insertSzgcYzCz(szgcYzCz);
    }

    /**
     * 修改业主产值
     * 
     * @param szgcYzCz 业主产值
     * @return 结果
     */
    @Override
    public int updateSzgcYzCz(SzgcYzCz szgcYzCz)
    {
        szgcYzCz.setUpdateTime(DateUtils.getNowDate());
        return szgcYzCzMapper.updateSzgcYzCz(szgcYzCz);
    }

    /**
     * 批量删除业主产值
     * 
     * @param czIds 需要删除的业主产值ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzCzByIds(Long[] czIds)
    {
        return szgcYzCzMapper.deleteSzgcYzCzByIds(czIds);
    }

    /**
     * 删除业主产值信息
     * 
     * @param czId 业主产值ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzCzById(Long czId)
    {
        return szgcYzCzMapper.deleteSzgcYzCzById(czId);
    }
}
