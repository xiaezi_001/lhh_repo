package com.ruoyi.szgc.service.impl;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.szgc.domain.SzgcYzPaymentCmp;
import com.ruoyi.szgc.mapper.SzgcYzPaymentCmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcYzJkMapper;
import com.ruoyi.szgc.domain.SzgcYzJk;
import com.ruoyi.szgc.service.ISzgcYzJkService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.unit.DataUnit;

/**
 * 业主进款Service业务层处理
 * 
 * @author lhh
 * @date 2020-11-29
 */
@Service
public class SzgcYzJkServiceImpl implements ISzgcYzJkService 
{
    @Autowired
    private SzgcYzJkMapper szgcYzJkMapper;

    @Autowired
    private SzgcYzPaymentCmpMapper cmpMapper;

    /**
     * 查询业主进款
     * 
     * @param jkId 业主进款ID
     * @return 业主进款
     */
    @Override
    public SzgcYzJk selectSzgcYzJkById(Long jkId)
    {
        SzgcYzJk res = szgcYzJkMapper.selectSzgcYzJkById(jkId);
        res.setJkCmpFGS(cmpMapper.getYzPaymentCmpListByJlId(res.getJkId(), 1));
        return res;
    }

    /**
     * 查询业主进款列表
     * 
     * @param szgcYzJk 业主进款
     * @return 业主进款
     */
    @Override
    public List<SzgcYzJk> selectSzgcYzJkList(SzgcYzJk szgcYzJk)
    {
        List<SzgcYzJk> res = szgcYzJkMapper.selectSzgcYzJkList(szgcYzJk);

        for (SzgcYzJk jk: res) {
            jk.setJkCmpFGS(cmpMapper.getYzPaymentCmpListByJlId(jk.getJkId(), 1));
        }
        return res;
    }

    /**
     * 新增业主进款
     * 
     * @param szgcYzJk 业主进款
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int insertSzgcYzJk(SzgcYzJk szgcYzJk)
    {
        int res = 0;
        szgcYzJk.setCreateTime(DateUtils.getNowDate());
        szgcYzJk.setUpdateTime(DateUtils.getNowDate());
        szgcYzJk.setDeleteMark(0);

        res = szgcYzJkMapper.insertSzgcYzJk(szgcYzJk);

        for (SzgcYzPaymentCmp cmp: szgcYzJk.getJkCmpFGS()) {
            if(cmp.getCmpMoney().compareTo(new BigDecimal(0)) > 0){
                cmp.setJlId(szgcYzJk.getJkId());
                cmpMapper.insertSzgcYzPaymentCmp(cmp);
            }
        }

        return res;
    }

    /**
     * 修改业主进款
     * 
     * @param szgcYzJk 业主进款
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int updateSzgcYzJk(SzgcYzJk szgcYzJk)
    {
        int res = 0;
        szgcYzJk.setUpdateTime(DateUtils.getNowDate());

        res = szgcYzJkMapper.updateSzgcYzJk(szgcYzJk);

        for (SzgcYzPaymentCmp cmp: szgcYzJk.getJkCmpFGS()) {
            if(cmpMapper.checkHasPaymentCmp(szgcYzJk.getJkId(), cmp.getCmpId(), 1) == 0){
                if(cmp.getCmpMoney().compareTo(new BigDecimal(0)) > 0){
                    cmp.setPayType(1);
                    cmpMapper.insertSzgcYzPaymentCmp(cmp);
                }
            }
            else{
                cmpMapper.updateSzgcYzPaymentCmp(cmp);
            }
        }

        return res;
    }

    /**
     * 批量删除业主进款
     * 
     * @param jkIds 需要删除的业主进款ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcYzJkByIds(Long[] jkIds)
    {
        int res = szgcYzJkMapper.deleteSzgcYzJkByIds(jkIds);

        cmpMapper.deleteSzgcYzPaymentCmpByIds(jkIds);

        return res;
    }

    /**
     * 删除业主进款信息
     * 
     * @param jkId 业主进款ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcYzJkById(Long jkId)
    {
        int res = szgcYzJkMapper.deleteSzgcYzJkById(jkId);

        cmpMapper.deleteSzgcYzPaymentCmpById(jkId);

        return res;
    }
}
