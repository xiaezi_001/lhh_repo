package com.ruoyi.szgc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcYzJlMapper;
import com.ruoyi.szgc.domain.SzgcYzJl;
import com.ruoyi.szgc.service.ISzgcYzJlService;

/**
 * 业主计量Service业务层处理
 * 
 * @author lhh
 * @date 2020-11-29
 */
@Service
public class SzgcYzJlServiceImpl implements ISzgcYzJlService 
{
    @Autowired
    private SzgcYzJlMapper szgcYzJlMapper;

    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    @Override
    public SzgcYzJl selectSzgcYzJlById(Long jlId)
    {
        return szgcYzJlMapper.selectSzgcYzJlById(jlId);
    }

    /**
     * 查询业主计量列表
     * 
     * @param szgcYzJl 业主计量
     * @return 业主计量
     */
    @Override
    public List<SzgcYzJl> selectSzgcYzJlList(SzgcYzJl szgcYzJl)
    {
        return szgcYzJlMapper.selectSzgcYzJlList(szgcYzJl);
    }

    /**
     * 新增业主计量
     * 
     * @param szgcYzJl 业主计量
     * @return 结果
     */
    @Override
    public int insertSzgcYzJl(SzgcYzJl szgcYzJl)
    {
        szgcYzJl.setCreateTime(DateUtils.getNowDate());
        return szgcYzJlMapper.insertSzgcYzJl(szgcYzJl);
    }

    /**
     * 修改业主计量
     * 
     * @param szgcYzJl 业主计量
     * @return 结果
     */
    @Override
    public int updateSzgcYzJl(SzgcYzJl szgcYzJl)
    {
        szgcYzJl.setUpdateTime(DateUtils.getNowDate());
        return szgcYzJlMapper.updateSzgcYzJl(szgcYzJl);
    }

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的业主计量ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzJlByIds(Long[] jlIds)
    {
        return szgcYzJlMapper.deleteSzgcYzJlByIds(jlIds);
    }

    /**
     * 删除业主计量信息
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzJlById(Long jlId)
    {
        return szgcYzJlMapper.deleteSzgcYzJlById(jlId);
    }
}
