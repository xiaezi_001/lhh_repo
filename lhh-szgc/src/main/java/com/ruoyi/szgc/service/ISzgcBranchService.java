package com.ruoyi.szgc.service;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcBranch;

/**
 * 分公司Service接口
 * 
 * @author lhh
 * @date 2020-09-20
 */
public interface ISzgcBranchService 
{
    /**
     * 查询分公司
     * 
     * @param branchId 分公司ID
     * @return 分公司
     */
    public SzgcBranch selectSzgcBranchById(Long branchId);

    /**
     * 查询分公司列表
     * 
     * @param szgcBranch 分公司
     * @return 分公司集合
     */
    public List<SzgcBranch> selectSzgcBranchList();
    public List<SzgcBranch> selectSzgcBranchList(SzgcBranch szgcBranch);

    /**
     * 新增分公司
     * 
     * @param szgcBranch 分公司
     * @return 结果
     */
    public int insertSzgcBranch(SzgcBranch szgcBranch);

    /**
     * 修改分公司
     * 
     * @param szgcBranch 分公司
     * @return 结果
     */
    public int updateSzgcBranch(SzgcBranch szgcBranch);

    /**
     * 批量删除分公司
     * 
     * @param branchIds 需要删除的分公司ID
     * @return 结果
     */
    public int deleteSzgcBranchByIds(Long[] branchIds);

    /**
     * 删除分公司信息
     * 
     * @param branchId 分公司ID
     * @return 结果
     */
    public int deleteSzgcBranchById(Long branchId);
}
