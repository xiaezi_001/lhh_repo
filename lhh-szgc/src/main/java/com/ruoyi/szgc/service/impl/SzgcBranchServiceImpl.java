package com.ruoyi.szgc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcBranchMapper;
import com.ruoyi.szgc.domain.SzgcBranch;
import com.ruoyi.szgc.service.ISzgcBranchService;

/**
 * 分公司Service业务层处理
 * 
 * @author lhh
 * @date 2020-09-20
 */
@Service
public class SzgcBranchServiceImpl implements ISzgcBranchService 
{
    @Autowired
    private SzgcBranchMapper szgcBranchMapper;

    /**
     * 查询分公司
     * 
     * @param branchId 分公司ID
     * @return 分公司
     */
    @Override
    public SzgcBranch selectSzgcBranchById(Long branchId)
    {
        return szgcBranchMapper.selectSzgcBranchById(branchId);
    }

    @Override
    public List<SzgcBranch> selectSzgcBranchList() {
        return selectSzgcBranchList(new SzgcBranch());
    }
     /**
     * 查询分公司列表
     * 
     * @param szgcBranch 分公司
     * @return 分公司
     */
    @Override
    public List<SzgcBranch> selectSzgcBranchList(SzgcBranch szgcBranch)
    {
        return szgcBranchMapper.selectSzgcBranchList(szgcBranch);
    }

    /**
     * 新增分公司
     * 
     * @param szgcBranch 分公司
     * @return 结果
     */
    @Override
    public int insertSzgcBranch(SzgcBranch szgcBranch)
    {
        return szgcBranchMapper.insertSzgcBranch(szgcBranch);
    }

    /**
     * 修改分公司
     * 
     * @param szgcBranch 分公司
     * @return 结果
     */
    @Override
    public int updateSzgcBranch(SzgcBranch szgcBranch)
    {
        return szgcBranchMapper.updateSzgcBranch(szgcBranch);
    }

    /**
     * 批量删除分公司
     * 
     * @param branchIds 需要删除的分公司ID
     * @return 结果
     */
    @Override
    public int deleteSzgcBranchByIds(Long[] branchIds)
    {
        return szgcBranchMapper.deleteSzgcBranchByIds(branchIds);
    }

    /**
     * 删除分公司信息
     * 
     * @param branchId 分公司ID
     * @return 结果
     */
    @Override
    public int deleteSzgcBranchById(Long branchId)
    {
        return szgcBranchMapper.deleteSzgcBranchById(branchId);
    }
}
