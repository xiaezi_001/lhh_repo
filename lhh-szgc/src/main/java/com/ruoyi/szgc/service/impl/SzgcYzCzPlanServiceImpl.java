package com.ruoyi.szgc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcYzCzPlanMapper;
import com.ruoyi.szgc.domain.SzgcYzCzPlan;
import com.ruoyi.szgc.service.ISzgcYzCzPlanService;

/**
 * 产值计划Service业务层处理
 * 
 * @author lhh
 * @date 2020-12-17
 */
@Service
public class SzgcYzCzPlanServiceImpl implements ISzgcYzCzPlanService 
{
    @Autowired
    private SzgcYzCzPlanMapper szgcYzCzPlanMapper;

    /**
     * 查询产值计划
     * 
     * @param czPlanId 产值计划ID
     * @return 产值计划
     */
    @Override
    public SzgcYzCzPlan selectSzgcYzCzPlanById(Long czPlanId)
    {
        return szgcYzCzPlanMapper.selectSzgcYzCzPlanById(czPlanId);
    }

    /**
     * 查询产值计划列表
     * 
     * @param szgcYzCzPlan 产值计划
     * @return 产值计划
     */
    @Override
    public List<SzgcYzCzPlan> selectSzgcYzCzPlanList(SzgcYzCzPlan szgcYzCzPlan)
    {
        return szgcYzCzPlanMapper.selectSzgcYzCzPlanList(szgcYzCzPlan);
    }

    /**
     * 新增产值计划
     * 
     * @param szgcYzCzPlan 产值计划
     * @return 结果
     */
    @Override
    public int insertSzgcYzCzPlan(SzgcYzCzPlan szgcYzCzPlan) throws Exception {
        List<SzgcYzCzPlan> czPlans = szgcYzCzPlanMapper.selectSzgcYzCzPlanList(szgcYzCzPlan);
        if(czPlans.size() > 0)
        {
            throw new Exception("已存在该年份的产值!");
        }

        szgcYzCzPlan.setCreateTime(DateUtils.getNowDate());
        szgcYzCzPlan.setDeleteMark(0);
        return szgcYzCzPlanMapper.insertSzgcYzCzPlan(szgcYzCzPlan);
    }

    /**
     * 修改产值计划
     * 
     * @param szgcYzCzPlan 产值计划
     * @return 结果
     */
    @Override
    public int updateSzgcYzCzPlan(SzgcYzCzPlan szgcYzCzPlan)
    {
        szgcYzCzPlan.setUpdateTime(DateUtils.getNowDate());
        return szgcYzCzPlanMapper.updateSzgcYzCzPlan(szgcYzCzPlan);
    }

    /**
     * 批量删除产值计划
     * 
     * @param czPlanIds 需要删除的产值计划ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzCzPlanByIds(Long[] czPlanIds)
    {
        return szgcYzCzPlanMapper.deleteSzgcYzCzPlanByIds(czPlanIds);
    }

    /**
     * 删除产值计划信息
     * 
     * @param czPlanId 产值计划ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzCzPlanById(Long czPlanId)
    {
        return szgcYzCzPlanMapper.deleteSzgcYzCzPlanById(czPlanId);
    }
}
