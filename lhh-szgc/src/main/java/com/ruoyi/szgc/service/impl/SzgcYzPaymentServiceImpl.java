package com.ruoyi.szgc.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.service.ISzgcYzPaymentService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业主计量Service业务层处理
 * 
 * @author lhh
 * @date 2020-09-24
 */
@Service
public class SzgcYzPaymentServiceImpl implements ISzgcYzPaymentService 
{
    @Autowired
    private SzgcYzPaymentMapper szgcYzPaymentMapper;

    @Autowired
    private SzgcYzPaymentItemsMapper szgcYzPaymentItemsMapper;

    @Autowired
    private SzgcYzPaymentCmpMapper szgcYzPaymentCmpMapper;

    @Autowired
    private SzgcYzUploadMapper szgcYzUploadMapper;

    @Autowired
    private SzgcYzJlMapper jlMapper;

    @Autowired
    private SzgcYzJkMapper jkMapper;

    @Autowired
    private SzgcYzCzPlanMapper czPlanMapper;

    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    @Override
    public SzgcJLPayment selectSzgcYzPaymentById(Long jlId)
    {
        SzgcJLPayment res = new SzgcJLPayment();
        SzgcYzPaymentItems query = new SzgcYzPaymentItems();
        query.setJlId(jlId);

        SzgcYzPayment yzPayment = szgcYzPaymentMapper.selectSzgcYzPaymentById(jlId);
        List<SzgcYzPaymentItems> jlItemList = szgcYzPaymentItemsMapper.selectSzgcYzPaymentItemsList(query);

        res.setJlId(yzPayment.getJlId());
        res.setConId(yzPayment.getConId());
        res.setJlType(yzPayment.getJlType());
        res.setJlDate(yzPayment.getJlDate());
        res.setJlCustomNum(yzPayment.getJlCustomNum());
        res.setCzMoney(yzPayment.getCzMoney());
        res.setJlMoney(yzPayment.getJlMoney());
        res.setJlBGSMoney(yzPayment.getJlBGSMoney());
        res.setJlPay(yzPayment.getJlPayment());
        res.setJlBGSPay(yzPayment.getJlBGSPayment());
        res.setNeedPay(yzPayment.getNeedPayment());
        res.setRealPay(yzPayment.getRealPayment());
        res.setInputDate(yzPayment.getInputDate());
        res.setMemo(yzPayment.getMemo());

        if(jlItemList.size() >= 6){
            res.setYfMoney(jlItemList.get(0).getDetailMoney());
            res.setGcMoney(jlItemList.get(1).getDetailMoney());
            res.setNmgMoney(jlItemList.get(2).getDetailMoney());
            res.setSlRate(jlItemList.get(3).getDetailValue());
            res.setSlMoney(jlItemList.get(3).getDetailMoney());
            res.setGlfRate(jlItemList.get(4).getDetailValue());
            res.setGlfMoney(jlItemList.get(4).getDetailMoney());
            res.setLrRate(jlItemList.get(5).getDetailValue());
            res.setLrMoney(jlItemList.get(5).getDetailMoney());
        }

        res.setCmpFGSMoney(szgcYzPaymentCmpMapper.getYzPaymentCmpListByJlId(jlId, 0));
        res.setCmpFGSPay(szgcYzPaymentCmpMapper.getYzPaymentCmpListByJlId(jlId, 1));

        return res;
    }

    @Override
    public List<SzgcYzPayment> selectSzgcYzPaymentByConId(Long conId) {
        return szgcYzPaymentMapper.selectSzgcYzPaymentByConId(conId);
    }
    /**
     * 查询业主计量列表
     * 
     * @param yzPayment 业主计量
     * @return 业主计量
     */
    @Override
    public List<SzgcYzPayment> selectSzgcYzPaymentList(SzgcYzPayment yzPayment)
    {
        return szgcYzPaymentMapper.selectSzgcYzPaymentList(yzPayment);
    }

    /**
     * 新增业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int insertSzgcYzPayment(SzgcJLPayment jlPayment) throws ParseException {

        SzgcYzPayment yzPayment = this.prepareYzPayment(jlPayment);

        //check year and month
        SzgcYzPayment jlcheck = new SzgcYzPayment();
        jlcheck.setConId(yzPayment.getConId());
        jlcheck.setJlYear(yzPayment.getJlYear());
        jlcheck.setJlMonth(yzPayment.getJlMonth());

        List<SzgcYzPayment> checkRes = null;
        if(jlPayment.getJlType() == 0) {
            checkRes = szgcYzPaymentMapper.selectSzgcYzPaymentList(jlcheck);
            if (checkRes.size() > 0) {
                jlPayment.setMemo("已存在改月份的计量, 请检查结算日期");
                return -1;
            }
        }

        if(jlPayment.getJlType() == 1){
            jlcheck.setJlYear(null);
            jlcheck.setJlMonth(null);
            jlcheck.setJlType(1);

            checkRes = szgcYzPaymentMapper.selectSzgcYzPaymentList(jlcheck);
            if(checkRes.size() > 0){
                jlPayment.setMemo("该项目已存在决算");
                return -2;
            }
        }

        int res = szgcYzPaymentMapper.insertSzgcYzPayment(yzPayment);

        //payment item
        SzgcYzPaymentItems ptyf = this.prepareYzPaymentItem(jlPayment, 1, "预付款");
        SzgcYzPaymentItems ptgc = this.prepareYzPaymentItem(jlPayment, 2, "工程款");
        SzgcYzPaymentItems ptnmg = this.prepareYzPaymentItem(jlPayment, 3, "农民工");
        SzgcYzPaymentItems ptsl = this.prepareYzPaymentItem(jlPayment, 4, "税率");
        SzgcYzPaymentItems ptglf = this.prepareYzPaymentItem(jlPayment, 5, "管理费");
        SzgcYzPaymentItems ptlr = this.prepareYzPaymentItem(jlPayment, 6, "利润");

        ptyf.setJlId(yzPayment.getJlId());
        ptgc.setJlId(yzPayment.getJlId());
        ptnmg.setJlId(yzPayment.getJlId());
        ptsl.setJlId(yzPayment.getJlId());
        ptglf.setJlId(yzPayment.getJlId());
        ptlr.setJlId(yzPayment.getJlId());

        szgcYzPaymentItemsMapper.insertSzgcYzPaymentItems(ptyf);
        szgcYzPaymentItemsMapper.insertSzgcYzPaymentItems(ptgc);
        szgcYzPaymentItemsMapper.insertSzgcYzPaymentItems(ptnmg);

        szgcYzPaymentItemsMapper.insertSzgcYzPaymentItems(ptsl);
        szgcYzPaymentItemsMapper.insertSzgcYzPaymentItems(ptglf);
        szgcYzPaymentItemsMapper.insertSzgcYzPaymentItems(ptlr);
        
        //payment cmp
        for (SzgcYzPaymentCmp cmp: jlPayment.getCmpFGSMoney()) {
            if(cmp.getCmpMoney().compareTo(new BigDecimal(0)) > 0){
                cmp.setJlId(yzPayment.getJlId());
                szgcYzPaymentCmpMapper.insertSzgcYzPaymentCmp(cmp);
            }
        }

        for(SzgcYzPaymentCmp cmp: jlPayment.getCmpFGSPay()){
            if(cmp.getCmpMoney().compareTo(new BigDecimal(0)) > 0){
                cmp.setJlId(yzPayment.getJlId());
                szgcYzPaymentCmpMapper.insertSzgcYzPaymentCmp(cmp);
            }
        }


        return res;
    }

    /**
     * 修改业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int updateSzgcYzPayment(SzgcJLPayment jlPayment) throws ParseException {
        SzgcYzPayment yzPayment = this.prepareYzPayment(jlPayment);
        int res = szgcYzPaymentMapper.updateSzgcYzPayment(yzPayment);

        //payment item
        SzgcYzPaymentItems ptyf = this.prepareYzPaymentItem(jlPayment, 1, "预付款");
        SzgcYzPaymentItems ptgc = this.prepareYzPaymentItem(jlPayment, 2, "工程款");
        SzgcYzPaymentItems ptnmg = this.prepareYzPaymentItem(jlPayment, 3, "农民工");
        SzgcYzPaymentItems ptsl = this.prepareYzPaymentItem(jlPayment, 4, "税率");
        SzgcYzPaymentItems ptglf = this.prepareYzPaymentItem(jlPayment, 5, "管理费");
        SzgcYzPaymentItems ptlr = this.prepareYzPaymentItem(jlPayment, 6, "利润");

        szgcYzPaymentItemsMapper.updateSzgcYzPaymentItems(ptyf);
        szgcYzPaymentItemsMapper.updateSzgcYzPaymentItems(ptgc);
        szgcYzPaymentItemsMapper.updateSzgcYzPaymentItems(ptnmg);

        szgcYzPaymentItemsMapper.updateSzgcYzPaymentItems(ptsl);
        szgcYzPaymentItemsMapper.updateSzgcYzPaymentItems(ptglf);
        szgcYzPaymentItemsMapper.updateSzgcYzPaymentItems(ptlr);

        //update cmp
        for(SzgcYzPaymentCmp c: jlPayment.getCmpFGSMoney())
        {
            if(szgcYzPaymentCmpMapper.checkHasPaymentCmp(jlPayment.getJlId(), c.getCmpId(), 0) == 0){
                if(c.getCmpMoney().compareTo(new BigDecimal(0)) > 0) {
                    c.setPayType(0);
                    szgcYzPaymentCmpMapper.insertSzgcYzPaymentCmp(c);
                }
            }
            else{
                szgcYzPaymentCmpMapper.updateSzgcYzPaymentCmp(c);
            }
        }

        for(SzgcYzPaymentCmp c: jlPayment.getCmpFGSPay())
        {
            if(szgcYzPaymentCmpMapper.checkHasPaymentCmp(jlPayment.getJlId(), c.getCmpId(), 1) == 0){
                if(c.getCmpMoney().compareTo(new BigDecimal(0)) > 0) {
                    c.setPayType(1);
                    szgcYzPaymentCmpMapper.insertSzgcYzPaymentCmp(c);
                }
            }
            else{
                szgcYzPaymentCmpMapper.updateSzgcYzPaymentCmp(c);
            }
        }

        return res;
    }

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的业主计量ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcYzPaymentByIds(Long[] jlIds) {
        szgcYzPaymentItemsMapper.deleteSzgcYzPaymentItemsByIds(jlIds);
        szgcYzPaymentCmpMapper.deleteSzgcYzPaymentCmpByIds(jlIds);
        return szgcYzPaymentMapper.deleteSzgcYzPaymentByIds(jlIds);
    }

    /**
     * 删除业主计量信息
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcYzPaymentById(Long jlId)
    {
        szgcYzPaymentItemsMapper.deleteSzgcYzPaymentItemsById(jlId);
        szgcYzPaymentCmpMapper.deleteSzgcYzPaymentCmpById(jlId);
        return szgcYzPaymentMapper.deleteSzgcYzPaymentById(jlId);
    }

    @Override
    public SzgcJLSumMoney getJLSumByConId(Long conId) {

        SzgcJLSumMoney sumAll = new SzgcJLSumMoney();
        SzgcJLSumMoney czSum = null;
        SzgcJLSumMoney jlSum = null;
        SzgcJLSumMoney jkSum = null;
        SzgcJLSumMoney bkSum = null;

        //cz plan
        SzgcYzCzPlan czPlan = new SzgcYzCzPlan();
        czPlan.setConId(conId);
        czPlan.setCzPlanYear(Calendar.getInstance().get(Calendar.YEAR));
        List<SzgcYzCzPlan> czPlanList = czPlanMapper.selectSzgcYzCzPlanList(czPlan);
        if(czPlanList.size() == 0){
            sumAll.setCzYearPlan(new BigDecimal(0));
        }
        else{
            sumAll.setCzYearPlan(czPlanList.get(0).getCzPlanMoney());
        }

        //cz
        czSum = szgcYzPaymentMapper.selectCZSum(conId);
        sumAll.setCzSumMoney(czSum.getCzSumMoney());

        //has JS
        SzgcYzJl js = jlMapper.selectSzgcYzJSByConId(conId);
        if(js != null){
            //sumAll.setCzSumMoney(js.getJlCz());
            sumAll.setPfSumMoney(js.getJlPf());
            sumAll.setAccountAges(this.getJLArreasAge(js.getJlDate()));
        }
        else{
            jlSum = szgcYzPaymentMapper.selectJLSum(conId);
            //sumAll.setCzSumMoney(jlSum.getCzSumMoney());
            sumAll.setPfSumMoney(jlSum.getPfSumMoney());
            sumAll.setAccountAges(jlSum.getAccountAges());
        }

        jkSum = szgcYzPaymentMapper.selectJKSum(conId);
        sumAll.setYzPaySumMoney(jkSum.getYzPaySumMoney());
        sumAll.setBgsSumMoney(jkSum.getBgsSumMoney());
        sumAll.setYbSumMoney(jkSum.getYbSumMoney());

        bkSum = szgcYzPaymentMapper.selectBKSum(conId);
        sumAll.setSbSumMoney(bkSum.getSbSumMoney());

        //calculate money
        sumAll.setZqSumMoney(sumAll.getPfSumMoney().subtract(sumAll.getYzPaySumMoney()));
        sumAll.setQkSumMoney(sumAll.getYbSumMoney().subtract(sumAll.getSbSumMoney()));

        return sumAll;
    }

    @Override
    public List<SzgcYzPaymentCmp> getCmpPaymentList(Long jlId) {
        return szgcYzPaymentCmpMapper.getYzPaymentCmpListByJlId(jlId, 0);
    }

    @Override
    public int saveYZJLCmpPayment(List<SzgcYzPaymentCmp> yzcmps) {

        for (SzgcYzPaymentCmp cmp: yzcmps) {
            cmp.setDeleteMark(0);

            if(cmp.getJlCmpId() == 0){
                szgcYzPaymentCmpMapper.insertSzgcYzPaymentCmp(cmp);
            }
            else{
                szgcYzPaymentCmpMapper.updateSzgcYzPaymentCmp(cmp);
            }
        }
        return yzcmps.size();
    }

    @Override
    public BigDecimal getLastYZJlSLRate(Long conId){
        SzgcYzPayment jlsl = new SzgcYzPayment();
        jlsl.setConId(conId);
        List<SzgcYzPayment> jlList = szgcYzPaymentMapper.selectSzgcYzPaymentList(jlsl);

        if(jlList.size() > 0){
            SzgcYzPaymentItems slitem = new SzgcYzPaymentItems();
            slitem.setJlId(jlList.get(jlList.size() - 1).getJlId());
            slitem.setDetailId(4);
            List<SzgcYzPaymentItems> slitemList = szgcYzPaymentItemsMapper.selectSzgcYzPaymentItemsList(slitem);

            if(slitemList.size() > 0){
                return slitemList.get(0).getDetailValue();
            }
            else{
                return new BigDecimal(0);
            }
        }
        else{
            return new BigDecimal(0);
        }
    }

    @Override
    public SzgcJLPayment getLastYZJLItem(Long conId) {
        SzgcYzJk jk = jkMapper.selectLastSzgcYzJk();
        SzgcJLPayment res = new SzgcJLPayment();
        //get jl item
        if(jk == null)
        {
            res.setSlRate(new BigDecimal(0));
            res.setGlfRate(new BigDecimal(0));
            res.setLrRate(new BigDecimal(0));
        }
        else
        {
            res.setSlRate(jk.getJkSl());
            res.setGlfRate(jk.getJkGlfl());
            res.setLrRate(jk.getJkLrl());
        }

        return res;
    }

    @Override
    public int saveUploadFile(SzgcYzUpload upload)
    {
        return szgcYzUploadMapper.insertSzgcYzUpload(upload);
    }

    @Override
    public List<SzgcYzUpload> getUploadFiles(Long jlId, Integer fileType) {
        SzgcYzUpload yzUpload = new SzgcYzUpload();
        yzUpload.setJlId(jlId);
        yzUpload.setFileType(fileType);

        return szgcYzUploadMapper.selectSzgcYzUploadList(yzUpload);
    }

    @Override
    public SzgcYzUpload getUploadFileById(Long fileId) {
        return szgcYzUploadMapper.selectSzgcYzUploadById(fileId);
    }

    @Override
    public int deleteUpload(Long uploadId) {
        return szgcYzUploadMapper.deleteSzgcYzUploadById(uploadId);
    }

    private SzgcYzPayment prepareYzPayment(SzgcJLPayment jlPayment) throws ParseException {
        SzgcYzPayment yzPayment = new SzgcYzPayment();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date jdate = df.parse(jlPayment.getJlDate());
        Calendar cal = Calendar.getInstance();
        cal.setTime(jdate);

        yzPayment.setJlId(jlPayment.getJlId());
        yzPayment.setConId(jlPayment.getConId());
        yzPayment.setJlType(jlPayment.getJlType());
        yzPayment.setJlDate(jlPayment.getJlDate());
        yzPayment.setJlYear(cal.get(Calendar.YEAR));
        yzPayment.setJlMonth(cal.get(Calendar.MONTH));
        yzPayment.setJlCustomNum(jlPayment.getJlCustomNum());
        yzPayment.setCzMoney(jlPayment.getCzMoney());
        yzPayment.setJlMoney(jlPayment.getJlMoney());
        yzPayment.setJlBGSMoney(jlPayment.getJlBGSMoney());
        yzPayment.setJlPayment(jlPayment.getJlPay());
        yzPayment.setJlBGSPayment(jlPayment.getJlBGSPay());
        yzPayment.setNeedPayment(jlPayment.getNeedPay());
        yzPayment.setRealPayment(jlPayment.getRealPay());
        yzPayment.setInputUser(jlPayment.getUserId());
        yzPayment.setInputDate(jlPayment.getInputDate());
        yzPayment.setMemo(jlPayment.getMemo());
        yzPayment.setDeleteMark(0);
        yzPayment.setRegisterId(jlPayment.getUserId());
        yzPayment.setOperatorId(jlPayment.getUserId());

        return yzPayment;
    }

    private SzgcYzPaymentItems prepareYzPaymentItem(SzgcJLPayment jlPayment, Integer pId, String pName){
        SzgcYzPaymentItems pt = new SzgcYzPaymentItems();
        pt.setJlId(jlPayment.getJlId());
        pt.setDetailId(pId);
        pt.setDetailName(pName);

        switch (pId){
            case 1:
                pt.setDetailValue(new BigDecimal(0));
                pt.setDetailMoney(jlPayment.getYfMoney());
                break;
            case 2:
                pt.setDetailValue(new BigDecimal(0));
                pt.setDetailMoney(jlPayment.getGcMoney());
                break;
            case 3:
                pt.setDetailValue(new BigDecimal(0));
                pt.setDetailMoney(jlPayment.getNmgMoney());
                break;
            case 4:
                pt.setDetailValue(jlPayment.getSlRate());
                pt.setDetailMoney(jlPayment.getSlMoney());
                break;
            case 5:
                pt.setDetailValue(jlPayment.getGlfRate());
                pt.setDetailMoney(jlPayment.getGlfMoney());
                break;
            case 6:
                pt.setDetailValue(jlPayment.getLrRate());
                pt.setDetailMoney(jlPayment.getLrMoney());
                break;
        }

        pt.setOrderNum(pId);

        return pt;
    }

    private int getJLArreasAge(String jldate){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int res = 0;
        try {
            Date src = sf.parse(jldate);
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c2.setTime(src);

            res = c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return res;
    }

}
