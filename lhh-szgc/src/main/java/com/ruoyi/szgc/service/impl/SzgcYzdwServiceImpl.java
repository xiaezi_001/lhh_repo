package com.ruoyi.szgc.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.mapper.SzgcContractMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcYzdwMapper;
import com.ruoyi.szgc.service.ISzgcYzdwService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业主单位Service业务层处理
 * 
 * @author lhh
 * @date 2020-09-19
 */
@Service
public class SzgcYzdwServiceImpl implements ISzgcYzdwService 
{
    @Autowired
    private SzgcYzdwMapper szgcYzdwMapper;

    @Autowired
    private SzgcContractMapper conMapper;

    /**
     * 查询业主单位
     * 
     * @param yzId 业主单位ID
     * @return 业主单位
     */
    @Override
    public SzgcYzdw selectSzgcYzdwById(Long yzId)
    {
        return szgcYzdwMapper.selectSzgcYzdwById(yzId);
    }

    /**
     * 查询业主单位列表
     * 
     * @param szgcYzdw 业主单位
     * @return 业主单位
     */
    @Override
    public List<SzgcYzdw> selectSzgcYzdwList(SzgcYzdw szgcYzdw)
    {
        SzgcYzdw query = new SzgcYzdw();
        query.setParentId(0l);

        List<SzgcYzdw> yzdw1 = szgcYzdwMapper.selectSzgcYzdwList(query);

        for (SzgcYzdw yz1: yzdw1) {
            query.setParentId(yz1.getYzId());
            yz1.setChildren(szgcYzdwMapper.selectSzgcYzdwList(query));
        }

        return yzdw1;
    }

    /**
     * 新增业主单位
     * 
     * @param szgcYzdw 业主单位
     * @return 结果
     */
    @Override
    public int insertSzgcYzdw(SzgcYzdw szgcYzdw)
    {
        return szgcYzdwMapper.insertSzgcYzdw(szgcYzdw);
    }

    /**
     * 修改业主单位
     * 
     * @param szgcYzdw 业主单位
     * @return 结果
     */
    @Override
    public int updateSzgcYzdw(SzgcYzdw szgcYzdw)
    {
        return szgcYzdwMapper.updateSzgcYzdw(szgcYzdw);
    }

    /**
     * 批量删除业主单位
     * 
     * @param yzIds 需要删除的业主单位ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcYzdwByIds(Long[] yzIds) throws Exception {
        SzgcYzdw yz = null;
        for (Long yzid: yzIds) {
            if(cannotDelete(yzid)){
                throw new Exception("业主单位存在合同数据，不能删除");
            }

            yz = szgcYzdwMapper.selectSzgcYzdwById(yzid);
            if(yz.getParentId() == 0l){
                szgcYzdwMapper.deleteSzgcYzdwByParenId(yzid);
            }
        }

        return szgcYzdwMapper.deleteSzgcYzdwByIds(yzIds);
    }

    /**
     * 删除业主单位信息
     * 
     * @param yzId 业主单位ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcYzdwById(Long yzId) throws Exception {
        if(this.cannotDelete(yzId)){
            throw new Exception("业主单位存在合同数据，不能删除");
        }

        SzgcYzdw yz = szgcYzdwMapper.selectSzgcYzdwById(yzId);

        if(yz.getParentId() == 0l)
        {
            szgcYzdwMapper.deleteSzgcYzdwByParenId(yzId);
        }

        return szgcYzdwMapper.deleteSzgcYzdwById(yzId);
    }

    @Override
    public List<SzgcSelectList> getYzdwDisplay() {
        List<SzgcYzdw> yzList = szgcYzdwMapper.selectSzgcYzdwList(new SzgcYzdw());
        List<SzgcSelectList> yzRes = new ArrayList<>();

        yzList.forEach(yz -> {
            SzgcSelectList item = new SzgcSelectList();

            item.setValue(yz.getYzId().toString());
            item.setLabel(yz.getYzName());
            item.setExtDataA(yz.getYzLinkMan());
            item.setExtDataB(yz.getYzLinkPhone());

            yzRes.add(item);
        });

        return yzRes;
    }

    @Override
    public List<SzgcSelectGroup> getYzdwGroupSelect() {
        SzgcYzdw query = new SzgcYzdw();
        query.setParentId(0L);

        List<SzgcYzdw> yzList1 = szgcYzdwMapper.selectSzgcYzdwList(query);
        List<SzgcYzdw> yzList2 = null;

        List<SzgcSelectGroup> res = new ArrayList<>();
        SzgcSelectGroup g1 = null;
        SzgcSelectGroup g2 = null;

        for(SzgcYzdw d: yzList1){
            g1 = new SzgcSelectGroup();
            g1.setLabel(d.getYzName());
            g1.setValue(d.getYzId());

            List<SzgcSelectGroup> groupIn = new ArrayList<>();
            query.setParentId(d.getYzId());
            yzList2 = szgcYzdwMapper.selectSzgcYzdwList(query);

            g2 = new SzgcSelectGroup();
            g2.setLabel(d.getYzName());
            g2.setValue(d.getYzId());
            groupIn.add(g2);
            for(SzgcYzdw d2: yzList2){
                g2 = new SzgcSelectGroup();
                g2.setLabel(d2.getYzName());
                g2.setValue(d2.getYzId());

                groupIn.add(g2);
            }
            g1.setOptions(groupIn);

            res.add(g1);
        }

        return res;
    }

    private Boolean cannotDelete(Long yzId){
        SzgcContract con = new SzgcContract();

        con.setConYz(yzId);

        List<SzgcContract> cons = conMapper.selectSzgcContractList(con);

        return cons.size() > 0;
    }
}
