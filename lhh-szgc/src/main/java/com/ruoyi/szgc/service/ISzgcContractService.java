package com.ruoyi.szgc.service;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcContract;
import com.ruoyi.szgc.domain.SzgcSelectList;

/**
 * 合同Service接口
 * 
 * @author lhh
 * @date 2020-09-17
 */
public interface ISzgcContractService 
{
    /**
     * 查询合同
     * 
     * @param conId 合同ID
     * @return 合同
     */
    public SzgcContract selectSzgcContractById(Long conId);

    /**
     * 查询合同列表
     * 
     * @param szgcContract 合同
     * @return 合同集合
     */
    public List<SzgcContract> selectSzgcContractList(SzgcContract szgcContract);

    /**
     * 新增合同
     * 
     * @param szgcContract 合同
     * @return 结果
     */
    public int insertSzgcContract(SzgcContract szgcContract);

    /**
     * 修改合同
     * 
     * @param szgcContract 合同
     * @return 结果
     */
    public int updateSzgcContract(SzgcContract szgcContract);

    /**
     * 批量删除合同
     * 
     * @param conIds 需要删除的合同ID
     * @return 结果
     */
    public int deleteSzgcContractByIds(Long[] conIds) throws Exception;

    /**
     * 删除合同信息
     * 
     * @param conId 合同ID
     * @return 结果
     */
    public int deleteSzgcContractById(Long conId) throws Exception;

    List<SzgcSelectList> getSelectConByName(String conName, Integer ctype);
}
