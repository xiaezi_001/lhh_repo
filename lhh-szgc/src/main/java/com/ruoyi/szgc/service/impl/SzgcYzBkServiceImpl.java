package com.ruoyi.szgc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.szgc.mapper.SzgcYzBkMapper;
import com.ruoyi.szgc.domain.SzgcYzBk;
import com.ruoyi.szgc.service.ISzgcYzBkService;

/**
 * 总公司拨款Service业务层处理
 * 
 * @author lhh
 * @date 2020-11-29
 */
@Service
public class SzgcYzBkServiceImpl implements ISzgcYzBkService 
{
    @Autowired
    private SzgcYzBkMapper szgcYzBkMapper;

    /**
     * 查询总公司拨款
     * 
     * @param bkId 总公司拨款ID
     * @return 总公司拨款
     */
    @Override
    public SzgcYzBk selectSzgcYzBkById(Long bkId)
    {
        return szgcYzBkMapper.selectSzgcYzBkById(bkId);
    }

    /**
     * 查询总公司拨款列表
     * 
     * @param szgcYzBk 总公司拨款
     * @return 总公司拨款
     */
    @Override
    public List<SzgcYzBk> selectSzgcYzBkList(SzgcYzBk szgcYzBk)
    {
        return szgcYzBkMapper.selectSzgcYzBkList(szgcYzBk);
    }

    /**
     * 新增总公司拨款
     * 
     * @param szgcYzBk 总公司拨款
     * @return 结果
     */
    @Override
    public int insertSzgcYzBk(SzgcYzBk szgcYzBk)
    {
        szgcYzBk.setCreateTime(DateUtils.getNowDate());
        szgcYzBk.setDeleteMark(0);
        return szgcYzBkMapper.insertSzgcYzBk(szgcYzBk);
    }

    /**
     * 修改总公司拨款
     * 
     * @param szgcYzBk 总公司拨款
     * @return 结果
     */
    @Override
    public int updateSzgcYzBk(SzgcYzBk szgcYzBk)
    {
        szgcYzBk.setUpdateTime(DateUtils.getNowDate());
        return szgcYzBkMapper.updateSzgcYzBk(szgcYzBk);
    }

    /**
     * 批量删除总公司拨款
     * 
     * @param bkIds 需要删除的总公司拨款ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzBkByIds(Long[] bkIds)
    {
        return szgcYzBkMapper.deleteSzgcYzBkByIds(bkIds);
    }

    /**
     * 删除总公司拨款信息
     * 
     * @param bkId 总公司拨款ID
     * @return 结果
     */
    @Override
    public int deleteSzgcYzBkById(Long bkId)
    {
        return szgcYzBkMapper.deleteSzgcYzBkById(bkId);
    }
}
