package com.ruoyi.szgc.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.szgc.domain.SzgcMainSumMoney;
import com.ruoyi.szgc.domain.SzgcRepSearch;
import com.ruoyi.szgc.domain.SzgcSearchResult;

import java.util.List;

public interface ISzgcExport {

    AjaxResult exportPayDetail(Long[] jlIds);

    AjaxResult exportYbb(SzgcRepSearch query);
    AjaxResult exportNBZQ(SzgcRepSearch query);
    AjaxResult exportBK(SzgcRepSearch query);
    AjaxResult exportGSZQ(SzgcRepSearch query);

    List<SzgcSearchResult> selectSearch(SzgcRepSearch query);

    SzgcMainSumMoney selectMainSum();
}
