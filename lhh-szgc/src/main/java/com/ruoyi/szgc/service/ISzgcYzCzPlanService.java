package com.ruoyi.szgc.service;

import java.util.List;
import com.ruoyi.szgc.domain.SzgcYzCzPlan;

/**
 * 产值计划Service接口
 * 
 * @author lhh
 * @date 2020-12-17
 */
public interface ISzgcYzCzPlanService 
{
    /**
     * 查询产值计划
     * 
     * @param czPlanId 产值计划ID
     * @return 产值计划
     */
    public SzgcYzCzPlan selectSzgcYzCzPlanById(Long czPlanId);

    /**
     * 查询产值计划列表
     * 
     * @param szgcYzCzPlan 产值计划
     * @return 产值计划集合
     */
    public List<SzgcYzCzPlan> selectSzgcYzCzPlanList(SzgcYzCzPlan szgcYzCzPlan);

    /**
     * 新增产值计划
     * 
     * @param szgcYzCzPlan 产值计划
     * @return 结果
     */
    public int insertSzgcYzCzPlan(SzgcYzCzPlan szgcYzCzPlan) throws Exception;

    /**
     * 修改产值计划
     * 
     * @param szgcYzCzPlan 产值计划
     * @return 结果
     */
    public int updateSzgcYzCzPlan(SzgcYzCzPlan szgcYzCzPlan);

    /**
     * 批量删除产值计划
     * 
     * @param czPlanIds 需要删除的产值计划ID
     * @return 结果
     */
    public int deleteSzgcYzCzPlanByIds(Long[] czPlanIds);

    /**
     * 删除产值计划信息
     * 
     * @param czPlanId 产值计划ID
     * @return 结果
     */
    public int deleteSzgcYzCzPlanById(Long czPlanId);
}
