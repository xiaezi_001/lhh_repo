package com.ruoyi.szgc.service.impl;

import com.ruoyi.szgc.domain.*;
import com.ruoyi.szgc.mapper.SzgcCmpPaymentCmpMapper;
import com.ruoyi.szgc.mapper.SzgcCmpPaymentItemsMapper;
import com.ruoyi.szgc.mapper.SzgcCmpPaymentMapper;
import com.ruoyi.szgc.service.ISzgcCmpPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 业主计量Service业务层处理
 * 
 * @author lhh
 * @date 2020-09-24
 */
@Service
public class SzgcCmpPaymentServiceImpl implements ISzgcCmpPaymentService
{
    @Autowired
    private SzgcCmpPaymentMapper szgcCmpPaymentMapper;

    @Autowired
    private SzgcCmpPaymentItemsMapper szgcCmpPaymentItemsMapper;

    @Autowired
    private SzgcCmpPaymentCmpMapper szgcCmpPaymentCmpMapper;
    /**
     * 查询业主计量
     * 
     * @param jlId 业主计量ID
     * @return 业主计量
     */
    @Override
    public SzgcJLPayment selectSzgcCmpPaymentById(Long jlId)
    {
        SzgcJLPayment res = new SzgcJLPayment();
        SzgcCmpPaymentItems query = new SzgcCmpPaymentItems();
        query.setJlId(jlId);

        SzgcCmpPayment CmpPayment = szgcCmpPaymentMapper.selectSzgcCmpPaymentById(jlId);
        List<SzgcCmpPaymentItems> jlItemList = szgcCmpPaymentItemsMapper.selectSzgcCmpPaymentItemsList(query);

        res.setJlId(CmpPayment.getJlId());
        res.setConId(CmpPayment.getConId());
        res.setJlType(CmpPayment.getJlType());
        res.setJlDate(CmpPayment.getJlDate());
        res.setJlCustomNum(CmpPayment.getJlCustomNum());
        res.setCzMoney(CmpPayment.getCzMoney());
        res.setJlMoney(CmpPayment.getJlMoney());
        res.setJlPay(CmpPayment.getJlPayment());
        res.setJlBGSPay(CmpPayment.getJlBGSPayment());
        res.setNeedPay(CmpPayment.getNeedPayment());
        res.setRealPay(CmpPayment.getRealPayment());
        res.setMemo(CmpPayment.getMemo());

        if(jlItemList.size() >= 3){
            res.setYfMoney(jlItemList.get(0).getDetailMoney());
            res.setGcMoney(jlItemList.get(1).getDetailMoney());
            res.setNmgMoney(jlItemList.get(2).getDetailMoney());
        }
        return res;
    }

    @Override
    public List<SzgcCmpPayment> selectSzgcCmpPaymentByConId(Long conId) {
        return szgcCmpPaymentMapper.selectSzgcCmpPaymentByConId(conId);
    }
    /**
     * 查询业主计量列表
     * 
     * @param CmpPayment 业主计量
     * @return 业主计量
     */
    @Override
    public List<SzgcCmpPayment> selectSzgcCmpPaymentList(SzgcCmpPayment CmpPayment)
    {
        return szgcCmpPaymentMapper.selectSzgcCmpPaymentList(CmpPayment);
    }

    /**
     * 新增业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int insertSzgcCmpPayment(SzgcJLPayment jlPayment) throws ParseException {

        SzgcCmpPayment CmpPayment = this.prepareCmpPayment(jlPayment);

        //check year and month
        SzgcCmpPayment jlcheck = new SzgcCmpPayment();
        jlcheck.setConId(CmpPayment.getConId());
        jlcheck.setJlYear(CmpPayment.getJlYear());
        jlcheck.setJlMonth(CmpPayment.getJlMonth());

        List<SzgcCmpPayment> checkRes = szgcCmpPaymentMapper.selectSzgcCmpPaymentList(jlcheck);
        if(checkRes.size() > 0){
            jlPayment.setMemo("已存在改月份的计量, 请检查结算日期");
            return -1;
        }

        if(jlPayment.getJlType() == 1){
            jlcheck.setJlYear(null);
            jlcheck.setJlMonth(null);
            jlcheck.setJlType(1);

            checkRes = szgcCmpPaymentMapper.selectSzgcCmpPaymentList(jlcheck);
            if(checkRes.size() > 0){
                jlPayment.setMemo("该项目已存在决算");
                return -2;
            }
        }

        int res = szgcCmpPaymentMapper.insertSzgcCmpPayment(CmpPayment);

        //payment item
        SzgcCmpPaymentItems ptyf = this.prepareCmpPaymentItem(jlPayment, 1, "预付款");
        SzgcCmpPaymentItems ptgc = this.prepareCmpPaymentItem(jlPayment, 2, "工程款");
        SzgcCmpPaymentItems ptnmg = this.prepareCmpPaymentItem(jlPayment, 3, "农民工");

        ptyf.setJlId(CmpPayment.getJlId());
        ptgc.setJlId(CmpPayment.getJlId());
        ptnmg.setJlId(CmpPayment.getJlId());

        szgcCmpPaymentItemsMapper.insertSzgcCmpPaymentItems(ptyf);
        szgcCmpPaymentItemsMapper.insertSzgcCmpPaymentItems(ptgc);
        szgcCmpPaymentItemsMapper.insertSzgcCmpPaymentItems(ptnmg);

        return res;
    }

    /**
     * 修改业主计量
     * 
     * @param jlPayment 业主计量
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int updateSzgcCmpPayment(SzgcJLPayment jlPayment) throws ParseException {
        SzgcCmpPayment CmpPayment = this.prepareCmpPayment(jlPayment);
        int res = szgcCmpPaymentMapper.updateSzgcCmpPayment(CmpPayment);

        //payment item
        SzgcCmpPaymentItems ptyf = this.prepareCmpPaymentItem(jlPayment, 1, "预付款");
        SzgcCmpPaymentItems ptgc = this.prepareCmpPaymentItem(jlPayment, 2, "工程款");
        SzgcCmpPaymentItems ptnmg = this.prepareCmpPaymentItem(jlPayment, 3, "农民工");

        szgcCmpPaymentItemsMapper.updateSzgcCmpPaymentItems(ptyf);
        szgcCmpPaymentItemsMapper.updateSzgcCmpPaymentItems(ptgc);
        szgcCmpPaymentItemsMapper.updateSzgcCmpPaymentItems(ptnmg);

        return res;
    }

    /**
     * 批量删除业主计量
     * 
     * @param jlIds 需要删除的业主计量ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcCmpPaymentByIds(Long[] jlIds) {
        szgcCmpPaymentItemsMapper.deleteSzgcCmpPaymentItemsByIds(jlIds);
        szgcCmpPaymentCmpMapper.deleteSzgcCmpPaymentCmpByIds(jlIds);
        return szgcCmpPaymentMapper.deleteSzgcCmpPaymentByIds(jlIds);
    }

    /**
     * 删除业主计量信息
     * 
     * @param jlId 业主计量ID
     * @return 结果
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteSzgcCmpPaymentById(Long jlId)
    {
        szgcCmpPaymentItemsMapper.deleteSzgcCmpPaymentItemsById(jlId);
        szgcCmpPaymentCmpMapper.deleteSzgcCmpPaymentCmpById(jlId);
        return szgcCmpPaymentMapper.deleteSzgcCmpPaymentById(jlId);
    }

    @Override
    public SzgcJLSumMoney getJLSumByConId(Long conId) {
        /*
        SzgcCmpPayment hasJS = new SzgcCmpPayment();
        hasJS.setConId(conId);
        hasJS.setJlType(1);

        List<SzgcCmpPayment> jsList = szgcCmpPaymentMapper.selectSzgcCmpPaymentList(hasJS);
        if(jsList.size() > 0){
            SzgcJLSumMoney jsSum = new SzgcJLSumMoney();
            jsSum.setCzSumMoney(jsList.get(0).getCzMoney());
            jsSum.setJlSumMoney(jsList.get(0).getJlMoney());
            jsSum.setJlSumPayMoney(jsList.get(0).getJlPayment());
            jsSum.setJlBGSSumPayMoney(jsList.get(0).getJlBGSPayment());
            jsSum.setJlSumNeedPay(jsList.get(0).getNeedPayment());
            jsSum.setJlSumRealPay(jsList.get(0).getRealPayment());
            jsSum.setAccountAges(this.getJLArreasAge(jsList.get(0).getJlDate()));
            return jsSum;
        }
        else{
            return szgcCmpPaymentMapper.selectJlSumByConId(conId);
        }
         */
        return null;
    }

    @Override
    public List<SzgcCmpPaymentCmp> getCmpPaymentList(Long jlId) {
        return szgcCmpPaymentCmpMapper.getCmpPaymentCmpListByJlId(jlId);
    }

    @Override
    public int saveCmpJLCmpPayment(List<SzgcCmpPaymentCmp> Cmpcmps) {

        for (SzgcCmpPaymentCmp cmp: Cmpcmps) {
            cmp.setDeleteMark(0);

            if(cmp.getJlCmpId() == 0){
                szgcCmpPaymentCmpMapper.insertSzgcCmpPaymentCmp(cmp);
            }
            else{
                szgcCmpPaymentCmpMapper.updateSzgcCmpPaymentCmp(cmp);
            }
        }
        return Cmpcmps.size();
    }


    private SzgcCmpPayment prepareCmpPayment(SzgcJLPayment jlPayment) throws ParseException {
        SzgcCmpPayment CmpPayment = new SzgcCmpPayment();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date jdate = df.parse(jlPayment.getJlDate());
        Calendar cal = Calendar.getInstance();
        cal.setTime(jdate);

        CmpPayment.setJlId(jlPayment.getJlId());
        CmpPayment.setConId(jlPayment.getConId());
        CmpPayment.setJlType(jlPayment.getJlType());
        CmpPayment.setJlDate(jlPayment.getJlDate());
        CmpPayment.setJlYear(cal.get(Calendar.YEAR));
        CmpPayment.setJlMonth(cal.get(Calendar.MONTH));
        CmpPayment.setJlCustomNum(jlPayment.getJlCustomNum());
        CmpPayment.setCzMoney(jlPayment.getCzMoney());
        CmpPayment.setJlMoney(jlPayment.getJlMoney());
        CmpPayment.setJlPayment(jlPayment.getJlPay());
        CmpPayment.setJlBGSPayment(jlPayment.getJlBGSPay());
        CmpPayment.setNeedPayment(jlPayment.getNeedPay());
        CmpPayment.setRealPayment(jlPayment.getRealPay());
        CmpPayment.setInputUser(jlPayment.getUserId());
        CmpPayment.setMemo(jlPayment.getMemo());
        CmpPayment.setDeleteMark(0);
        CmpPayment.setRegisterId(jlPayment.getUserId());
        CmpPayment.setOperatorId(jlPayment.getUserId());

        return CmpPayment;
    }

    private SzgcCmpPaymentItems prepareCmpPaymentItem(SzgcJLPayment jlPayment, Integer pId, String pName){
        SzgcCmpPaymentItems pt = new SzgcCmpPaymentItems();
        pt.setJlId(jlPayment.getJlId());
        pt.setDetailId(pId);
        pt.setDetailName(pName);

        switch (pId){
            case 1:
                pt.setDetailValue(new BigDecimal(0));
                pt.setDetailMoney(jlPayment.getYfMoney());
                break;
            case 2:
                pt.setDetailValue(new BigDecimal(0));
                pt.setDetailMoney(jlPayment.getGcMoney());
                break;
            case 3:
                pt.setDetailValue(new BigDecimal(0));
                pt.setDetailMoney(jlPayment.getNmgMoney());
                break;
            case 4:
                pt.setDetailValue(jlPayment.getSlRate());
                pt.setDetailMoney(jlPayment.getSlMoney());
                break;
            case 5:
                pt.setDetailValue(jlPayment.getGlfRate());
                pt.setDetailMoney(jlPayment.getGlfMoney());
                break;
            case 6:
                pt.setDetailValue(jlPayment.getLrRate());
                pt.setDetailMoney(jlPayment.getLrMoney());
                break;
        }

        pt.setOrderNum(pId);

        return pt;
    }

    private int getJLArreasAge(String jldate){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int res = 0;
        try {
            Date src = sf.parse(jldate);
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c2.setTime(src);

            res = c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return res;
    }
}
