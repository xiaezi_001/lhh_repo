package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepYbb implements Serializable {

    //管理状态
    private Integer conFlag;
    private String glzt;
    //业主单位
    private String yzdw;
    //项目名称
    private String conName;
    //项目类型
    private String conType;
    //项目状态
    private String conStatus;
    //合同总额
    private BigDecimal conMoney;
    //产值-开工累计
    private BigDecimal czSum;
    //产值-剩余累计
    private BigDecimal czRemain;
    //产值-总计划
    private BigDecimal czPlan;
    //产值-当年计划
    private BigDecimal czYearPlan;
    //产值-当月累计
    private BigDecimal czMonth;
    //产值-当年累计
    private BigDecimal czYear;
    //产值-当年剩余
    private BigDecimal czYearRemain;
    //计量-决算
    private BigDecimal jsMoney;
    //计量-开工累计
    private BigDecimal jlSum;
    //计量-当月累计
    private BigDecimal jlMonth;
    //计量-当年累计
    private BigDecimal jlYear;
    //进款-开工累计进款
    private BigDecimal jkSum;
    //进款-当月累计
    private BigDecimal jkMonth;
    //进款-当年累计
    private BigDecimal jkYear;
    //拨款-累计拨款
    private BigDecimal bkSum;
    //拨款-当年累计
    private BigDecimal bkYear;

    public Integer getConFlag() {
        return conFlag;
    }

    public void setConFlag(Integer conFlag) {
        this.conFlag = conFlag;
    }

    public String getGlzt() {
        return glzt;
    }

    public void setGlzt(String glzt) {
        this.glzt = glzt;
    }

    public String getYzdw() {
        return yzdw;
    }

    public void setYzdw(String yzdw) {
        this.yzdw = yzdw;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getConType() {
        return conType;
    }

    public void setConType(String conType) {
        this.conType = conType;
    }

    public String getConStatus() {
        return conStatus;
    }

    public void setConStatus(String conStatus) {
        this.conStatus = conStatus;
    }

    public BigDecimal getConMoney() {
        return conMoney;
    }

    public void setConMoney(BigDecimal conMoney) {
        this.conMoney = conMoney;
    }

    public BigDecimal getCzSum() {
        return czSum;
    }

    public void setCzSum(BigDecimal czSum) {
        this.czSum = czSum;
    }

    public BigDecimal getCzRemain() {
        return czRemain;
    }

    public void setCzRemain(BigDecimal czRemain) {
        this.czRemain = czRemain;
    }

    public BigDecimal getCzPlan() {
        return czPlan;
    }

    public void setCzPlan(BigDecimal czPlan) {
        this.czPlan = czPlan;
    }

    public BigDecimal getCzYearPlan() {
        return czYearPlan;
    }

    public void setCzYearPlan(BigDecimal czYearPlan) {
        this.czYearPlan = czYearPlan;
    }

    public BigDecimal getCzMonth() {
        return czMonth;
    }

    public void setCzMonth(BigDecimal czMonth) {
        this.czMonth = czMonth;
    }

    public BigDecimal getCzYear() {
        return czYear;
    }

    public void setCzYear(BigDecimal czYear) {
        this.czYear = czYear;
    }

    public BigDecimal getCzYearRemain() {
        return czYearRemain;
    }

    public void setCzYearRemain(BigDecimal czYearRemain) {
        this.czYearRemain = czYearRemain;
    }

    public BigDecimal getJsMoney() {
        return jsMoney;
    }

    public void setJsMoney(BigDecimal jsMoney) {
        this.jsMoney = jsMoney;
    }

    public BigDecimal getJlSum() {
        return jlSum;
    }

    public void setJlSum(BigDecimal jlSum) {
        this.jlSum = jlSum;
    }

    public BigDecimal getJlMonth() {
        return jlMonth;
    }

    public void setJlMonth(BigDecimal jlMonth) {
        this.jlMonth = jlMonth;
    }

    public BigDecimal getJlYear() {
        return jlYear;
    }

    public void setJlYear(BigDecimal jlYear) {
        this.jlYear = jlYear;
    }

    public BigDecimal getJkSum() {
        return jkSum;
    }

    public void setJkSum(BigDecimal jkSum) {
        this.jkSum = jkSum;
    }

    public BigDecimal getJkMonth() {
        return jkMonth;
    }

    public void setJkMonth(BigDecimal jkMonth) {
        this.jkMonth = jkMonth;
    }

    public BigDecimal getJkYear() {
        return jkYear;
    }

    public void setJkYear(BigDecimal jkYear) {
        this.jkYear = jkYear;
    }

    public BigDecimal getBkSum() {
        return bkSum;
    }

    public void setBkSum(BigDecimal bkSum) {
        this.bkSum = bkSum;
    }

    public BigDecimal getBkYear() {
        return bkYear;
    }

    public void setBkYear(BigDecimal bkYear) {
        this.bkYear = bkYear;
    }

    public void reset(){
        this.setConMoney(new BigDecimal(0));
        this.setCzSum(new BigDecimal(0));
        this.setCzRemain(new BigDecimal(0));
        this.setCzYearPlan(new BigDecimal(0));
        this.setCzMonth(new BigDecimal(0));
        this.setCzYear(new BigDecimal(0));
        this.setCzYearRemain(new BigDecimal(0));
        this.setJlSum(new BigDecimal(0));
        this.setJlMonth(new BigDecimal(0));
        this.setJlYear(new BigDecimal(0));
        this.setJkSum(new BigDecimal(0));
        this.setJkMonth(new BigDecimal(0));
        this.setJkYear(new BigDecimal(0));
        this.setBkSum(new BigDecimal(0));
        this.setBkYear(new BigDecimal(0));
    }

    @Override
    public String toString() {
        return "SzgcRepYbb{" +
                "conFlag=" + conFlag +
                ", glzt='" + glzt + '\'' +
                ", yzdw='" + yzdw + '\'' +
                ", conName='" + conName + '\'' +
                ", conType='" + conType + '\'' +
                ", conMoney=" + conMoney +
                ", czSum=" + czSum +
                ", czRemain=" + czRemain +
                ", czPlan=" + czPlan +
                ", czMonth=" + czMonth +
                ", czYear=" + czYear +
                ", czYearRemain=" + czYearRemain +
                ", jlSum=" + jlSum +
                ", jlMonth=" + jlMonth +
                ", jlYear=" + jlYear +
                ", jkSum=" + jkSum +
                ", jkMonth=" + jkMonth +
                ", jkYear=" + jkYear +
                ", bkSum=" + bkSum +
                ", bkYear=" + bkYear +
                '}';
    }
}
