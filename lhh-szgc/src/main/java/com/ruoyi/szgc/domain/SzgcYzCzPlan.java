package com.ruoyi.szgc.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产值计划对象 szgc_yz_cz_plan
 * 
 * @author lhh
 * @date 2020-12-17
 */
public class SzgcYzCzPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 计划编号 */
    private Long czPlanId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private Long conId;

    /** 计划客户编号 */
    @Excel(name = "计划客户编号")
    private String czPlanNo;

    /** 计划年份 */
    @Excel(name = "计划年份")
    private Integer czPlanYear;

    /** 计划金额 */
    @Excel(name = "计划金额")
    private BigDecimal czPlanMoney;

    /** 备注 */
    @Excel(name = "备注")
    private String czPlanMemo;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Integer deleteMark;

    public void setCzPlanId(Long czPlanId) 
    {
        this.czPlanId = czPlanId;
    }

    public Long getCzPlanId() 
    {
        return czPlanId;
    }
    public void setConId(Long conId) 
    {
        this.conId = conId;
    }

    public Long getConId() 
    {
        return conId;
    }
    public void setCzPlanNo(String czPlanNo) 
    {
        this.czPlanNo = czPlanNo;
    }

    public String getCzPlanNo() 
    {
        return czPlanNo;
    }
    public void setCzPlanYear(Integer czPlanYear) 
    {
        this.czPlanYear = czPlanYear;
    }

    public Integer getCzPlanYear() 
    {
        return czPlanYear;
    }
    public void setCzPlanMoney(BigDecimal czPlanMoney) 
    {
        this.czPlanMoney = czPlanMoney;
    }

    public BigDecimal getCzPlanMoney() 
    {
        return czPlanMoney;
    }
    public void setCzPlanMemo(String czPlanMemo) 
    {
        this.czPlanMemo = czPlanMemo;
    }

    public String getCzPlanMemo() 
    {
        return czPlanMemo;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("czPlanId", getCzPlanId())
            .append("conId", getConId())
            .append("czPlanNo", getCzPlanNo())
            .append("czPlanYear", getCzPlanYear())
            .append("czPlanMoney", getCzPlanMoney())
            .append("czPlanMemo", getCzPlanMemo())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
