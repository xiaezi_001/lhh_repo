package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.util.List;

public class SzgcSelectGroup implements Serializable {

    private String label;
    private Long value;
    private List<SzgcSelectGroup> options;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public List<SzgcSelectGroup> getOptions() {
        return options;
    }

    public void setOptions(List<SzgcSelectGroup> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "SzgcSelectGroup{" +
                "label='" + label + '\'' +
                ", value='" + value + '\'' +
                ", options=" + options +
                '}';
    }
}
