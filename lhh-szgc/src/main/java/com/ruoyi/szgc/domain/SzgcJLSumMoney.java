package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcJLSumMoney implements Serializable {

    //本年计划产值
    private BigDecimal czYearPlan;
    //产值合计
    private BigDecimal czSumMoney;
    //计量批复合计
    private BigDecimal pfSumMoney;
    //业主支付合计
    private BigDecimal yzPaySumMoney;
    //债权合计 = 计量批复 - 业主支付
    private BigDecimal zqSumMoney;
    //本公司划拨合计
    private BigDecimal bgsSumMoney;
    //总公司应拨
    private BigDecimal ybSumMoney;
    //总公司实拨
    private BigDecimal sbSumMoney;
    //累计欠款 = 应拨 - 实拨
    private BigDecimal qkSumMoney;
    //账龄
    private Integer accountAges;

    public BigDecimal getCzYearPlan() {
        return czYearPlan;
    }

    public void setCzYearPlan(BigDecimal czYearPlan) {
        this.czYearPlan = czYearPlan;
    }

    public BigDecimal getCzSumMoney() {
        return czSumMoney;
    }

    public void setCzSumMoney(BigDecimal czSumMoney) {
        this.czSumMoney = czSumMoney;
    }

    public BigDecimal getPfSumMoney() {
        return pfSumMoney;
    }

    public void setPfSumMoney(BigDecimal pfSumMoney) {
        this.pfSumMoney = pfSumMoney;
    }

    public BigDecimal getYzPaySumMoney() {
        return yzPaySumMoney;
    }

    public void setYzPaySumMoney(BigDecimal yzPaySumMoney) {
        this.yzPaySumMoney = yzPaySumMoney;
    }

    public BigDecimal getZqSumMoney() {
        return zqSumMoney;
    }

    public void setZqSumMoney(BigDecimal zqSumMoney) {
        this.zqSumMoney = zqSumMoney;
    }

    public BigDecimal getBgsSumMoney() {
        return bgsSumMoney;
    }

    public void setBgsSumMoney(BigDecimal bgsSumMoney) {
        this.bgsSumMoney = bgsSumMoney;
    }

    public BigDecimal getYbSumMoney() {
        return ybSumMoney;
    }

    public void setYbSumMoney(BigDecimal ybSumMoney) {
        this.ybSumMoney = ybSumMoney;
    }

    public BigDecimal getSbSumMoney() {
        return sbSumMoney;
    }

    public void setSbSumMoney(BigDecimal sbSumMoney) {
        this.sbSumMoney = sbSumMoney;
    }

    public BigDecimal getQkSumMoney() {
        return qkSumMoney;
    }

    public void setQkSumMoney(BigDecimal qkSumMoney) {
        this.qkSumMoney = qkSumMoney;
    }

    public Integer getAccountAges() {
        return accountAges;
    }

    public void setAccountAges(Integer accountAges) {
        this.accountAges = accountAges;
    }
}
