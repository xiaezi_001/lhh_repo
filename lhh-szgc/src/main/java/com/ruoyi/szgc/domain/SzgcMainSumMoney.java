package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcMainSumMoney implements Serializable {

    private BigDecimal czSum;
    private BigDecimal jlSum;
    private BigDecimal jkSum;
    private BigDecimal bkSum;
    private BigDecimal zqSum;
    private BigDecimal zqyqrSum;
    private BigDecimal zqwqrSum;

    public BigDecimal getCzSum() {
        return czSum;
    }

    public void setCzSum(BigDecimal czSum) {
        this.czSum = czSum;
    }

    public BigDecimal getJlSum() {
        return jlSum;
    }

    public void setJlSum(BigDecimal jlSum) {
        this.jlSum = jlSum;
    }

    public BigDecimal getJkSum() {
        return jkSum;
    }

    public void setJkSum(BigDecimal jkSum) {
        this.jkSum = jkSum;
    }

    public BigDecimal getBkSum() {
        return bkSum;
    }

    public void setBkSum(BigDecimal bkSum) {
        this.bkSum = bkSum;
    }

    public BigDecimal getZqSum() {
        return zqSum;
    }

    public void setZqSum(BigDecimal zqSum) {
        this.zqSum = zqSum;
    }

    public BigDecimal getZqyqrSum() {
        return zqyqrSum;
    }

    public void setZqyqrSum(BigDecimal zqyqrSum) {
        this.zqyqrSum = zqyqrSum;
    }

    public BigDecimal getZqwqrSum() {
        return zqwqrSum;
    }

    public void setZqwqrSum(BigDecimal zqwqrSum) {
        this.zqwqrSum = zqwqrSum;
    }

    public void reset(){
        this.czSum = new BigDecimal(0);
        this.jlSum = new BigDecimal(0);
        this.jkSum = new BigDecimal(0);
        this.bkSum = new BigDecimal(0);
        this.zqSum = new BigDecimal(0);
        this.zqyqrSum = new BigDecimal(0);
        this.zqwqrSum = new BigDecimal(0);
    }
}
