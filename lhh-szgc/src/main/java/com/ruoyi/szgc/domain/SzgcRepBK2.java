package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepBK2 implements Serializable {

    private String yzName;
    private String conName;
    private String startYear;
    private String endYear;
    private BigDecimal conMoney;
    private BigDecimal jlSum;
    private BigDecimal jkSum;
    private BigDecimal jkFGSSum;
    private BigDecimal jkBGSSum;
    private BigDecimal jkSFSum;
    private BigDecimal jkGLFSum;
    private BigDecimal jkLRSum;
    private BigDecimal jkKJSum;
    private BigDecimal jkYBSum;
    private BigDecimal jkBKSum;
    private String lastBKDate;
    private BigDecimal cmpQFSum;

    public String getYzName() {
        return yzName;
    }

    public void setYzName(String yzName) {
        this.yzName = yzName;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public BigDecimal getConMoney() {
        return conMoney;
    }

    public void setConMoney(BigDecimal conMoney) {
        this.conMoney = conMoney;
    }

    public BigDecimal getJlSum() {
        return jlSum;
    }

    public void setJlSum(BigDecimal jlSum) {
        this.jlSum = jlSum;
    }

    public BigDecimal getJkSum() {
        return jkSum;
    }

    public void setJkSum(BigDecimal jkSum) {
        this.jkSum = jkSum;
    }

    public BigDecimal getJkFGSSum() {
        return jkFGSSum;
    }

    public void setJkFGSSum(BigDecimal jkFGSSum) {
        this.jkFGSSum = jkFGSSum;
    }

    public BigDecimal getJkBGSSum() {
        return jkBGSSum;
    }

    public void setJkBGSSum(BigDecimal jkBGSSum) {
        this.jkBGSSum = jkBGSSum;
    }

    public BigDecimal getJkSFSum() {
        return jkSFSum;
    }

    public void setJkSFSum(BigDecimal jkSFSum) {
        this.jkSFSum = jkSFSum;
    }

    public BigDecimal getJkGLFSum() {
        return jkGLFSum;
    }

    public void setJkGLFSum(BigDecimal jkGLFSum) {
        this.jkGLFSum = jkGLFSum;
    }

    public BigDecimal getJkLRSum() {
        return jkLRSum;
    }

    public void setJkLRSum(BigDecimal jkLRSum) {
        this.jkLRSum = jkLRSum;
    }

    public BigDecimal getJkKJSum() {
        return jkKJSum;
    }

    public void setJkKJSum(BigDecimal jkKJSum) {
        this.jkKJSum = jkKJSum;
    }

    public BigDecimal getJkYBSum() {
        return jkYBSum;
    }

    public void setJkYBSum(BigDecimal jkYBSum) {
        this.jkYBSum = jkYBSum;
    }

    public BigDecimal getJkBKSum() {
        return jkBKSum;
    }

    public void setJkBKSum(BigDecimal jkBKSum) {
        this.jkBKSum = jkBKSum;
    }

    public String getLastBKDate() {
        return lastBKDate;
    }

    public void setLastBKDate(String lastBKDate) {
        this.lastBKDate = lastBKDate;
    }

    public BigDecimal getCmpQFSum() {
        return cmpQFSum;
    }

    public void setCmpQFSum(BigDecimal cmpQFSum) {
        this.cmpQFSum = cmpQFSum;
    }

    public void reset()
    {
        this.conMoney = new BigDecimal(0);
        this.jlSum = new BigDecimal(0);
        this.jkSum = new BigDecimal(0);
        this.jkFGSSum = new BigDecimal(0);
        this.jkBGSSum = new BigDecimal(0);
        this.jkSFSum = new BigDecimal(0);
        this.jkGLFSum = new BigDecimal(0);
        this.jkLRSum = new BigDecimal(0);
        this.jkKJSum = new BigDecimal(0);
        this.jkYBSum = new BigDecimal(0);
        this.jkBKSum = new BigDecimal(0);
        this.cmpQFSum = new BigDecimal(0);
    }
}
