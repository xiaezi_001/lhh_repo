package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepNBZQ implements Serializable {

    private Long conID;
    private String conName;
    private BigDecimal cmpMoney;

    public Long getConID() {
        return conID;
    }

    public void setConID(Long conID) {
        this.conID = conID;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public BigDecimal getCmpMoney() {
        return cmpMoney;
    }

    public void setCmpMoney(BigDecimal cmpMoney) {
        this.cmpMoney = cmpMoney;
    }

    public void reset(){
        this.setCmpMoney(new BigDecimal(0));
    }

    @Override
    public String toString() {
        return "SzgcRepNBZQ{" +
                "conID=" + conID +
                ", conName='" + conName + '\'' +
                ", cmpMoney=" + cmpMoney +
                '}';
    }
}
