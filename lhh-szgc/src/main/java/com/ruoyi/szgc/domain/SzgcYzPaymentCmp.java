package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主计量分公司对象 szgc_yz_payment_cmp
 * 
 * @author lhh
 * @date 2020-09-24
 */
public class SzgcYzPaymentCmp implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long jlCmpId;

    /** 计量编号 */
    @Excel(name = "计量编号")
    private Long jlId;

    private int payType;

    /** 分公司编号 */
    @Excel(name = "分公司编号")
    private Integer cmpId;

    /** 分公司名称 */
    @Excel(name = "分公司名称")
    private String cmpName;

    /** 计量金额 */
    @Excel(name = "计量金额")
    private BigDecimal cmpMoney;

    /** $column.columnComment */
    @Excel(name = "计量金额")
    private Integer deleteMark;

    public void setJlCmpId(Long jlCmpId) 
    {
        this.jlCmpId = jlCmpId;
    }

    public Long getJlCmpId() 
    {
        return jlCmpId;
    }
    public void setJlId(Long jlId) 
    {
        this.jlId = jlId;
    }

    public Long getJlId() 
    {
        return jlId;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public void setCmpId(Integer cmpId)
    {
        this.cmpId = cmpId;
    }

    public Integer getCmpId() 
    {
        return cmpId;
    }
    public void setCmpName(String cmpName) 
    {
        this.cmpName = cmpName;
    }

    public String getCmpName() 
    {
        return cmpName;
    }
    public void setCmpMoney(BigDecimal cmpMoney) 
    {
        this.cmpMoney = cmpMoney;
    }

    public BigDecimal getCmpMoney() 
    {
        return cmpMoney;
    }
    public void setDeleteMark(Integer deleteMark)
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark()
    {
        return deleteMark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jlCmpId", getJlCmpId())
            .append("jlId", getJlId())
            .append("payType", getPayType())
            .append("cmpId", getCmpId())
            .append("cmpName", getCmpName())
            .append("cmpMoney", getCmpMoney())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
