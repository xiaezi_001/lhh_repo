package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcSearchDetail implements Serializable {

    private Long conId;
    private String tableName;
    private String colDate;
    private String colMoney;
    private String payDate;
    private BigDecimal payMoney;
    private BigDecimal payMoney2;
    private BigDecimal payMoney3;
    private BigDecimal payMoney4;
    private BigDecimal payMoney5;
    private BigDecimal payMoney6;
    private BigDecimal payMoney7;
    private Integer payType;
    private String startDate;
    private String endDate;

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColDate() {
        return colDate;
    }

    public void setColDate(String colDate) {
        this.colDate = colDate;
    }

    public String getColMoney() {
        return colMoney;
    }

    public void setColMoney(String colMoney) {
        this.colMoney = colMoney;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getPayMoney2() {
        return payMoney2;
    }

    public void setPayMoney2(BigDecimal payMoney2) {
        this.payMoney2 = payMoney2;
    }

    public BigDecimal getPayMoney3() {
        return payMoney3;
    }

    public void setPayMoney3(BigDecimal payMoney3) {
        this.payMoney3 = payMoney3;
    }

    public BigDecimal getPayMoney4() {
        return payMoney4;
    }

    public void setPayMoney4(BigDecimal payMoney4) {
        this.payMoney4 = payMoney4;
    }

    public BigDecimal getPayMoney5() {
        return payMoney5;
    }

    public void setPayMoney5(BigDecimal payMoney5) {
        this.payMoney5 = payMoney5;
    }

    public BigDecimal getPayMoney6() {
        return payMoney6;
    }

    public void setPayMoney6(BigDecimal payMoney6) {
        this.payMoney6 = payMoney6;
    }

    public BigDecimal getPayMoney7() {
        return payMoney7;
    }

    public void setPayMoney7(BigDecimal payMoney7) {
        this.payMoney7 = payMoney7;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
