package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class SzgcJLConData implements Serializable {

    private SzgcContract contract;
    private SzgcJLSumMoney jlSumInfo;
    private BigDecimal lastSL;
    private BigDecimal lastGLF;
    private BigDecimal lastLR;

    private List<SzgcYzPaymentCmp> cmpList;


    public SzgcContract getContract() {
        return contract;
    }

    public void setContract(SzgcContract contract) {
        this.contract = contract;
    }

    public SzgcJLSumMoney getJlSumInfo() {
        return jlSumInfo;
    }

    public void setJlSumInfo(SzgcJLSumMoney jlSumInfo) {
        this.jlSumInfo = jlSumInfo;
    }

    public BigDecimal getLastSL() {
        return lastSL;
    }

    public void setLastSL(BigDecimal lastSL) {
        this.lastSL = lastSL;
    }

    public BigDecimal getLastGLF() {
        return lastGLF;
    }

    public void setLastGLF(BigDecimal lastGLF) {
        this.lastGLF = lastGLF;
    }

    public BigDecimal getLastLR() {
        return lastLR;
    }

    public void setLastLR(BigDecimal lastLR) {
        this.lastLR = lastLR;
    }

    public List<SzgcYzPaymentCmp> getCmpList() {
        return cmpList;
    }

    public void setCmpList(List<SzgcYzPaymentCmp> cmpList) {
        this.cmpList = cmpList;
    }
}
