package com.ruoyi.szgc.domain;

import java.io.Serializable;

public class SzgcRepSearch implements Serializable {

    private String startDate;
    private String endDate;
    private Integer month;
    private Integer year;
    private Integer conType;
    private Integer cmpId;
    private Integer conAccount;
    private Long yzId;
    private Integer conStatus;
    private Integer conJSStatus;
    private Integer isYZParent;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getConType() {
        return conType;
    }

    public void setConType(Integer conType) {
        this.conType = conType;
    }

    public Integer getCmpId() {
        return cmpId;
    }

    public void setCmpId(Integer cmpId) {
        this.cmpId = cmpId;
    }

    public Integer getConAccount() {
        return conAccount;
    }

    public void setConAccount(Integer conAccount) {
        this.conAccount = conAccount;
    }

    public Long getYzId() {
        return yzId;
    }

    public void setYzId(Long yzid) {
        this.yzId = yzid;
    }

    public Integer getConStatus() {
        return conStatus;
    }

    public void setConStatus(Integer conStatus) {
        this.conStatus = conStatus;
    }

    public Integer getConJSStatus() {
        return conJSStatus;
    }

    public void setConJSStatus(Integer conJSStatus) {
        this.conJSStatus = conJSStatus;
    }

    public Integer getYZParent() {
        return isYZParent;
    }

    public void setYZParent(Integer YZParent) {
        isYZParent = YZParent;
    }
}
