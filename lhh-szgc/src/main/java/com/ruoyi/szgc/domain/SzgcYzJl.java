package com.ruoyi.szgc.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主计量对象 szgc_yz_jl
 * 
 * @author lhh
 * @date 2020-11-29
 */
public class SzgcYzJl extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long jlId;

    private Long conId;

    /** 计量编号 */
    @Excel(name = "计量编号")
    private String jlCustNo;

    /** 结算日期 */
    @Excel(name = "结算日期", width = 30, dateFormat = "yyyy-MM-dd")
    private String jlDate;

    private String jlRegDate;

    /** 计量类型 */
    @Excel(name = "计量类型")
    private Long jlType;

    /** 产值 */
    @Excel(name = "产值")
    private BigDecimal jlCz;

    /** 批复 */
    @Excel(name = "批复")
    private BigDecimal jlPf;

    /** 备注 */
    @Excel(name = "备注")
    private String jlMemo;

    /** 删除标志 */
    private Integer deleteMark;

    public void setJlId(Long jlId) 
    {
        this.jlId = jlId;
    }

    public Long getJlId() 
    {
        return jlId;
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public void setJlCustNo(String jlCustNo)
    {
        this.jlCustNo = jlCustNo;
    }

    public String getJlCustNo() 
    {
        return jlCustNo;
    }
    public void setJlDate(String jlDate)
    {
        this.jlDate = jlDate;
    }

    public String getJlDate()
    {
        return jlDate;
    }

    public String getJlRegDate() {
        return jlRegDate;
    }

    public void setJlRegDate(String jlRegDate) {
        this.jlRegDate = jlRegDate;
    }

    public void setJlType(Long jlType)
    {
        this.jlType = jlType;
    }

    public Long getJlType() 
    {
        return jlType;
    }
    public void setJlCz(BigDecimal jlCz) 
    {
        this.jlCz = jlCz;
    }

    public BigDecimal getJlCz() 
    {
        return jlCz;
    }
    public void setJlPf(BigDecimal jlPf) 
    {
        this.jlPf = jlPf;
    }

    public BigDecimal getJlPf() 
    {
        return jlPf;
    }
    public void setJlMemo(String jlMemo) 
    {
        this.jlMemo = jlMemo;
    }

    public String getJlMemo() 
    {
        return jlMemo;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jlId", getJlId())
            .append("conId", getConId())
            .append("jlCustNo", getJlCustNo())
            .append("jlDate", getJlDate())
            .append("jlRegDate", getJlRegDate())
            .append("jlType", getJlType())
            .append("jlCz", getJlCz())
            .append("jlPf", getJlPf())
            .append("jlMemo", getJlMemo())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
