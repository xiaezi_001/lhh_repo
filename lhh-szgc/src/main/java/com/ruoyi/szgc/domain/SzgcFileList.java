package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.util.List;

public class SzgcFileList implements Serializable {

    List<SzgcYzUpload> jlFiles;

    public List<SzgcYzUpload> getJlFiles() {
        return jlFiles;
    }

    public void setJlFiles(List<SzgcYzUpload> jlFiles) {
        this.jlFiles = jlFiles;
    }
}
