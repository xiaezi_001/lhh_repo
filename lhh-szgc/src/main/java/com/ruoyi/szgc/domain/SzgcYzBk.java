package com.ruoyi.szgc.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 总公司拨款对象 szgc_yz_bk
 * 
 * @author lhh
 * @date 2020-11-29
 */
public class SzgcYzBk extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long bkId;

    private Long conId;

    /** 拨款编号 */
    @Excel(name = "拨款编号")
    private String bkCustNo;

    /** 工程款 */
    @Excel(name = "工程款")
    private BigDecimal bkGck;

    /** 农民工 */
    @Excel(name = "农民工")
    private BigDecimal bkNmg;

    /** 实付款 */
    @Excel(name = "实付款")
    private BigDecimal bkSf;

    /** 拨款日期 */
    @Excel(name = "拨款日期", width = 30, dateFormat = "yyyy-MM-dd")
    private String bkDate;

    /** 拨款备注 */
    private String bkMemo;

    /** 删除标志 */
    private Integer deleteMark;

    public void setBkId(Long bkId) 
    {
        this.bkId = bkId;
    }

    public Long getBkId() 
    {
        return bkId;
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public void setBkCustNo(String bkCustNo)
    {
        this.bkCustNo = bkCustNo;
    }

    public String getBkCustNo() 
    {
        return bkCustNo;
    }
    public void setBkGck(BigDecimal bkGck) 
    {
        this.bkGck = bkGck;
    }

    public BigDecimal getBkGck() 
    {
        return bkGck;
    }
    public void setBkNmg(BigDecimal bkNmg) 
    {
        this.bkNmg = bkNmg;
    }

    public BigDecimal getBkNmg() 
    {
        return bkNmg;
    }
    public void setBkSf(BigDecimal bkSf) 
    {
        this.bkSf = bkSf;
    }

    public BigDecimal getBkSf() 
    {
        return bkSf;
    }
    public void setBkDate(String bkDate)
    {
        this.bkDate = bkDate;
    }

    public String getBkDate()
    {
        return bkDate;
    }
    public void setBkMemo(String bkMemo) 
    {
        this.bkMemo = bkMemo;
    }

    public String getBkMemo() 
    {
        return bkMemo;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bkId", getBkId())
            .append("conId", getConId())
            .append("bkCustNo", getBkCustNo())
            .append("bkGck", getBkGck())
            .append("bkNmg", getBkNmg())
            .append("bkSf", getBkSf())
            .append("bkDate", getBkDate())
            .append("bkMemo", getBkMemo())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
