package com.ruoyi.szgc.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主进款对象 szgc_yz_jk
 * 
 * @author lhh
 * @date 2020-11-29
 */
public class SzgcYzJk extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long jkId;

    private Long conId;

    /** 进款编号 */
    @Excel(name = "进款编号")
    private String jkCustNo;

    /** 预付款 */
    private BigDecimal jkYfk;

    /** 工程款 */
    private BigDecimal jkGck;

    /** 农民工 */
    private BigDecimal jkNmg;

    /** 计量支付 */
    @Excel(name = "计量支付")
    private BigDecimal jkSum;

    private BigDecimal jkBGS;

    private BigDecimal jkFGS;

    /** 进款日期 */
    @Excel(name = "进款日期", width = 30, dateFormat = "yyyy-MM-dd")
    private String jkDate;

    /** 税率 */
    private BigDecimal jkSl;

    /** 税费 */
    private BigDecimal jkSf;

    /** 管理费率 */
    private BigDecimal jkGlfl;

    /** 管理费 */
    private BigDecimal jkGlf;

    /** 利润率 */
    private BigDecimal jkLrl;

    /** 利润 */
    private BigDecimal jkLr;

    /** 总公司应付 */
    @Excel(name = "总公司应付")
    private BigDecimal jkYb;

    /** 备注 */
    @Excel(name = "备注")
    private String jkMemo;

    /** 删除标志 */
    private Integer deleteMark;

    private List<SzgcYzPaymentCmp> jkCmpFGS;

    public void setJkId(Long jkId) 
    {
        this.jkId = jkId;
    }

    public Long getJkId() 
    {
        return jkId;
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public void setJkCustNo(String jkCustNo)
    {
        this.jkCustNo = jkCustNo;
    }

    public String getJkCustNo() 
    {
        return jkCustNo;
    }
    public void setJkYfk(BigDecimal jkYfk) 
    {
        this.jkYfk = jkYfk;
    }

    public BigDecimal getJkYfk() 
    {
        return jkYfk;
    }
    public void setJkGck(BigDecimal jkGck) 
    {
        this.jkGck = jkGck;
    }

    public BigDecimal getJkGck() 
    {
        return jkGck;
    }
    public void setJkNmg(BigDecimal jkNmg) 
    {
        this.jkNmg = jkNmg;
    }

    public BigDecimal getJkNmg() 
    {
        return jkNmg;
    }
    public void setJkSum(BigDecimal jkSum) 
    {
        this.jkSum = jkSum;
    }

    public BigDecimal getJkSum() 
    {
        return jkSum;
    }

    public BigDecimal getJkBGS() {
        return jkBGS;
    }

    public void setJkBGS(BigDecimal jkBGS) {
        this.jkBGS = jkBGS;
    }

    public BigDecimal getJkFGS() {
        return jkFGS;
    }

    public void setJkFGS(BigDecimal jkFGS) {
        this.jkFGS = jkFGS;
    }

    public void setJkDate(String jkDate)
    {
        this.jkDate = jkDate;
    }

    public String getJkDate()
    {
        return jkDate;
    }
    public void setJkSl(BigDecimal jkSl) 
    {
        this.jkSl = jkSl;
    }

    public BigDecimal getJkSl() 
    {
        return jkSl;
    }
    public void setJkSf(BigDecimal jkSf) 
    {
        this.jkSf = jkSf;
    }

    public BigDecimal getJkSf() 
    {
        return jkSf;
    }
    public void setJkGlfl(BigDecimal jkGlfl) 
    {
        this.jkGlfl = jkGlfl;
    }

    public BigDecimal getJkGlfl() 
    {
        return jkGlfl;
    }
    public void setJkGlf(BigDecimal jkGlf) 
    {
        this.jkGlf = jkGlf;
    }

    public BigDecimal getJkGlf() 
    {
        return jkGlf;
    }
    public void setJkLrl(BigDecimal jkLrl) 
    {
        this.jkLrl = jkLrl;
    }

    public BigDecimal getJkLrl() 
    {
        return jkLrl;
    }
    public void setJkLr(BigDecimal jkLr) 
    {
        this.jkLr = jkLr;
    }

    public BigDecimal getJkLr() 
    {
        return jkLr;
    }
    public void setJkYb(BigDecimal jkYb) 
    {
        this.jkYb = jkYb;
    }

    public BigDecimal getJkYb() 
    {
        return jkYb;
    }
    public void setJkMemo(String jkMemo) 
    {
        this.jkMemo = jkMemo;
    }

    public String getJkMemo() 
    {
        return jkMemo;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }

    public List<SzgcYzPaymentCmp> getJkCmpFGS() {
        return jkCmpFGS;
    }

    public void setJkCmpFGS(List<SzgcYzPaymentCmp> jkCmpFGS) {
        this.jkCmpFGS = jkCmpFGS;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jkId", getJkId())
            .append("conId", getConId())
            .append("jkCustNo", getJkCustNo())
            .append("jkYfk", getJkYfk())
            .append("jkGck", getJkGck())
            .append("jkNmg", getJkNmg())
            .append("jkSum", getJkSum())
            .append("jkBGS", getJkBGS())
            .append("jkFGS", getJkFGS())
            .append("jkDate", getJkDate())
            .append("jkSl", getJkSl())
            .append("jkSf", getJkSf())
            .append("jkGlfl", getJkGlfl())
            .append("jkGlf", getJkGlf())
            .append("jkLrl", getJkLrl())
            .append("jkLr", getJkLr())
            .append("jkYb", getJkYb())
            .append("jkMemo", getJkMemo())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
