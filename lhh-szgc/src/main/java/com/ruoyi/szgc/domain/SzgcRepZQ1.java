package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepZQ1 implements Serializable {
    private String yzName;
    private BigDecimal czSum;
    private BigDecimal jlSum;
    private BigDecimal jkSum;
    private BigDecimal zqSum;
    private BigDecimal yqrSum;
    private BigDecimal dqrSum;

    public String getYzName() {
        return yzName;
    }

    public void setYzName(String yzName) {
        this.yzName = yzName;
    }

    public BigDecimal getCzSum() {
        return czSum;
    }

    public void setCzSum(BigDecimal czSum) {
        this.czSum = czSum;
    }

    public BigDecimal getJlSum() {
        return jlSum;
    }

    public void setJlSum(BigDecimal jlSum) {
        this.jlSum = jlSum;
    }

    public BigDecimal getJkSum() {
        return jkSum;
    }

    public void setJkSum(BigDecimal jkSum) {
        this.jkSum = jkSum;
    }

    public BigDecimal getZqSum() {
        return zqSum;
    }

    public void setZqSum(BigDecimal zqSum) {
        this.zqSum = zqSum;
    }

    public BigDecimal getYqrSum() {
        return yqrSum;
    }

    public void setYqrSum(BigDecimal yqrSum) {
        this.yqrSum = yqrSum;
    }

    public BigDecimal getDqrSum() {
        return dqrSum;
    }

    public void setDqrSum(BigDecimal dqrSum) {
        this.dqrSum = dqrSum;
    }

    public void reset(){
        this.setCzSum(new BigDecimal(0));
        this.setJlSum(new BigDecimal(0));
        this.setJkSum(new BigDecimal(0));
        this.setZqSum(new BigDecimal(0));
        this.setYqrSum(new BigDecimal(0));
        this.setDqrSum(new BigDecimal(0));
    }
}
