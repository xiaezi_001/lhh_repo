package com.ruoyi.szgc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 计量上传文件对象 szgc_yz_upload
 * 
 * @author lhh
 * @date 2020-10-29
 */
public class SzgcYzUpload implements Serializable
{
    /** 上传编号 */
    private Long uploadId;

    /** 计量编号 */
    @Excel(name = "计量编号")
    private Long jlId;

    private Integer fileType;
    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    /** 文件链接 */
    @Excel(name = "文件链接")
    private String fileUrl;

    /** 删除标志 */
    @Excel(name = "删除标志")
    private Integer deleteMark;

    public void setUploadId(Long uploadId) 
    {
        this.uploadId = uploadId;
    }

    public Long getUploadId() 
    {
        return uploadId;
    }
    public void setJlId(Long jlId) 
    {
        this.jlId = jlId;
    }

    public Long getJlId() 
    {
        return jlId;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setFileUrl(String fileUrl) 
    {
        this.fileUrl = fileUrl;
    }

    public String getFileUrl() 
    {
        return fileUrl;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uploadId", getUploadId())
            .append("jlId", getJlId())
            .append("fileType", getFileType())
            .append("fileName", getFileName())
            .append("fileUrl", getFileUrl())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
