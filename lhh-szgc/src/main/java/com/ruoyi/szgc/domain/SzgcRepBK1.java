package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepBK1 implements Serializable {

    private String conName;
    private BigDecimal bkJan;
    private BigDecimal bkFeb;
    private BigDecimal bkMar;
    private BigDecimal bkApr;
    private BigDecimal bkMay;
    private BigDecimal bkJun;
    private BigDecimal bkJul;
    private BigDecimal bkAug;
    private BigDecimal bkSep;
    private BigDecimal bkOct;
    private BigDecimal bkNav;
    private BigDecimal bkDec;
    private BigDecimal bkYear;
    private BigDecimal bkSum;

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public BigDecimal getBkJan() {
        return bkJan;
    }

    public void setBkJan(BigDecimal bkJan) {
        this.bkJan = bkJan;
    }

    public BigDecimal getBkFeb() {
        return bkFeb;
    }

    public void setBkFeb(BigDecimal bkFeb) {
        this.bkFeb = bkFeb;
    }

    public BigDecimal getBkMar() {
        return bkMar;
    }

    public void setBkMar(BigDecimal bkMar) {
        this.bkMar = bkMar;
    }

    public BigDecimal getBkApr() {
        return bkApr;
    }

    public void setBkApr(BigDecimal bkApr) {
        this.bkApr = bkApr;
    }

    public BigDecimal getBkMay() {
        return bkMay;
    }

    public void setBkMay(BigDecimal bkMay) {
        this.bkMay = bkMay;
    }

    public BigDecimal getBkJun() {
        return bkJun;
    }

    public void setBkJun(BigDecimal bkJun) {
        this.bkJun = bkJun;
    }

    public BigDecimal getBkJul() {
        return bkJul;
    }

    public void setBkJul(BigDecimal bkJul) {
        this.bkJul = bkJul;
    }

    public BigDecimal getBkAug() {
        return bkAug;
    }

    public void setBkAug(BigDecimal bkAug) {
        this.bkAug = bkAug;
    }

    public BigDecimal getBkSep() {
        return bkSep;
    }

    public void setBkSep(BigDecimal bkSep) {
        this.bkSep = bkSep;
    }

    public BigDecimal getBkOct() {
        return bkOct;
    }

    public void setBkOct(BigDecimal bkOct) {
        this.bkOct = bkOct;
    }

    public BigDecimal getBkNav() {
        return bkNav;
    }

    public void setBkNav(BigDecimal bkNav) {
        this.bkNav = bkNav;
    }

    public BigDecimal getBkDec() {
        return bkDec;
    }

    public void setBkDec(BigDecimal bkDec) {
        this.bkDec = bkDec;
    }

    public BigDecimal getBkYear() {
        return bkYear;
    }

    public void setBkYear(BigDecimal bkYear) {
        this.bkYear = bkYear;
    }

    public BigDecimal getBkSum() {
        return bkSum;
    }

    public void setBkSum(BigDecimal bkSum) {
        this.bkSum = bkSum;
    }

    public void reset(){
        this.bkJan = new BigDecimal(0);
        this.bkFeb = new BigDecimal(0);
        this.bkMar = new BigDecimal(0);
        this.bkApr = new BigDecimal(0);
        this.bkMay = new BigDecimal(0);
        this.bkJun = new BigDecimal(0);
        this.bkJul = new BigDecimal(0);
        this.bkAug = new BigDecimal(0);
        this.bkSep = new BigDecimal(0);
        this.bkOct = new BigDecimal(0);
        this.bkNav = new BigDecimal(0);
        this.bkDec = new BigDecimal(0);
        this.bkYear = new BigDecimal(0);
        this.bkSum = new BigDecimal(0);
    }
}
