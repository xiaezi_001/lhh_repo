package com.ruoyi.szgc.domain;

import java.io.Serializable;

public class SzgcSelectList implements Serializable {
    private String value;
    private String label;
    private String extDataA;
    private String extDataB;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getExtDataA() {
        return extDataA;
    }

    public void setExtDataA(String extDataA) {
        this.extDataA = extDataA;
    }

    public String getExtDataB() {
        return extDataB;
    }

    public void setExtDataB(String extDataB) {
        this.extDataB = extDataB;
    }

    @Override
    public String toString() {
        return "SzgcSelectList{" +
                "value='" + value + '\'' +
                ", label='" + label + '\'' +
                ", extDataA='" + extDataA + '\'' +
                ", extDataB='" + extDataB + '\'' +
                '}';
    }
}
