package com.ruoyi.szgc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 合同对象 szgc_contract
 *
 * @author lhh
 * @date 2020-09-17
 */
public class SzgcContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long conId;

    private String conCustNo;
    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String conName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String conStartDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String conEndDate;

    private Integer conType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long conYz;

    private String conRespon;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String conLinkMan;

    private BigDecimal conMoney;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String conLinkPhone;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer conStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer dataLine;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer conLine;

    private String conPM;
    private String conPMPhone;

    private String conRealStartDate;
    private String conRealEndDate;
    private String conAcceptDate;

    private Integer conDays;

    private Integer conAccount;

    private Integer conJSStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long registerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String registerDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long operatorId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String operatorDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer deleteMark;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String memo;
    private Long yzParent;

    public void setConId(Long conId)
    {
        this.conId = conId;
    }

    public Long getConId()
    {
        return conId;
    }
    public void setConName(String conName)
    {
        this.conName = conName;
    }

    public String getConName()
    {
        return conName;
    }
    public void setConStartDate(String conStartDate)
    {
        this.conStartDate = conStartDate;
    }

    public String getConStartDate()
    {
        return conStartDate;
    }
    public void setConEndDate(String conEndDate)
    {
        this.conEndDate = conEndDate;
    }

    public String getConEndDate()
    {
        return conEndDate;
    }
    public void setConYz(Long conYz)
    {
        this.conYz = conYz;
    }

    public Integer getConType() {
        return conType;
    }

    public void setConType(Integer conType) {
        this.conType = conType;
    }

    public Long getConYz()
    {
        return conYz;
    }
    public void setConLinkMan(String conLinkMan)
    {
        this.conLinkMan = conLinkMan;
    }

    public String getConLinkMan()
    {
        return conLinkMan;
    }
    public void setConLinkPhone(String conLinkPhone)
    {
        this.conLinkPhone = conLinkPhone;
    }

    public String getConLinkPhone()
    {
        return conLinkPhone;
    }
    public void setConStatus(Integer conStatus)
    {
        this.conStatus = conStatus;
    }

    public BigDecimal getConMoney() {
        return conMoney;
    }

    public void setConMoney(BigDecimal conMoney) {
        this.conMoney = conMoney;
    }

    public Integer getConStatus()
    {
        return conStatus;
    }
    public void setDataLine(Integer dataLine)
    {
        this.dataLine = dataLine;
    }

    public Integer getDataLine()
    {
        return dataLine;
    }
    public void setConLine(Integer conLine)
    {
        this.conLine = conLine;
    }

    public Integer getConLine()
    {
        return conLine;
    }
    public void setRegisterId(Long registerId)
    {
        this.registerId = registerId;
    }

    public Long getRegisterId()
    {
        return registerId;
    }
    public void setRegisterDate(String registerDate)
    {
        this.registerDate = registerDate;
    }

    public String getRegisterDate()
    {
        return registerDate;
    }
    public void setOperatorId(Long operatorId)
    {
        this.operatorId = operatorId;
    }

    public Long getOperatorId()
    {
        return operatorId;
    }
    public void setOperatorDate(String operatorDate)
    {
        this.operatorDate = operatorDate;
    }

    public String getOperatorDate()
    {
        return operatorDate;
    }
    public void setDeleteMark(Integer deleteMark)
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark()
    {
        return deleteMark;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getConDays() {
        return conDays;
    }

    public void setConDays(Integer conDays) {
        this.conDays = conDays;
    }

    public String getConPM() {
        return conPM;
    }

    public void setConPM(String conPM) {
        this.conPM = conPM;
    }

    public String getConPMPhone() {
        return conPMPhone;
    }

    public void setConPMPhone(String conPMPhone) {
        this.conPMPhone = conPMPhone;
    }

    public String getConRealStartDate() {
        return conRealStartDate;
    }

    public void setConRealStartDate(String conRealStartDate) {
        this.conRealStartDate = conRealStartDate;
    }

    public String getConRealEndDate() {
        return conRealEndDate;
    }

    public void setConRealEndDate(String conRealEndDate) {
        this.conRealEndDate = conRealEndDate;
    }

    public String getConAcceptDate() {
        return conAcceptDate;
    }

    public void setConAcceptDate(String conAcceptDate) {
        this.conAcceptDate = conAcceptDate;
    }

    public Integer getConJSStatus() {
        return conJSStatus;
    }

    public void setConJSStatus(Integer conJSStatus) {
        this.conJSStatus = conJSStatus;
    }

    public String getConCustNo() {
        return conCustNo;
    }

    public void setConCustNo(String conCustNo) {
        this.conCustNo = conCustNo;
    }

    public String getConRespon() {
        return conRespon;
    }

    public void setConRespon(String conRespon) {
        this.conRespon = conRespon;
    }

    public Integer getConAccount() {
        return conAccount;
    }

    public void setConAccount(Integer conAccount) {
        this.conAccount = conAccount;
    }

    public Long getYzParent() {
        return yzParent;
    }

    public void setYzParent(Long yzParent) {
        this.yzParent = yzParent;
    }

    @Override
    public String toString() {
        return "SzgcContract{" +
                "conId=" + conId +
                ", conCustNo='" + conCustNo + '\'' +
                ", conName='" + conName + '\'' +
                ", conStartDate='" + conStartDate + '\'' +
                ", conEndDate='" + conEndDate + '\'' +
                ", conType=" + conType +
                ", conYz=" + conYz +
                ", conRespon='" + conRespon + '\'' +
                ", conLinkMan='" + conLinkMan + '\'' +
                ", conMoney=" + conMoney +
                ", conLinkPhone='" + conLinkPhone + '\'' +
                ", conStatus=" + conStatus +
                ", dataLine=" + dataLine +
                ", conLine=" + conLine +
                ", conPM='" + conPM + '\'' +
                ", conPMPhone='" + conPMPhone + '\'' +
                ", conRealStartDate='" + conRealStartDate + '\'' +
                ", conRealEndDate='" + conRealEndDate + '\'' +
                ", conAcceptDate='" + conAcceptDate + '\'' +
                ", conDays=" + conDays +
                ", conAccount=" + conAccount +
                ", registerId=" + registerId +
                ", registerDate='" + registerDate + '\'' +
                ", operatorId=" + operatorId +
                ", operatorDate='" + operatorDate + '\'' +
                ", deleteMark=" + deleteMark +
                ", memo='" + memo + '\'' +
                '}';
    }
}
