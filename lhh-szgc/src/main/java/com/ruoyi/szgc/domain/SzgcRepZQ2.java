package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepZQ2 implements Serializable {

    private String conName;
    private String yzName;
    private BigDecimal conMoney;
    private String startYear;
    private String endYear;
    private BigDecimal czSum;
    private BigDecimal jlSum;
    private BigDecimal jkSum;
    private BigDecimal wqrcz;   //未确认产值
    private BigDecimal zqk;     //总欠款
    private BigDecimal kqrqk;   //可确认欠款
    private BigDecimal wqrqk;   //未确认欠款
    private String lastJKDate;
    private String accountAge;
    private String zlDis;
    private String conPerson;

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getYzName() {
        return yzName;
    }

    public void setYzName(String yzName) {
        this.yzName = yzName;
    }

    public BigDecimal getConMoney() {
        return conMoney;
    }

    public void setConMoney(BigDecimal conMoney) {
        this.conMoney = conMoney;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public BigDecimal getCzSum() {
        return czSum;
    }

    public void setCzSum(BigDecimal czSum) {
        this.czSum = czSum;
    }

    public BigDecimal getJlSum() {
        return jlSum;
    }

    public void setJlSum(BigDecimal jlSum) {
        this.jlSum = jlSum;
    }

    public BigDecimal getJkSum() {
        return jkSum;
    }

    public void setJkSum(BigDecimal jkSum) {
        this.jkSum = jkSum;
    }

    public BigDecimal getWqrcz() {
        return wqrcz;
    }

    public void setWqrcz(BigDecimal wqrcz) {
        this.wqrcz = wqrcz;
    }

    public BigDecimal getZqk() {
        return zqk;
    }

    public void setZqk(BigDecimal zqk) {
        this.zqk = zqk;
    }

    public BigDecimal getKqrqk() {
        return kqrqk;
    }

    public void setKqrqk(BigDecimal kqrqk) {
        this.kqrqk = kqrqk;
    }

    public BigDecimal getWqrqk() {
        return wqrqk;
    }

    public void setWqrqk(BigDecimal wqrqk) {
        this.wqrqk = wqrqk;
    }

    public String getLastJKDate() {
        return lastJKDate;
    }

    public void setLastJKDate(String lastJKDate) {
        this.lastJKDate = lastJKDate;
    }

    public String getAccountAge() {
        return accountAge;
    }

    public void setAccountAge(String accountAge) {
        this.accountAge = accountAge;
    }

    public String getZlDis() {
        return zlDis;
    }

    public void setZlDis(String zlDis) {
        this.zlDis = zlDis;
    }

    public String getConPerson() {
        return conPerson;
    }

    public void setConPerson(String conPerson) {
        this.conPerson = conPerson;
    }

    public void reset(){
        this.setConMoney(new BigDecimal(0));
        this.setCzSum(new BigDecimal(0));
        this.setJlSum(new BigDecimal(0));
        this.setJkSum(new BigDecimal(0));
        this.setWqrcz(new BigDecimal(0));
        this.setZqk(new BigDecimal(0));
        this.setKqrqk(new BigDecimal(0));
        this.setWqrqk(new BigDecimal(0));
    }
}
