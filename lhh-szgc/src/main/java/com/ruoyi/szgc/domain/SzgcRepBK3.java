package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class SzgcRepBK3 implements Serializable {

    private String conName;
    private String yzName;
    private String typeDis;
    private String dateDis;
    private BigDecimal jl;
    private BigDecimal jk;
    private BigDecimal jkFGS;
    private BigDecimal jkBGS;
    private BigDecimal sf;
    private BigDecimal glf;
    private BigDecimal lr;
    private BigDecimal yb;
    private BigDecimal cmpsf;

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getYzName() {
        return yzName;
    }

    public void setYzName(String yzName) {
        this.yzName = yzName;
    }

    public String getTypeDis() {
        return typeDis;
    }

    public void setTypeDis(String typeDis) {
        this.typeDis = typeDis;
    }

    public String getDateDis() {
        return dateDis;
    }

    public void setDateDis(String dateDis) {
        this.dateDis = dateDis;
    }

    public BigDecimal getJl() {
        return jl;
    }

    public void setJl(BigDecimal jl) {
        this.jl = jl;
    }

    public BigDecimal getJk() {
        return jk;
    }

    public void setJk(BigDecimal jk) {
        this.jk = jk;
    }

    public BigDecimal getJkFGS() {
        return jkFGS;
    }

    public void setJkFGS(BigDecimal jkFGS) {
        this.jkFGS = jkFGS;
    }

    public BigDecimal getSf() {
        return sf;
    }

    public void setSf(BigDecimal sf) {
        this.sf = sf;
    }

    public BigDecimal getGlf() {
        return glf;
    }

    public void setGlf(BigDecimal glf) {
        this.glf = glf;
    }

    public BigDecimal getLr() {
        return lr;
    }

    public void setLr(BigDecimal lr) {
        this.lr = lr;
    }

    public BigDecimal getYb() {
        return yb;
    }

    public void setYb(BigDecimal yb) {
        this.yb = yb;
    }

    public BigDecimal getJkBGS() {
        return jkBGS;
    }

    public void setJkBGS(BigDecimal jkBGS) {
        this.jkBGS = jkBGS;
    }

    public BigDecimal getCmpsf() {
        return cmpsf;
    }

    public void setCmpsf(BigDecimal cmpsf) {
        this.cmpsf = cmpsf;
    }

    public void reset(){
        this.setJl(new BigDecimal(0));
        this.setJk(new BigDecimal(0));
        this.setJkFGS(new BigDecimal(0));
        this.setJkBGS(new BigDecimal(0));
        this.setSf(new BigDecimal(0));
        this.setGlf(new BigDecimal(0));
        this.setLr(new BigDecimal(0));
        this.setYb(new BigDecimal(0));
        this.setCmpsf(new BigDecimal(0));
    }
}
