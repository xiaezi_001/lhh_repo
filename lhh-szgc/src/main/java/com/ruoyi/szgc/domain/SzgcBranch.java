package com.ruoyi.szgc.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分公司对象 szgc_branch
 * 
 * @author lhh
 * @date 2020-09-20
 */
public class SzgcBranch extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Integer branchId;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String branchName;

    /** 类型 */
    @Excel(name = "类型")
    private Integer branchType;

    /** 联系人 */
    @Excel(name = "联系人")
    private String linkMan;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String linkPhone;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Integer deleteMark;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long registerId;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "备注", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registerDate;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long operatorId;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "备注", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operatorDate;

    public void setBranchId(Integer branchId)
    {
        this.branchId = branchId;
    }

    public Integer getBranchId()
    {
        return branchId;
    }
    public void setBranchName(String branchName) 
    {
        this.branchName = branchName;
    }

    public String getBranchName() 
    {
        return branchName;
    }
    public void setBranchType(Integer branchType) 
    {
        this.branchType = branchType;
    }

    public Integer getBranchType() 
    {
        return branchType;
    }
    public void setLinkMan(String linkMan) 
    {
        this.linkMan = linkMan;
    }

    public String getLinkMan() 
    {
        return linkMan;
    }
    public void setLinkPhone(String linkPhone) 
    {
        this.linkPhone = linkPhone;
    }

    public String getLinkPhone() 
    {
        return linkPhone;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }
    public void setRegisterId(Long registerId) 
    {
        this.registerId = registerId;
    }

    public Long getRegisterId() 
    {
        return registerId;
    }
    public void setRegisterDate(Date registerDate) 
    {
        this.registerDate = registerDate;
    }

    public Date getRegisterDate() 
    {
        return registerDate;
    }
    public void setOperatorId(Long operatorId) 
    {
        this.operatorId = operatorId;
    }

    public Long getOperatorId() 
    {
        return operatorId;
    }
    public void setOperatorDate(Date operatorDate) 
    {
        this.operatorDate = operatorDate;
    }

    public Date getOperatorDate() 
    {
        return operatorDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("branchId", getBranchId())
            .append("branchName", getBranchName())
            .append("branchType", getBranchType())
            .append("linkMan", getLinkMan())
            .append("linkPhone", getLinkPhone())
            .append("memo", getMemo())
            .append("deleteMark", getDeleteMark())
            .append("registerId", getRegisterId())
            .append("registerDate", getRegisterDate())
            .append("operatorId", getOperatorId())
            .append("operatorDate", getOperatorDate())
            .toString();
    }
}
