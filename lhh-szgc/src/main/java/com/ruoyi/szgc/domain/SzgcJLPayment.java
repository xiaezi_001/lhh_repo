package com.ruoyi.szgc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class SzgcJLPayment implements Serializable {

    private Long jlId;
    private Long conId;
    //0 计量 1 决算
    private Integer jlType;
    private String jlDate;
    private String jlCustomNum;
    private BigDecimal czMoney;
    private BigDecimal jlMoney;
    private BigDecimal jlFGSMoney;
    private BigDecimal jlBGSMoney;
    private BigDecimal jlPay;
    private BigDecimal jlFGSPay;
    private BigDecimal jlBGSPay;
    private BigDecimal needPay;
    private BigDecimal realPay;
    private BigDecimal yfMoney;
    private BigDecimal gcMoney;
    private BigDecimal nmgMoney;
    private BigDecimal slRate;
    private BigDecimal slMoney;
    private BigDecimal glfRate;
    private BigDecimal glfMoney;
    private BigDecimal lrRate;
    private BigDecimal lrMoney;
    private Long userId;
    private String inputDate;
    private List<SzgcYzPaymentCmp> cmpFGSMoney;
    private List<SzgcYzPaymentCmp> cmpFGSPay;
    private String memo;

    public Long getJlId() {
        return jlId;
    }

    public void setJlId(Long jlId) {
        this.jlId = jlId;
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public Integer getJlType() {
        return jlType;
    }

    public void setJlType(Integer jlType) {
        this.jlType = jlType;
    }

    public String getJlDate() {
        return jlDate;
    }

    public void setJlDate(String jlDate) {
        this.jlDate = jlDate;
    }

    public String getJlCustomNum() {
        return jlCustomNum;
    }

    public void setJlCustomNum(String jlCustomNum) {
        this.jlCustomNum = jlCustomNum;
    }

    public BigDecimal getCzMoney() {
        return czMoney;
    }

    public void setCzMoney(BigDecimal czMoney) {
        this.czMoney = czMoney;
    }

    public BigDecimal getJlMoney() {
        return jlMoney;
    }

    public void setJlMoney(BigDecimal jlMoney) {
        this.jlMoney = jlMoney;
    }

    public BigDecimal getJlPay() {
        return jlPay;
    }

    public void setJlPay(BigDecimal jlPay) {
        this.jlPay = jlPay;
    }

    public BigDecimal getJlBGSPay() {
        return jlBGSPay;
    }

    public void setJlBGSPay(BigDecimal jlBGSPay) {
        this.jlBGSPay = jlBGSPay;
    }

    public BigDecimal getNeedPay() {
        return needPay;
    }

    public void setNeedPay(BigDecimal needPay) {
        this.needPay = needPay;
    }

    public BigDecimal getRealPay() {
        return realPay;
    }

    public void setRealPay(BigDecimal realPay) {
        this.realPay = realPay;
    }

    public BigDecimal getYfMoney() {
        return yfMoney;
    }

    public void setYfMoney(BigDecimal yfMoney) {
        this.yfMoney = yfMoney;
    }

    public BigDecimal getGcMoney() {
        return gcMoney;
    }

    public void setGcMoney(BigDecimal gcMoney) {
        this.gcMoney = gcMoney;
    }

    public BigDecimal getNmgMoney() {
        return nmgMoney;
    }

    public void setNmgMoney(BigDecimal nmgMoney) {
        this.nmgMoney = nmgMoney;
    }

    public BigDecimal getSlRate() {
        return slRate;
    }

    public void setSlRate(BigDecimal slRate) {
        this.slRate = slRate;
    }

    public BigDecimal getSlMoney() {
        return slMoney;
    }

    public void setSlMoney(BigDecimal slMoney) {
        this.slMoney = slMoney;
    }

    public BigDecimal getGlfRate() {
        return glfRate;
    }

    public void setGlfRate(BigDecimal glfRate) {
        this.glfRate = glfRate;
    }

    public BigDecimal getGlfMoney() {
        return glfMoney;
    }

    public void setGlfMoney(BigDecimal glfMoney) {
        this.glfMoney = glfMoney;
    }

    public BigDecimal getLrRate() {
        return lrRate;
    }

    public void setLrRate(BigDecimal lrRate) {
        this.lrRate = lrRate;
    }

    public BigDecimal getLrMoney() {
        return lrMoney;
    }

    public void setLrMoney(BigDecimal lrMoney) {
        this.lrMoney = lrMoney;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public BigDecimal getJlBGSMoney() {
        return jlBGSMoney;
    }

    public void setJlBGSMoney(BigDecimal jlBGSMoney) {
        this.jlBGSMoney = jlBGSMoney;
    }

    public List<SzgcYzPaymentCmp> getCmpFGSMoney() {
        return cmpFGSMoney;
    }

    public void setCmpFGSMoney(List<SzgcYzPaymentCmp> cmpFGSMoney) {
        this.cmpFGSMoney = cmpFGSMoney;
    }

    public List<SzgcYzPaymentCmp> getCmpFGSPay() {
        return cmpFGSPay;
    }

    public void setCmpFGSPay(List<SzgcYzPaymentCmp> cmpFGSPay) {
        this.cmpFGSPay = cmpFGSPay;
    }

    public BigDecimal getJlFGSMoney() {
        return jlMoney.subtract(jlBGSMoney);
    }

    public void setJlFGSMoney(BigDecimal jlFGSMoney) {
        this.jlFGSMoney = jlFGSMoney;
    }

    public BigDecimal getJlFGSPay() {
        return jlPay.subtract(jlBGSPay);
    }

    public void setJlFGSPay(BigDecimal jlFGSPay) {
        this.jlFGSPay = jlFGSPay;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }
}
