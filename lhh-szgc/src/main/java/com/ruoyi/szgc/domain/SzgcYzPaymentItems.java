package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主计量明细对象 szgc_yz_payment_items
 * 
 * @author lhh
 * @date 2020-09-24
 */
public class SzgcYzPaymentItems implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long jlItemId;

    /** 计量编号 */
    @Excel(name = "计量编号")
    private Long jlId;

    /** 详细信息编号 */
    @Excel(name = "详细信息编号")
    private Integer detailId;

    /** 详细信息名称 */
    @Excel(name = "详细信息名称")
    private String detailName;

    /** 浮点值 */
    @Excel(name = "浮点值")
    private BigDecimal detailValue;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal detailMoney;

    /** $column.columnComment */
    @Excel(name = "删除标志")
    private Integer deleteMark;

    private Integer orderNum;

    public void setJlItemId(Long jlItemId) 
    {
        this.jlItemId = jlItemId;
    }

    public Long getJlItemId() 
    {
        return jlItemId;
    }
    public void setJlId(Long jlId) 
    {
        this.jlId = jlId;
    }

    public Long getJlId() 
    {
        return jlId;
    }
    public void setDetailId(Integer detailId) 
    {
        this.detailId = detailId;
    }

    public Integer getDetailId() 
    {
        return detailId;
    }
    public void setDetailName(String detailName) 
    {
        this.detailName = detailName;
    }

    public String getDetailName() 
    {
        return detailName;
    }
    public void setDetailValue(BigDecimal detailValue) 
    {
        this.detailValue = detailValue;
    }

    public BigDecimal getDetailValue() 
    {
        return detailValue;
    }
    public void setDetailMoney(BigDecimal detailMoney) 
    {
        this.detailMoney = detailMoney;
    }

    public BigDecimal getDetailMoney() 
    {
        return detailMoney;
    }
    public void setDeleteMark(Integer deleteMark)
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark()
    {
        return deleteMark;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jlItemId", getJlItemId())
            .append("jlId", getJlId())
            .append("detailId", getDetailId())
            .append("detailName", getDetailName())
            .append("detailValue", getDetailValue())
            .append("detailMoney", getDetailMoney())
            .append("deleteMark", getDeleteMark())
            .append("orderNum", getOrderNum())
            .toString();
    }
}
