package com.ruoyi.szgc.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主产值对象 szgc_yz_cz
 * 
 * @author lhh
 * @date 2020-12-17
 */
public class SzgcYzCz extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产值编号 */
    private Long czId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private Long conId;

    /** 产值客户编号 */
    @Excel(name = "产值客户编号")
    private String czCustNo;

    /** 产值日期 */
    @Excel(name = "产值日期")
    private String czDate;

    /** 产值金额 */
    @Excel(name = "产值金额")
    private BigDecimal czMoney;

    /** 备注 */
    @Excel(name = "备注")
    private String czMemo;

    /** $column.columnComment */
    private Integer deleteMark;

    public void setCzId(Long czId) 
    {
        this.czId = czId;
    }

    public Long getCzId() 
    {
        return czId;
    }
    public void setConId(Long conId) 
    {
        this.conId = conId;
    }

    public Long getConId() 
    {
        return conId;
    }
    public void setCzCustNo(String czCustNo) 
    {
        this.czCustNo = czCustNo;
    }

    public String getCzCustNo() 
    {
        return czCustNo;
    }
    public void setCzDate(String czDate) 
    {
        this.czDate = czDate;
    }

    public String getCzDate() 
    {
        return czDate;
    }
    public void setCzMoney(BigDecimal czMoney) 
    {
        this.czMoney = czMoney;
    }

    public BigDecimal getCzMoney() 
    {
        return czMoney;
    }
    public void setCzMemo(String czMemo) 
    {
        this.czMemo = czMemo;
    }

    public String getCzMemo() 
    {
        return czMemo;
    }
    public void setDeleteMark(Integer deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark() 
    {
        return deleteMark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("czId", getCzId())
            .append("conId", getConId())
            .append("czCustNo", getCzCustNo())
            .append("czDate", getCzDate())
            .append("czMoney", getCzMoney())
            .append("czMemo", getCzMemo())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("deleteMark", getDeleteMark())
            .toString();
    }
}
