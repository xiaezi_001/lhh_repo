package com.ruoyi.szgc.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主单位对象 szgc_yzdw
 * 
 * @author lhh
 * @date 2020-09-19
 */
public class SzgcYzdw extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 单位编号 */
    private Long yzId;

    private Long parentId;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String yzName;

    /** 类型 */
    @Excel(name = "类型")
    private Integer yzType;

    /** 联系人 */
    @Excel(name = "联系人")
    private String yzLinkMan;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String yzLinkPhone;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long yzParentId;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long yzStatus;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long deleteMark;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long registerId;

    /** $column.columnComment */
    @Excel(name = "备注", width = 30, dateFormat = "yyyy-MM-dd")
    private String registerDate;

    /** $column.columnComment */
    @Excel(name = "备注")
    private Long operatorId;

    /** $column.columnComment */
    @Excel(name = "备注", width = 30, dateFormat = "yyyy-MM-dd")
    private String operatorDate;

    List<SzgcYzdw> children;

    public void setYzId(Long yzId) 
    {
        this.yzId = yzId;
    }

    public Long getYzId() 
    {
        return yzId;
    }
    public void setYzName(String yzName) 
    {
        this.yzName = yzName;
    }

    public String getYzName() 
    {
        return yzName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setYzType(Integer yzType)
    {
        this.yzType = yzType;
    }

    public Integer getYzType() 
    {
        return yzType;
    }
    public void setYzLinkMan(String yzLinkMan) 
    {
        this.yzLinkMan = yzLinkMan;
    }

    public String getYzLinkMan() 
    {
        return yzLinkMan;
    }
    public void setYzLinkPhone(String yzLinkPhone) 
    {
        this.yzLinkPhone = yzLinkPhone;
    }

    public String getYzLinkPhone() 
    {
        return yzLinkPhone;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setYzParentId(Long yzParentId) 
    {
        this.yzParentId = yzParentId;
    }

    public Long getYzParentId() 
    {
        return yzParentId;
    }
    public void setYzStatus(Long yzStatus) 
    {
        this.yzStatus = yzStatus;
    }

    public Long getYzStatus() 
    {
        return yzStatus;
    }
    public void setDeleteMark(Long deleteMark) 
    {
        this.deleteMark = deleteMark;
    }

    public Long getDeleteMark() 
    {
        return deleteMark;
    }
    public void setRegisterId(Long registerId) 
    {
        this.registerId = registerId;
    }

    public Long getRegisterId() 
    {
        return registerId;
    }
    public void setRegisterDate(String registerDate)
    {
        this.registerDate = registerDate;
    }

    public String getRegisterDate()
    {
        return registerDate;
    }
    public void setOperatorId(Long operatorId) 
    {
        this.operatorId = operatorId;
    }

    public Long getOperatorId() 
    {
        return operatorId;
    }
    public void setOperatorDate(String operatorDate)
    {
        this.operatorDate = operatorDate;
    }

    public String getOperatorDate()
    {
        return operatorDate;
    }

    public List<SzgcYzdw> getChildren() {
        return children;
    }

    public void setChildren(List<SzgcYzdw> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("yzId", getYzId())
                .append("parentID", getParentId())
            .append("yzName", getYzName())
            .append("yzType", getYzType())
            .append("yzLinkMan", getYzLinkMan())
            .append("yzLinkPhone", getYzLinkPhone())
            .append("memo", getMemo())
            .append("yzParentId", getYzParentId())
            .append("yzStatus", getYzStatus())
            .append("deleteMark", getDeleteMark())
            .append("registerId", getRegisterId())
            .append("registerDate", getRegisterDate())
            .append("operatorId", getOperatorId())
            .append("operatorDate", getOperatorDate())
            .toString();
    }
}
