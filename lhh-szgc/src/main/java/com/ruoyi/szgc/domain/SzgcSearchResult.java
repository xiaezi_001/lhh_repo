package com.ruoyi.szgc.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class SzgcSearchResult implements Serializable {

    private Long conId;
    private String yzName;
    private String conName;
    private String startYear;
    private String endYear;
    private String conPeriod;
    private BigDecimal conMoney;
    private BigDecimal czPlan;
    private BigDecimal czSum;
    private BigDecimal jlPF;
    private BigDecimal jkSum;
    private BigDecimal jkFGS;
    private BigDecimal jkBGS;
    private BigDecimal jkSF;
    private BigDecimal jkGLF;
    private BigDecimal jkLR;
    private BigDecimal jkKJ;
    private BigDecimal jkYB;
    private BigDecimal bkSF;
    private String lastBKDate;
    private BigDecimal gsqf;
    private BigDecimal zqk;
    private BigDecimal kqrqk;
    private BigDecimal wqrqk;
    private String zlDis;
    private String typeDis;
    private String ztDis;
    private String jsztDis;
    private String zhDis;
    private String conRespon;

    private List<SzgcSearchDetail> czDeatil;
    private List<SzgcSearchDetail> jsDeatil;
    private List<SzgcSearchDetail> jlDeatil;
    private List<SzgcSearchDetail> jkDeatil;
    private List<SzgcSearchDetail> bkDeatil;

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public String getYzName() {
        return yzName;
    }

    public void setYzName(String yzName) {
        this.yzName = yzName;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public String getConPeriod() {
        return conPeriod;
    }

    public void setConPeriod(String conPeriod) {
        this.conPeriod = conPeriod;
    }

    public BigDecimal getConMoney() {
        return conMoney;
    }

    public void setConMoney(BigDecimal conMoney) {
        this.conMoney = conMoney;
    }

    public BigDecimal getCzPlan() {
        return czPlan;
    }

    public void setCzPlan(BigDecimal czPlan) {
        this.czPlan = czPlan;
    }

    public BigDecimal getCzSum() {
        return czSum;
    }

    public void setCzSum(BigDecimal czSum) {
        this.czSum = czSum;
    }

    public BigDecimal getJlPF() {
        return jlPF;
    }

    public void setJlPF(BigDecimal jlPF) {
        this.jlPF = jlPF;
    }

    public BigDecimal getJkSum() {
        return jkSum;
    }

    public void setJkSum(BigDecimal jkSum) {
        this.jkSum = jkSum;
    }

    public BigDecimal getJkFGS() {
        return jkFGS;
    }

    public void setJkFGS(BigDecimal jkFGS) {
        this.jkFGS = jkFGS;
    }

    public BigDecimal getJkBGS() {
        return jkBGS;
    }

    public void setJkBGS(BigDecimal jkBGS) {
        this.jkBGS = jkBGS;
    }

    public BigDecimal getJkSF() {
        return jkSF;
    }

    public void setJkSF(BigDecimal jkSF) {
        this.jkSF = jkSF;
    }

    public BigDecimal getJkGLF() {
        return jkGLF;
    }

    public void setJkGLF(BigDecimal jkGLF) {
        this.jkGLF = jkGLF;
    }

    public BigDecimal getJkLR() {
        return jkLR;
    }

    public void setJkLR(BigDecimal jkLR) {
        this.jkLR = jkLR;
    }

    public BigDecimal getJkKJ() {
        return jkKJ;
    }

    public void setJkKJ(BigDecimal jkKJ) {
        this.jkKJ = jkKJ;
    }

    public BigDecimal getJkYB() {
        return jkYB;
    }

    public void setJkYB(BigDecimal jkYB) {
        this.jkYB = jkYB;
    }

    public BigDecimal getBkSF() {
        return bkSF;
    }

    public void setBkSF(BigDecimal bkSF) {
        this.bkSF = bkSF;
    }

    public String getLastBKDate() {
        return lastBKDate;
    }

    public void setLastBKDate(String lastBKDate) {
        this.lastBKDate = lastBKDate;
    }

    public BigDecimal getGsqf() {
        return gsqf;
    }

    public void setGsqf(BigDecimal gsqf) {
        this.gsqf = gsqf;
    }

    public BigDecimal getZqk() {
        return zqk;
    }

    public void setZqk(BigDecimal zqk) {
        this.zqk = zqk;
    }

    public BigDecimal getKqrqk() {
        return kqrqk;
    }

    public void setKqrqk(BigDecimal kqrqk) {
        this.kqrqk = kqrqk;
    }

    public BigDecimal getWqrqk() {
        return wqrqk;
    }

    public void setWqrqk(BigDecimal wqrqk) {
        this.wqrqk = wqrqk;
    }

    public String getZlDis() {
        return zlDis;
    }

    public void setZlDis(String zlDis) {
        this.zlDis = zlDis;
    }

    public String getTypeDis() {
        return typeDis;
    }

    public void setTypeDis(String typeDis) {
        this.typeDis = typeDis;
    }

    public String getZtDis() {
        return ztDis;
    }

    public void setZtDis(String ztDis) {
        this.ztDis = ztDis;
    }

    public String getJsztDis() {
        return jsztDis;
    }

    public void setJsztDis(String jsztDis) {
        this.jsztDis = jsztDis;
    }

    public String getZhDis() {
        return zhDis;
    }

    public void setZhDis(String zhDis) {
        this.zhDis = zhDis;
    }

    public String getConRespon() {
        return conRespon;
    }

    public void setConRespon(String conRespon) {
        this.conRespon = conRespon;
    }

    public List<SzgcSearchDetail> getCzDeatil() {
        return czDeatil;
    }

    public void setCzDeatil(List<SzgcSearchDetail> czDeatil) {
        this.czDeatil = czDeatil;
    }

    public List<SzgcSearchDetail> getJsDeatil() {
        return jsDeatil;
    }

    public void setJsDeatil(List<SzgcSearchDetail> jsDeatil) {
        this.jsDeatil = jsDeatil;
    }

    public List<SzgcSearchDetail> getJlDeatil() {
        return jlDeatil;
    }

    public void setJlDeatil(List<SzgcSearchDetail> jlDeatil) {
        this.jlDeatil = jlDeatil;
    }

    public List<SzgcSearchDetail> getJkDeatil() {
        return jkDeatil;
    }

    public void setJkDeatil(List<SzgcSearchDetail> jkDeatil) {
        this.jkDeatil = jkDeatil;
    }

    public List<SzgcSearchDetail> getBkDeatil() {
        return bkDeatil;
    }

    public void setBkDeatil(List<SzgcSearchDetail> bkDeatil) {
        this.bkDeatil = bkDeatil;
    }
}
