package com.ruoyi.szgc.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主计量对象 szgc_yz_payment
 * 
 * @author lhh
 * @date 2020-09-24
 */
public class SzgcYzPayment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long jlId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private Long conId;

    //0 计量 1 决算
    private Integer jlType;

    /** 年 */
    @Excel(name = "年")
    private Integer jlYear;

    /** 月 */
    @Excel(name = "月")
    private Integer jlMonth;

    /** 日 */
    private String jlDate;

    /** 用户编号 */
    @Excel(name = "用户编号")
    private String jlCustomNum;

    /** 产值金额 */
    @Excel(name = "产值金额")
    private BigDecimal czMoney;

    /** 批复金额 */
    @Excel(name = "批复金额")
    private BigDecimal jlMoney;

    private BigDecimal jlBGSMoney;

    //支付金额
    private BigDecimal jlPayment;

    private BigDecimal jlBGSPayment;

    //应付金额
    private BigDecimal needPayment;

    /** 实际拨款 */
    @Excel(name = "实际拨款")
    private BigDecimal realPayment;

    /** 录入用户 */
    @Excel(name = "录入用户")
    private Long inputUser;

    private String inputDate;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** $column.columnComment */
    private Integer deleteMark;

    /** $column.columnComment */
    private Long registerId;

    /** $column.columnComment */
    private Date registerDate;

    /** $column.columnComment */
    private Long operatorId;

    /** $column.columnComment */
    private Date operatorDate;

    public void setJlId(Long jlId) 
    {
        this.jlId = jlId;
    }

    public Long getJlId() 
    {
        return jlId;
    }
    public void setConId(Long conId) 
    {
        this.conId = conId;
    }

    public Long getConId() 
    {
        return conId;
    }
    public void setJlYear(Integer jlYear)
    {
        this.jlYear = jlYear;
    }

    public Integer getJlYear()
    {
        return jlYear;
    }
    public void setJlMonth(Integer jlMonth)
    {
        this.jlMonth = jlMonth;
    }

    public Integer getJlMonth()
    {
        return jlMonth;
    }
    public void setJlDate(String jlDate)
    {
        this.jlDate = jlDate;
    }

    public String getJlDate()
    {
        return jlDate;
    }
    public void setJlCustomNum(String jlCustomNum) 
    {
        this.jlCustomNum = jlCustomNum;
    }

    public String getJlCustomNum() 
    {
        return jlCustomNum;
    }

    public void setJlMoney(BigDecimal jlMoney) 
    {
        this.jlMoney = jlMoney;
    }
    public BigDecimal getJlMoney() 
    {
        return jlMoney;
    }

    public BigDecimal getJlBGSMoney() {
        return jlBGSMoney;
    }

    public void setJlBGSMoney(BigDecimal jlBGSMoney) {
        this.jlBGSMoney = jlBGSMoney;
    }

    public void setCzMoney(BigDecimal czMoney)
    {
        this.czMoney = czMoney;
    }

    public BigDecimal getCzMoney() 
    {
        return czMoney;
    }
    public void setRealPayment(BigDecimal realPayment) 
    {
        this.realPayment = realPayment;
    }

    public BigDecimal getRealPayment() 
    {
        return realPayment;
    }
    public void setInputUser(Long inputUser) 
    {
        this.inputUser = inputUser;
    }

    public Long getInputUser() 
    {
        return inputUser;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setDeleteMark(Integer deleteMark)
    {
        this.deleteMark = deleteMark;
    }

    public Integer getDeleteMark()
    {
        return deleteMark;
    }
    public void setRegisterId(Long registerId) 
    {
        this.registerId = registerId;
    }

    public Long getRegisterId() 
    {
        return registerId;
    }
    public void setRegisterDate(Date registerDate) 
    {
        this.registerDate = registerDate;
    }

    public Date getRegisterDate() 
    {
        return registerDate;
    }
    public void setOperatorId(Long operatorId) 
    {
        this.operatorId = operatorId;
    }

    public Long getOperatorId() 
    {
        return operatorId;
    }
    public void setOperatorDate(Date operatorDate) 
    {
        this.operatorDate = operatorDate;
    }

    public Date getOperatorDate() 
    {
        return operatorDate;
    }

    public Integer getJlType() {
        return jlType;
    }

    public void setJlType(Integer jlType) {
        this.jlType = jlType;
    }

    public BigDecimal getJlPayment() {
        return jlPayment;
    }

    public void setJlPayment(BigDecimal jlPayment) {
        this.jlPayment = jlPayment;
    }

    public BigDecimal getNeedPayment() {
        return needPayment;
    }

    public void setNeedPayment(BigDecimal needPayment) {
        this.needPayment = needPayment;
    }

    public BigDecimal getJlBGSPayment() {
        return jlBGSPayment;
    }

    public void setJlBGSPayment(BigDecimal jlBGSPayment) {
        this.jlBGSPayment = jlBGSPayment;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jlId", getJlId())
            .append("conId", getConId())
            .append("jlYear", getJlYear())
            .append("jlMonth", getJlMonth())
            .append("jlBGSMoney", getJlBGSMoney())
            .append("jlDate", getJlDate())
            .append("jlCustomNum", getJlCustomNum())
            .append("jlMoney", getJlMoney())
            .append("czMoney", getCzMoney())
            .append("realPayment", getRealPayment())
            .append("inputUser", getInputUser())
            .append("memo", getMemo())
            .append("deleteMark", getDeleteMark())
            .append("registerId", getRegisterId())
            .append("registerDate", getRegisterDate())
            .append("operatorId", getOperatorId())
            .append("operatorDate", getOperatorDate())
            .toString();
    }
}
