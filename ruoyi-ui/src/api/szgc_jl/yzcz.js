import request from '@/utils/request'

// 查询业主产值列表
export function listYzcz(query) {
  return request({
    url: '/szgc/yzcz/list',
    method: 'get',
    params: query
  })
}

// 查询业主产值详细
export function getYzcz(czId) {
  return request({
    url: '/szgc/yzcz/' + czId,
    method: 'get'
  })
}

// 新增业主产值
export function addYzcz(data) {
  return request({
    url: '/szgc/yzcz',
    method: 'post',
    data: data
  })
}

// 修改业主产值
export function updateYzcz(data) {
  return request({
    url: '/szgc/yzcz',
    method: 'put',
    data: data
  })
}

// 删除业主产值
export function delYzcz(czId) {
  return request({
    url: '/szgc/yzcz/' + czId,
    method: 'delete'
  })
}

// 导出业主产值
export function exportYzcz(query) {
  return request({
    url: '/szgc/yzcz/export',
    method: 'get',
    params: query
  })
}