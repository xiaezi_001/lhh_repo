import request from '@/utils/request'

// 查询总公司拨款列表
export function listZgsbk(query) {
  return request({
    url: '/szgc/zgsbk/list',
    method: 'get',
    params: query
  })
}

// 查询总公司拨款详细
export function getZgsbk(bkId) {
  return request({
    url: '/szgc/zgsbk/' + bkId,
    method: 'get'
  })
}

// 新增总公司拨款
export function addZgsbk(data) {
  return request({
    url: '/szgc/zgsbk',
    method: 'post',
    data: data
  })
}

// 修改总公司拨款
export function updateZgsbk(data) {
  return request({
    url: '/szgc/zgsbk',
    method: 'put',
    data: data
  })
}

// 删除总公司拨款
export function delZgsbk(bkId) {
  return request({
    url: '/szgc/zgsbk/' + bkId,
    method: 'delete'
  })
}

// 导出总公司拨款
export function exportZgsbk(query) {
  return request({
    url: '/szgc/zgsbk/export',
    method: 'get',
    params: query
  })
}