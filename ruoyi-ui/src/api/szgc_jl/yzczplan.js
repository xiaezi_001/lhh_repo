import request from '@/utils/request'

// 查询产值计划列表
export function listYzczplan(query) {
  return request({
    url: '/szgc/yzczplan/list',
    method: 'get',
    params: query
  })
}

// 查询产值计划详细
export function getYzczplan(czPlanId) {
  return request({
    url: '/szgc/yzczplan/' + czPlanId,
    method: 'get'
  })
}

// 新增产值计划
export function addYzczplan(data) {
  return request({
    url: '/szgc/yzczplan',
    method: 'post',
    data: data
  })
}

// 修改产值计划
export function updateYzczplan(data) {
  return request({
    url: '/szgc/yzczplan',
    method: 'put',
    data: data
  })
}

// 删除产值计划
export function delYzczplan(czPlanId) {
  return request({
    url: '/szgc/yzczplan/' + czPlanId,
    method: 'delete'
  })
}

// 导出产值计划
export function exportYzczplan(query) {
  return request({
    url: '/szgc/yzczplan/export',
    method: 'get',
    params: query
  })
}