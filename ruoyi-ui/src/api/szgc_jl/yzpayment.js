import request from '@/utils/request'

// 查询业主计量列表
export function listYzpayment(query) {
  return request({
    url: '/szgc/yzpayment/list',
    method: 'get',
    params: query
  })
}

export function getInitData(){
  return request({
    url: '/szgc/yzpayment/initData',
    method: 'get'
  })
}

// 查询业主计量详细
export function getYzpayment(jlId) {
  return request({
    url: '/szgc/yzpayment/' + jlId,
    method: 'get'
  })
}

export function getYzpaymentByConId(conId) {
  return request({
    url: '/szgc/yzpayment/listbycon/' + conId,
    method: 'get'
  })
}

// 新增业主计量
export function addYzpayment(data) {
  return request({
    url: '/szgc/yzpayment',
    method: 'post',
    data: data
  })
}

// 修改业主计量
export function updateYzpayment(data) {
  return request({
    url: '/szgc/yzpayment',
    method: 'put',
    data: data
  })
}

// 删除业主计量
export function delYzpayment(jlId) {
  return request({
    url: '/szgc/yzpayment/' + jlId,
    method: 'delete'
  })
}

// 导出业主计量
export function exportYzpayment(jlIds) {
  alert(jlIds);
  return request({
    url: '/szgc/yzpayment/export/' + jlIds,
    method: 'get'
  })
}

//get conname
export function getSelectContract(cname, ctype){
  return request({
    url: '/szgc/contract/conSelectList/' + cname + '/' + ctype,
    method: 'get'
  })
}

export function getJLConSum(conId){
  return request({
    url: '/szgc/yzpayment/jlcon/' + conId,
    method: 'get'
  })
}

export function getJLYZCmpList(jlId){
  return request({
    url: '/szgc/yzpayment/jlcmplist/' + jlId,
    method: 'get'
  })
}

export function saveYZJLCmpList(data){
  return request({
    url: '/szgc/yzpayment/saveyzjlcmp',
    method: 'post',
    data: data
  })
}

export function saveUploadFile(data){
  return request({
    url: '/szgc/yzpayment/saveUpload',
    method: 'post',
    data: data
  })
}

export function getJLUploadFiles(jlId, ftype){
  return request({
    url: '/szgc/yzpayment/getUploadFile/' + jlId + '/' + ftype,
    method: 'get'
  })
}

export function deleteUploadFile(upId){
  return request({
    url: '/szgc/yzpayment/deleteUpLoad/' + upId,
    method: 'delete'
  })
}

const baseURL = process.env.VUE_APP_BASE_API
//downloadEx
export function downloadFile(fileId) {
	window.location.href = baseURL + "/szgc/report/downloadEx?fileId=" + fileId + "&delete=" + true;
}