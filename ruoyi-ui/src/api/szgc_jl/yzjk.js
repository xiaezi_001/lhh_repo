import request from '@/utils/request'

// 查询业主进款列表
export function listYzjk(query) {
  return request({
    url: '/szgc/yzjk/list',
    method: 'get',
    params: query
  })
}

// 查询业主进款详细
export function getYzjk(jkId) {
  return request({
    url: '/szgc/yzjk/' + jkId,
    method: 'get'
  })
}

// 新增业主进款
export function addYzjk(data) {
  return request({
    url: '/szgc/yzjk',
    method: 'post',
    data: data
  })
}

// 修改业主进款
export function updateYzjk(data) {
  return request({
    url: '/szgc/yzjk',
    method: 'put',
    data: data
  })
}

// 删除业主进款
export function delYzjk(jkId) {
  return request({
    url: '/szgc/yzjk/' + jkId,
    method: 'delete'
  })
}

// 导出业主进款
export function exportYzjk(query) {
  return request({
    url: '/szgc/yzjk/export',
    method: 'get',
    params: query
  })
}