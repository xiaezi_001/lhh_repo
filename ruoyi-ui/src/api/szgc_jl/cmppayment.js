import request from '@/utils/request'

// 查询业主计量列表
export function listCmppayment(query) {
  alert("11111");
  return request({
    url: '/szgc/cmppayment/list',
    method: 'get',
    params: query
  })
}

export function getInitData(){
  return request({
    url: '/szgc/cmppayment/initData',
    method: 'get'
  })
}

// 查询业主计量详细
export function getCmppayment(jlId) {
  return request({
    url: '/szgc/cmppayment/' + jlId,
    method: 'get'
  })
}

export function getCmppaymentByConId(conId) {
  return request({
    url: '/szgc/cmppayment/listbycon/' + conId,
    method: 'get'
  })
}

// 新增业主计量
export function addCmppayment(data) {
  return request({
    url: '/szgc/cmppayment',
    method: 'post',
    data: data
  })
}

// 修改业主计量
export function updateCmppayment(data) {
  return request({
    url: '/szgc/cmppayment',
    method: 'put',
    data: data
  })
}

// 删除业主计量
export function delCmppayment(jlId) {
  return request({
    url: '/szgc/cmppayment/' + jlId,
    method: 'delete'
  })
}

// 导出业主计量
export function exportCmppayment(query) {
  return request({
    url: '/szgc/cmppayment/export',
    method: 'get',
    params: query
  })
}

//get conname
export function getSelectContract(cname){
  return request({
    url: '/szgc/contract/conSelectList/' + cname,
    method: 'get'
  })
}

export function getJLConSum(conId){
  return request({
    url: '/szgc/cmppayment/jlcon/' + conId,
    method: 'get'
  })
}

export function getJLCmpCmpList(jlId){
  return request({
    url: '/szgc/cmppayment/jlcmplist/' + jlId,
    method: 'get'
  })
}

export function saveCmpJLCmpList(data){
  return request({
    url: '/szgc/cmppayment/savecmpjlcmp',
    method: 'post',
    data: data
  })
}

export function getYzJLByMonth(data){
  return request({
    url: '/szgc/yzpayment/getYzJLMonth',
    method: 'get',
    data: data
  })
}