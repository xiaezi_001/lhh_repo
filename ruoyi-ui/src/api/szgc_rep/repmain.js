import request from '@/utils/request'

// 查询业主计量列表
/*
export function listYzjl(query) {
  return request({
    url: '/szgc/yzjl/list',
    method: 'get',
    params: query
  })
}

// 查询业主计量详细
export function getYzjl(jlId) {
  return request({
    url: '/szgc/yzjl/' + jlId,
    method: 'get'
  })
}

// 新增业主计量
export function addYzjl(data) {
  return request({
    url: '/szgc/yzjl',
    method: 'post',
    data: data
  })
}

// 修改业主计量
export function updateYzjl(data) {
  return request({
    url: '/szgc/yzjl',
    method: 'put',
    data: data
  })
}

// 删除业主计量
export function delYzjl(jlId) {
  return request({
    url: '/szgc/yzjl/' + jlId,
    method: 'delete'
  })
}

// 导出业主计量
export function exportYzjl(query) {
  return request({
    url: '/szgc/yzjl/export',
    method: 'get',
    params: query
  })
}
*/

export function exportYbb(query){
  return request({
    url: '/szgc/report/ybb',
    method: 'get',
    params: query
  });
}

export function exportNBZQ(query){
  return request({
    url: '/szgc/report/nbzq',
    method: 'get',
    params: query
  });
}

export function exportBK(query){
  return request({
    url: '/szgc/report/gsbk',
    method: 'get',
    params: query
  });
}

export function exportZQ(query){
  return request({
    url: '/szgc/report/gszq',
    method: 'get',
    params: query
  });
}

const baseURL = process.env.VUE_APP_BASE_API

export function downloadRep(fileName) {
	window.location.href = baseURL + "/szgc/report/download?fileName=" + encodeURI(fileName) + "&delete=" + true;
}

/////////////////////////////
export function getSearchResult(query) {
  return request({
    url: '/szgc/report/searchlist',
    method: 'get',
    params: query
  })
}

export function getMainSumMoney(){
  return request({
    url: '/szgc/report/mainsum',
    method: 'get'
  })
}