import request from '@/utils/request'

// 查询分公司列表
export function listBranch(query) {
  return request({
    url: '/szgc/branch/list',
    method: 'get',
    params: query
  })
}

// 查询分公司详细
export function getBranch(branchId) {
  return request({
    url: '/szgc/branch/' + branchId,
    method: 'get'
  })
}

// 新增分公司
export function addBranch(data) {
  return request({
    url: '/szgc/branch',
    method: 'post',
    data: data
  })
}

// 修改分公司
export function updateBranch(data) {
  return request({
    url: '/szgc/branch',
    method: 'put',
    data: data
  })
}

// 删除分公司
export function delBranch(branchId) {
  return request({
    url: '/szgc/branch/' + branchId,
    method: 'delete'
  })
}

// 导出分公司
export function exportBranch(query) {
  return request({
    url: '/szgc/branch/export',
    method: 'get',
    params: query
  })
}