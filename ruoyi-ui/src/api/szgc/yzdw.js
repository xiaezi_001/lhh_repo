import request from '@/utils/request'

// 查询业主单位列表
export function listYzdw(query) {
  return request({
    url: '/szgc/yzdw/list',
    method: 'get',
    params: query
  })
}

// 查询业主单位详细
export function getYzdw(yzId) {
  return request({
    url: '/szgc/yzdw/' + yzId,
    method: 'get'
  })
}

// 新增业主单位
export function addYzdw(data) {
  return request({
    url: '/szgc/yzdw',
    method: 'post',
    data: data
  })
}

// 修改业主单位
export function updateYzdw(data) {
  return request({
    url: '/szgc/yzdw',
    method: 'put',
    data: data
  })
}

// 删除业主单位
export function delYzdw(yzId) {
  return request({
    url: '/szgc/yzdw/' + yzId,
    method: 'delete'
  })
}

// 导出业主单位
export function exportYzdw(query) {
  return request({
    url: '/szgc/yzdw/export',
    method: 'get',
    params: query
  })
}